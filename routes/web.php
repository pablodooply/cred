<?php
use App\DataTables\PruebaDataTable;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
// Route::get('/creditosVencidos', 'CreditosController@creditosVencidos')->name('credito.creditosVencidos');
Route::get('fichasDiarias' , 'PagoController@fichasDiarias');
Route::get('fichasSemanales' , 'PagoController@fichasSemanales');
Route::get('fichasQuincena' , 'PagoController@fichasQuincena');
Route::get('primerPasoSemanal/' , 'PagoController@primerPasoSemanal');
Route::get('primerPasoDiario/' , 'PagoController@primerPasoDiario');
Route::get('primerPasoQuincena/' , 'PagoController@primerPasoQuincena');

Route::get('elminiarFichasQuincena/' , 'PagoController@elminiarFichasQuincena');

Route::get('moraSinSaldo/' , 'PagoController@moraSinSaldo');

Route::get('pagosVencidos/' , 'PagoController@pagosVencidos');
Route::get('pagosVencidos2/' , 'PagoController@pagosVencidos2');
Route::get('intentoTres/' , 'PagoController@intentoTres');
Route::get('cuentaFichas/' , 'PagoController@cuentaFichas');

Route::get('/', ['as' => 'home','uses' => 'HomeController@index'])->middleware('auth','revalidate');

//Rutas de clientes
//***********************************************CLIENTES************************************************
Route::resource('clientes', 'ClientesController')->middleware('auth','role:Administrador.Secretaria.Supervisor');
Route::get('cumple/', ['as' => 'clientes.cumple', 'uses' => 'ClientesController@cumpleanios'])->middleware('auth','revalidate');
Route::get('infomacion/cumple', ['as' => "info.cumpleanios", 'uses' => 'ClientesController@infoCumpleanios'])->middleware('auth','revalidate');
Route::get('clientes/listaNB/{id}', ['as' => 'clientes.ln', 'uses' => "ClientesController@listaNB"])->middleware('auth','revalidate', 'role:Administrador.Secretaria.Supervisor');
Route::get('api/infoCliente/{id}', ['as' => 'info.cliente', 'uses' => "ClientesController@infoCliente"])->middleware('auth','revalidate');
Route::post('clientes/quitarCumple/{id}', ['as' => 'clientes.quitarCumple', 'uses'  => 'ClientesController@quitarCumple'])->middleware('auth','revalidate');
Route::post('add_img/cliente/{id}',['as' => "clientes.add_img", 'uses' => 'ClientesController@agregarImagen'])->middleware('auth','revalidate');
Route::get('dataClientes',['as' => 'clientes.tableCliente', 'uses' =>  'ClientesController@tableCliente'])->middleware('auth', 'revalidate');


//Rutas de transferencia
Route::put('transferencia/ruta' ,['as' => 'ruta.transferir', 'uses' => 'RutaController@transferir'])->middleware('auth', 'revalidate');
Route::put('transferencia/cliente/{id}', ['as' => 'clientes.transferir', 'uses' => 'ClientesController@transferir'])->middleware('auth','revalidate','role:Administrador.Supervisor');
Route::get('api/promotor/ruta/{id}', ['as' => 'ruta.promotor', 'uses' => 'RutaController@infoPromotor'])->middleware('auth','revalidate', 'role:Administrador.Secretaria.Supervisor');
Route::get('info/transferecia/ruta/{id}', ['as' => 'ruta.infoRuta','uses' => 'RutaController@transferenciaRuta'])->middleware('auth','revalidate', 'role:Administrador.Supervisor');

//Rutas de creditos
//************************************************CREDITOS**********************************************
Route::resource('creditos', 'CreditosController');
// Route::get('creditos/', ['as' => 'creditos.index', 'uses' => 'CreditosController@entregados'])->middleware('auth');
// Route::get('creditos/create', ['as' => 'creditos.create', 'uses' => 'CreditosController@create'])->middleware('auth');
// Route::POST('creditos', ['as' => 'creditos.store', 'uses' => 'CreditosController@store'])->middleware('auth');
// Route::get('creditos/{creditos}', ['as' => 'creditos.show', 'uses' => 'CreditosController@show'])->middleware('auth');
// Route::get('creditos/{creditos}/edit', ['as' => 'creditos.edit', 'uses' => 'CreditosController@edit'])->middleware('auth');
// Route::put('creditos/{creditos} ' ,['as' => 'creditos.update', 'uses' => 'CreditosController@update'])->middleware('auth');

// GET           /users                      index   users.index
// GET           /users/create               create  users.create
// POST          /users                      store   users.store
// GET           /users/{user}               show    users.show
// GET           /users/{user}/edit          edit    users.edit
// PUT|PATCH     /users/{user}               update  users.update
// DELETE        /users/{user}               destroy users.destroy
Route::get('entregados/', ['as' => 'creditos.entregados', 'uses' => 'CreditosController@entregados'])->middleware('auth');
Route::get('proxFinalizar/', ['as' => 'creditos.finalizar', 'uses' => 'CreditosController@finalizar'])->middleware('auth','revalidate');
Route::get('renovacion/{id}', ['as' => 'creditos.renovacion', 'uses' => 'CreditosController@renovar'])->middleware('auth','revalidate');
Route::post('renovacion/create', ['as' => 'creditos.renoCreate', 'uses' => 'CreditosController@renovacionCreate'])->middleware('auth');
Route::get('fecha_especial' , ['as' => 'creditos.fecha', 'uses' => 'CreditosController@fecha_esp'])->middleware('auth','revalidate','role:Administrador.Supervisor');
Route::post('fecha_create' , ['as' => 'creditos.fecha_create', 'uses' => 'CreditosController@fecha_create'])->middleware('auth','revalidate', 'role:Administrador.Supervisor');
Route::get('api/infoCredito/{id}', ['as' => 'info.credito', 'uses' => 'CreditosController@credinfo'])->middleware('auth','revalidate');
Route::post('api/pago/credito/{id}', ['as' => 'pagoCreditoVencido', 'uses' => 'PagoController@pagoVencido'])->middleware('auth','revalidate');
Route::get("api/proxFinalizar", ['as' => 'creditos.apifinalizar', 'uses' => "CreditosController@apiProxFin"])->middleware('auth','revalidate');
Route::get("planes/create", ['as' => 'creditos.planes', 'uses' => 'CreditosController@planes'])->middleware('auth','revalidate');;
Route::post("planes/store", ['as' => 'creditos.planesStore', 'uses' => 'CreditosController@planesStore']);
Route::get('info/planes/montos', 'CreditosController@infoMonto')->name('info.montos')->middleware('auth','revalidate');
Route::get('info/planes/info', 'CreditosController@infoPlan')->name('info.infoPlan')->middleware('auth','revalidate');
Route::get('info/planes/nopagos', 'CreditosController@infoNoPagos')->name('info.nopagos')->middleware('auth','revalidate');
Route::get('verificar/dpi','CreditosController@verificarDPI')->middleware('auth','revalidate');
Route::post('addCompromiso/{id}', ['as' => 'creditos.addCompromiso', 'uses' => 'CreditosController@agregarFotoCompromiso'])->middleware('auth','revalidate');
Route::get('dataCreditos', ['as' => 'creditos.tableCreditos', 'uses' => 'CreditosController@tableCreditos'])->middleware('auth');

Route::get('tablaTodos', ['as' => 'tabla.todos', 'uses' => 'CreditosController@tablaTodos'])->middleware('auth');
Route::get('api/tablaTodos', ['as' => 'api.tabla.todos', 'uses' => 'CreditosController@ApitablaTodos'])->middleware('auth');
Route::get('tablaActivos', ['as' => 'tabla.activos', 'uses' => 'CreditosController@tablaActivos'])->middleware('auth');
Route::get('api/tablaActivos', ['as' => 'api.tabla.activos', 'uses' => 'CreditosController@ApitablaActivos'])->middleware('auth');
Route::get('tablaVencidos', ['as' => 'tabla.vencidos', 'uses' => 'CreditosController@tablaVencidos'])->middleware('auth');
Route::get('api/tablaVencidos', ['as' => 'api.tabla.vencidos', 'uses' => 'CreditosController@ApitablaVencidos'])->middleware('auth');
Route::get('tablaMora', ['as' => 'tabla.mora', 'uses' => 'CreditosController@tablaMora'])->middleware('auth');
Route::get('api/tablaMora', ['as' => 'api.tabla.mora', 'uses' => 'CreditosController@ApitablaMora'])->middleware('auth');
Route::get('tablaPendientes', ['as' => 'tabla.pendientes', 'uses' => 'CreditosController@tablaPendientes'])->middleware('auth');
Route::get('api/tablaPendientes', ['as' => 'api.tabla.pendientes', 'uses' => 'CreditosController@ApitablaPendientes'])->middleware('auth');
Route::get('tablaHoy', ['as' => 'tabla.hoy', 'uses' => 'CreditosController@tablaHoy'])->middleware('auth');
Route::get('api/tablaHoy', ['as' => 'api.tabla.hoy', 'uses' => 'CreditosController@ApitablaHoy'])->middleware('auth');


Route::post('add_img/{id}',['as' => 'creditos.addImg', 'uses' => 'CreditosController@agregarImagen'])->middleware('auth');
Route::match(['GET', 'POST'],'info/edit/plan',     ['as' => 'creditos.editar_plan',    'uses' => 'CreditosController@editar_plan'])->middleware('auth');
Route::get('eliminarcredito/', ['as' => 'creditos.eliminarcredito', 'uses' => 'CreditosController@eliminarcredito'])->middleware('auth');
Route::get('/credito/verificarEdad', 'CreditosController@verificarEdad')->name('credito.verificarEdad');




//Rutas de usuarios
//**********************************************USUARIOS**********************************************
Route::resource('usuarios', 'UsuarioController')->middleware('auth');
Route::match(['get', 'post'], '/usuario/crediMyn/{id}', 'UsuarioController@crediMyn')     ->name('credi.Myn');
Route::get('api/posicionesCapital', ['as' => 'usuarios.posicionesCapital', 'uses' => 'UsuarioController@posicionesCapital'])->middleware('auth','role:Administrador.Supervisor');
Route::get('api/posicionesMora', ['as' => 'usuarios.posicionesMora', 'uses' => 'UsuarioController@posicionesMora'])->middleware('auth', 'role:Administrador.Supervisor');
Route::get('posiciones', ['as' => 'usuarios.posicionesView','uses' => 'UsuarioController@posiciones'])->middleware('auth');
Route::get('dataEmpleados', ['as' => 'usuarios.tableEmpleados', 'uses' => 'UsuarioController@tableEmpleados'])->middleware('auth');
//Rutas Ajax
Route::get('api/municipios/{id}', ['as' => 'api.municipios', 'uses' => 'CreditosController@infomuni'])->middleware('auth');

//Rutas de reportes
//**********************************************REPORTES***********************************************
Route::get('reportes/{tipo}',['as'=>'reportes.index', 'uses' => 'ReportesController@index'])->middleware('auth','role:Administrador.Secretaria.Supervisor');
Route::get('api/busqueda/reportes', ['as'=>'reportes.busqueda', 'uses' => 'ReportesController@apiBusquedaClientes'])->middleware('auth', 'role:Administrador.Secretaria.Supervisor');
Route::get('api/busqueda/promotores', ['as'=>'reportes.promotores', 'uses' => 'ReportesController@apiBusquedaPromotores'])->middleware('auth', 'role:Administrador.Secretaria.Supervisor');
Route::get('api/busqueda/clasificacion', ['as'=>'reportes.clasificacion', 'uses' => 'ReportesController@apiBusquedaPlanes'])->middleware('auth', 'role:Administrador.Secretaria.Supervisor');
Route::get('api/reportes/mora',['as'=>'reportes.mora', 'uses'=>'ReportesController@apiReporteMoras'])->middleware('auth', 'role:Administrador.Secretaria.Supervisor');
Route::get('api/reportes/general', ['as' => 'reportes.general', 'uses' => 'ReportesController@apiReportesGeneral'])->middleware('auth','role:Administrador.Secretaria.Supervisor');
Route::get('api/reportes/rutas', ['as' => 'reportes.rutas','uses' => 'ReportesController@infoReporteRuta'])->middleware('auth','role:Administrador.Secretaria.Supervisor');
Route::get('info/reporte/promotor_uno', ['as' => 'reportes.promotor_uno','uses' => 'ReportesController@ReportePromotorUno'])->middleware('auth');
Route::get('info/reporte/promotor_dos', ['as' => 'reportes.promotor_dos','uses' => 'ReportesController@ReportePromotorDos'])->middleware('auth');

//Rutas notificacion
//***********************************************NOTIFICACION**********************************************
Route::get('pendientes', ['as' => 'Notificacion.pendientes','uses'=>'NotificacionController@pendiente'])->middleware('auth', 'role:Administrador.Supervisor');
Route::get('examinados', ['as' => 'Notificacion.examinados','uses'=>'NotificacionController@examinados'])->middleware('auth', 'role:Administrador.Supervisor');
Route::get('notificacion/show/{id}', ['as' => 'notificacion.show', 'uses' => 'NotificacionController@show'])->middleware('auth', 'role:Administrador.Supervisor');
Route::get('notificacion/create', ['as' => 'notificacion.create', 'uses' => 'NotificacionController@create'])->middleware('auth', 'role:Administrador.Supervisor');
Route::get('api/notificacion/num', 'NotificacionController@num')->middleware('auth', 'role:Administrador.Supervisor');

//Pagos
//***********************************************PAGOS******************************************************
Route::get('pagos/refresh/pendientes', 'PagoController@refreshPendientes')->name('refresh.pendientes')->middleware('auth');
Route::post('pagos/store/sipago', 'PagoController@SIpago')->name('pagos.store.SIpago')->middleware('auth');
Route::post('pagos/store/nopago', 'PagoController@NOpago')->name('pagos.store.NOpago')->middleware('auth');
Route::resource('pagos', 'PagoController')->middleware('auth');
Route::get('api/infopago/{id}', ['as' => 'pagos.info', 'uses' => 'PagoController@infoPago'])->middleware('auth');
Route::get('comentario/{id}', ['as' => 'comentario.info', 'uses' => 'PagoController@comentarioShow'])->middleware('auth');
Route::get('api/infopago/pendiente/{id}', ['as' => 'pagos.info.pendiente', 'uses' => 'PagoController@infoPagoPendiente'])->middleware('auth');
Route::post('boleta_pago/usuario/{id}', ['as' => "pagos.empleado", "uses" => "PagoController@pagoEmpleado"])->middleware('auth', 'role:Administrador.Supervisor');
Route::post('pago/vencido', ['as' => "pagos.vencidos", "uses" => "PagoController@pagoPrestamoVencido"])->middleware('auth');
Route::get('pago/revertir',['as' => "pagos.revertir", 'uses' => 'PagoController@revertirPago'])->middleware('auth');
Route::get('pago/eliminar',['as' => "pagos.eliminar", 'uses' => 'PagoController@eliminarPago'])->middleware('auth');
Route::get('pago/revertir/completo',['as' => "pagos.revertirCompleto", 'uses' => 'PagoController@revertirCompleto'])->middleware('auth');
Route::get('pago/revertir/hoy',['as' => "pagos.revertir.hoy", 'uses' => 'PagoController@revertirPagoHoy'])->middleware('auth');
Route::match(['get', 'post'],'actualizar/comentario', ['as' => 'pagos.updComentario', 'uses' => 'PagoController@setComentario'])->middleware('auth','role:Administrador.Secretaria.Supervisor');
Route::get('api/pagos', ['as' => 'api.pagos', 'uses' => 'PagoController@apiPagos'])->middleware('auth');

//Rutas
//*************************************************RUTAS*************************************************
Route::resource('rutas', 'RutaController')->middleware('auth', 'role:Administrador.Secretaria.Supervisor');
Route::get('cierre/ruta/{id}', ['as' => 'rutas.cierre', 'uses' => 'RutaController@cierre'])->middleware('auth');
Route::post('asignacionSupervisor', ['as' => 'rutas.asignacion', 'uses' => 'RutaController@asignacion'])->middleware('auth', 'role:Administrador');
Route::get('modalSupervisor', ['as' => 'rutas.modal', 'uses' => 'RutaController@modalSupervisor'])->middleware('auth','role:Administrador');
ROUTE::post('editar/rutas', ['as' => 'rutas.editar_rutas', 'uses' => 'RutaController@editar_rutas'])->middleware('auth', 'role:Administrador');
//Rutas Graficas
//*************************************************GRAFICAS*************************************************
Route::get('graficas', ['as' => 'graficas.index', 'uses' => 'GraficasController@index'])->middleware('auth', 'revalidate');
Route::get('grafica/promotor', ['as' => 'graficas.promotor', 'uses' => 'GraficasController@promotor'])->middleware('auth');
Route::get('ruta/grafica', ['as' => 'graficas.ruta' , 'uses' => 'GraficasController@graficaRuta'])->middleware('auth');
Route::get('api/datosGrafica', 'GraficasController@datosGrafico2')->middleware('auth');

//Rutas documentos
//**********************************************Documentos****************************************************
Route::get('pdf_compromiso/{id}', 'DocumentosController@compromiso')->name('pdf_com')->middleware('auth');
Route::get('ficha_pago/{id}', 'DocumentosController@ficha')->name('pdf_ficha')->middleware('auth');
Route::get('ficha_pago_pdf/{id}', 'DocumentosController@fichaPDF')->name('ficha_PDF')->middleware('auth');
Route::get('printCompromiso/{id}', 'DocumentosController@printCompromiso')->name('printCom')->middleware('auth');
Route::get('printPagoColaborador/{id}', 'DocumentosController@printPagoColaborador')->name('printPagoColaborador')->middleware('auth');
Route::get('printSolicitud/{id}', 'DocumentosController@printSolicitud')->name('printSol')->middleware('auth');



//Rutas Geolocalizacion
//************************************************GEOLOCALIZACION**********************************************
Route::get('geolocalizacion', ['as' => 'geolocalizacion.index', 'uses' => 'GeolocalizacionController@index'])->middleware('auth', 'role:Administrador.Supervisor');
Route::get('trazo/ruta/', ['as' => 'geolocalizacion.ruta', 'uses' => 'GeolocalizacionController@georuta'])->middleware('auth', 'role:Administrador.Supervisor');
Route::get('coordenadas/' , 'GeolocalizacionController@posicion_empleado');
Route::get('coordenadas/cliente/' , 'GeolocalizacionController@posicion_cliente');
Route::get('api/obtener_posiciones/{id}', 'GeolocalizacionController@getPosition')->name('geolocalizacion.getgps')->middleware('auth');
Route::get('api/obtener_clientes/{id}', 'GeolocalizacionController@getPositionsClients')->middleware('auth', 'role:Administrador.Supervisor');
Route::get('api/recorrido/{id}', ['as' => "api.recorrido", 'uses' => 'GeolocalizacionController@apiRecorrido'])->middleware('auth', 'role:Administrador.Supervisor');
Route::get('info/promo/rutas', 'GeolocalizacionController@infoRutas')->name('info.promoRutas')->middleware('auth');
Route::get('geo/promotor/', 'GeolocalizacionController@gpsPromotor')->name('geo.promotor')->middleware('auth');

//Propecto
Route::get('prospectos', ['as' => 'prospecto.index', 'uses' => 'ProspectoController@index'])->middleware('auth');
Route::post('prospecto/create', ['as' => 'prospecto.create', 'uses' => 'ProspectoController@create'])->middleware('auth');
Route::get('eliminar/prospecto/{id}', ['as' => 'prospecto.delete', 'uses' => 'ProspectoController@delete'])->middleware('auth');
Route::get('info/prospectos', ['as' => 'prospecto.info', 'uses' => 'ProspectoController@infoProspectos'])->middleware('auth');

//Ruta Configuracion
Route::get('configuracion', ['as' => 'configuracion.index', 'uses' => 'ConfiguracionController@index'])->middleware('auth');
Route::post('configuracion/create', ['as' => 'configuracion.store', 'uses' => 'ConfiguracionController@create'])->middleware('auth');

//Rutas de autentificacion
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->middleware('auth', 'revalidate')->name('logout');

//Pruebas
Route::get('prueba', function(PruebaDataTable $dataTable) {
    return $dataTable->render('Pruebas.index');
});

Route::get('verificar/totalpago','PagoController@verificarTOTAL')->middleware('auth','revalidate');
Route::get('pago/modal/tabla/pago/{id}', 'PagoController@ModalTablaPago')->name('pago.modal.tabla.pago')->middleware('auth');
Route::get('tabla/pagos/pendientes/{id}','PagoController@DatatablePendientes')->name('tabla.pagos.pendientes')->middleware('auth');
