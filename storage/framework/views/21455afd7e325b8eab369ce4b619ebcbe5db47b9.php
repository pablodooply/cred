<div class="row">
  <div id="pagomodalpendiente" class="modal fade bd-modal-pago-pendiente" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5> Pago </h5>
                </div>
                <div class="ibox-content">
                  <?php echo Form::open(['method' => 'POST', 'id' => 'PagoModalPendiente']); ?>
                    <div class="form-row col-sm-12">
                        <div class="form-group col-sm-4">
                            <?php echo e(Form::label('name', 'Cliente')); ?>
                            <?php echo e(Form::text('nom_cliente',null , ['class' => 'form-control valor', 'id' => 'nom_clienteee', "readonly"])); ?>
                            <?php echo e(Form::hidden('cliente', '', ['id' => 'prestamooo'])); ?>
                        </div>
                        <div class="form-group col-sm-4">
                          <?php echo e(Form::label('name', 'No. Prestamo')); ?>
                          <?php echo e(Form::text('no_prestamo', null, ['class' => 'form-control valor', 'id' => 'no_prestamooo', "readonly" ])); ?>
                        </div>
                        <div class="form-group col-sm-4">
                            <?php echo e(Form::label('name', 'No. Cuota')); ?>
                            <?php echo e(Form::text('no_cuota',null, ['class' => 'form-control valor', 'id' => 'no_cuotaaa', "readonly" ])); ?>
                        </div>
                    </div>
                    <div class="form-row col-sm-12">
                      <div class="form-group col-sm-4">
                        <?php echo e(Form::label('name', 'Mora')); ?>
                        <?php echo e(Form::text('mora',null, ['class' => 'form-control valor', 'id' => 'moraaa' , "readonly"])); ?>
                      </div>
                      <div class="form-group col-sm-8">
                        <?php echo e(Form::label('name', 'Total')); ?>
                        <?php echo e(Form::text('cuota','', ['class' => 'form-control valor', 'id' => 'totalll'])); ?>
                      </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group" id="guardar">
                          <?php echo e(Form::submit('Pago', ['class' => 'btn btn-md btn-primary', 'id' => 'botonSiPago', 'name' => 'submitbutton', 'value' => 'pago'])); ?>
                          <?php echo e(Form::submit('No pago',['class' => 'btn btn-md btn-danger','id' => 'botonNoPago', 'name' => 'submitbutton', 'value' => 'no_pago'])); ?>
                          
                      </div>
                    </div>
                    <?php echo Form::close(); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
