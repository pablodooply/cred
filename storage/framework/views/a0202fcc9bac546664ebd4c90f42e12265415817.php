<?php $__env->startSection('title', 'Inicio'); ?>

<?php $__env->startSection('content'); ?>
  <?php $__env->startSection('nombre','Inicio'); ?>
    <div class="wrapper wrapper-content animated fadeInRight">
      <div class="row">
          <div class="col-md-12">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="widget style1 navy-bg">
                  <div class="row">
                    <a class="client-link" href="<?php echo e(route('reportes.index',1)); ?>">
                      <div class="col-md-12">
                        <div class="col-lg-4 col-md-2 text-center text-uppercase	">
                          <i class="fa  fa-5x"><strong>Q</strong></i>
                        </div>
                        <div class="col-md-8  text-center">
                          <h3 class="">Capital total </h3>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="widget style1 lazur-bg">
                  <div class="row">
                    <a class="client-link" href="<?php echo e(route('reportes.index',1)); ?>">
                      <div class="col-md-12">
                      <div class="col-md-4  text-center">
                        <i class="fa fa-plus-square fa-5x"></i>
                      </div>
                      <div class="col-md-8 text-center">
                        <h3>Capital recuperado</h3>
                      </div>
                    </div>
                    </a>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="widget style1 yellow-bg">
                  <div class="row">
                    <a class="client-link" href="<?php echo e(route('reportes.index',1)); ?>">
                      <div class="col-md-12">
                      <div class="col-md-4 text-center">
                        <i class="fa fa-bell fa-5x"></i>
                      </div>
                      <div class="col-md-8 text-center">
                        <h3>Intereses cobrados</h3>
                      </div>
                    </div>
                    </a>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="widget style1 red-bg">
                  <div class="row">
                    <a class="client-link" href="<?php echo e(route('reportes.index',1)); ?>">
                      <div class="col-md-12">
                      <div class="col-md-4 text-center">
                        <i class="fa fa-user fa-5x"></i>
                      </div>
                      <div class="col-md-8 text-center">
                        <h3>Mora recaudada</h3>
                      </div>
                    </div>
                    </a>
                  </div>
                </div>
            </div>
          </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-6">
            <div class="widget-head-color-box navy-bg p-lg text-center">
                <div class="m-b-md">
                  <h2 class="font-bold no-margins">
                    Capital Activo
                  </h2>
                  <a id="empCap"class="client-link"><h4 id="empleado_capital"></h4></a>
                  <input type="hidden" id="aEmpleadoCapital">
                </div>
                <img id="img_capital" src="<?php echo e(asset('images/default/perfil.jpg')); ?>" class="img-circle circle-border m-b-md" alt="profile">
                <div>
                </div>
            </div>
          </div>
          <div class="col-md-6">
                <div class="widget-head-color-box red-bg p-lg text-center">
                    <div class="m-b-md">
                    <h2 class="font-bold no-margins">
                        Mora recuperada
                    </h2>
                        <a id="empMora" class="client-link"><h4 id="empleado_mora"></h4></a>
                        <input type="hidden" id="aEmpleadoMora">
                    </div>
                    <img id="img_mora" src="<?php echo e(asset('images/default/perfil.jpg')); ?>" class="img-circle circle-border m-b-md" alt="profile">
                    <div>
                    </div>
                </div>
          </div>
        </div>
      </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>

<script type="text/javascript">
  $(document).ready(function(){

    function obtenerEmpleados(){
      obtenerEmpleadoCapital();
      obtenerEmpleadoMora();
    }

    function obtenerEmpleadoCapital(){
      var base_imagen = "<?php echo e(asset('images/empleados/perfil/')); ?>"
      url = "<?php echo e(route('usuarios.posicionesCapital')); ?>";
      var urlHref = "<?php echo e(route('usuarios.show',':id')); ?>";
      $.ajax({
          type: "GET",
          url: url,
          success: function( response ) {
            $('#empleado_capital').text(response.nombre);
            urlHref = urlHref.replace(':id', response.codigo);
            $("#empCap").attr("href", urlHref);
            // if(response.imagen !=null){
            //   $('#img_capital').attr('src',"/" + base_imagen + response.imagen);
            // }
          }
      });
    }

    function obtenerEmpleadoMora(){
      url = "<?php echo e(route('usuarios.posicionesMora')); ?>";
      var urlHref = "<?php echo e(route('usuarios.show',':id')); ?>";
      $.ajax({
          type: "GET",
          url: url,
          success: function( response ) {
            $('#empleado_mora').text(response.nombre);
            urlHref = urlHref.replace(':id', response.codigo);
            $("#empMora").attr("href", urlHref);
          }
      });
    }
    obtenerEmpleados();

    $('#empCap').click(function(){

    });
  });

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>