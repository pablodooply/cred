<?php $__env->startSection('title', 'Pagos'); ?>

<?php $__env->startSection('content'); ?>

<?php $__env->startSection('nombre','Pagos'); ?>
<?php $__env->startSection('ruta'); ?>
<li class="active">
  <strong>Pagos actuales</strong>
</li>
<?php $__env->stopSection(); ?>
<?php
use Carbon\Carbon;
setlocale(LC_TIME, 'es_AR.utf8');
Carbon::setUtf8(false);
?>

<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Pagos para el dia <?php echo e(Carbon::now()->formatLocalized('%A %d de %B %Y')); ?></h5>
        </div>
        <div class="ibox-content">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#creditos_hoy">Pagos de hoy</a></li>
            <li><a data-toggle="tab" href="#all_creditos">Prestamos vencidos mora</a></li>
          </ul>
          <div class="tab-content">
              <div id="creditos_hoy" class="tab-pane fade in active">
                <div id="resumen-refresh-pendientes">
                <div class="input-group">
                  <input type="text" placeholder="Ejemplo Cliente: Nombre, Ruta: Alfa, Cod.: Cre-123, Estado: Pendiente" class="input form-control" id="busqueda-creditos-hoy">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Buscar</button>
                  </span>
                </div>

                <div class="table-responsive">
                  <table class='table table-striped table-hover' id="tabla-creditos-hoy">
                    <thead>
                      <tr>
                        <th>DPI</th>
                        <th>Cliente</th>
                        <th>Ruta</th>
                        <th>Plan</th>
                        <th>Hora</th>
                        <th>Cod. prestamo</th>
                        <th>No. cuota</th>
                        <th class="sum">Cuota</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if($fichas_pago_hoy->isEmpty()): ?>
                        <h1> No hay creditos<h1>
                            <?php else: ?>
                            <?php $__currentLoopData = $fichas_pago_hoy->where('estado_p', 0); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ficha_pago): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                              <td><?php echo e($ficha_pago->prestamo->cliente->persona->dpi); ?></td>
                              <td><a class="client-link" href="<?php echo e(route('clientes.show',$ficha_pago->prestamo->cliente->id)); ?>"><?php echo e($ficha_pago->prestamo->cliente->persona->nombre ." " .
                                              $ficha_pago->prestamo->cliente->persona->apellido); ?></a></td>
                              <td><a class="client-link" href="<?php echo e(route('rutas.show',$ficha_pago->prestamo->ruta->first()->hoja_ruta->id)); ?>">
                                  <?php echo e($ficha_pago->prestamo->ruta->first()->hoja_ruta->nombre); ?></a></td>
                              <?php if($ficha_pago->prestamo->plan->nombre=='Diario'): ?>
                                <td><span class="badge badge-primary"><?php echo e($ficha_pago->prestamo->plan->nombre); ?></span></td>
                              <?php elseif($ficha_pago->prestamo->plan->nombre=='Semanal'): ?>
                                <td><span class="badge badge-success"><?php echo e($ficha_pago->prestamo->plan->nombre); ?></span></td>
                              <?php elseif($ficha_pago->prestamo->plan->nombre=='Quincena'): ?>
                                <td><span class="badge badge-info"><?php echo e($ficha_pago->prestamo->plan->nombre); ?></span></td>
                              <?php endif; ?>
                                    <td><?php echo e($ficha_pago->prestamo->ruta->first()->hora); ?></td>
                                    <td><a class="client-link" href="<?php echo e(route('creditos.show', $ficha_pago->prestamo->id)); ?>">Cre-<?php echo e($ficha_pago->prestamo->id); ?></a></td>
                                    <td><?php echo e($ficha_pago->no_dia); ?></td>
                                    <td><?php echo e($ficha_pago->cuota); ?></td>
                                    <?php switch($ficha_pago->estado_p):
                                      case (0): ?>
                                      <td><a><span class='badge badge-warning pull-center'>Pendiente</span></a></td>
                                      <?php break; ?>
                                      <?php case (1): ?>
                                      <td><a><span class="badge badge-primary pull-center">Pagado</span></a></td>
                                      <?php break; ?>
                                      <?php case (2): ?>
                                      <?php if($ficha_pago->cont == 1): ?>
                                        <td><a><span class="badge badge-danger pull-center">No Pagado</span></a></td>
                                        <?php else: ?>
                                        <td><a><span class="badge badge pull-center">Perdon</span></a></td>
                                        <?php endif; ?>
                                        <?php break; ?>
                                        <?php case (3): ?>
                                        <td><a><span class="badge badge-info pull-center">Pago parcial</span></a></td>
                                        <?php break; ?>
                                        <?php case (4): ?>
                                        <td><a><span class="badge badge-success pull-center">Pago adelantado</span></a></td>
                                        <?php break; ?>
                                        <?php case (5): ?>
                                        <td><a><span class="badge badge-success pull-center">Parcial adelantado</span></a></td>
                                        <?php break; ?>;
                                        <?php endswitch; ?>
                                        <td>
                                          <?php
                                          $ficha_coment = App\Ficha_pago::where('prestamo_id', $ficha_pago->prestamo->id)->where('no_dia',$ficha_pago->no_dia)->first();
                                          $advertencia = App\Ficha_pago::where('prestamo_id', $ficha_pago->prestamo->id)->where('tipo','=',1)->where('no_dia','<',$ficha_pago->no_dia)->where('estado_p', '=',0)->count();
                                            ?>
                                            <?php if(($ficha_pago->estado_p==0 || $ficha_pago->estado_p==5) && $advertencia == 0): ?>
                                              <a id="pagar-prestamo" value="<?php echo e($ficha_pago->prestamo->id); ?>" class="btn btn-outline btn-primary btn-sm"><i class="fa"><strong>Q</strong></i></a>
                                              
                                              <?php else: ?>
                                              <a href="#" data-toggle="modal" data-target="#modal-tabla-pago" id="" value="<?php echo e($ficha_pago->prestamo->id); ?>" onclick="ModalTabla('<?php echo e($ficha_pago->prestamo->id); ?>')" class="btn btn-outline btn-primary btn-sm"><i
                                                  class="fa"><strong>Q</strong></i></a>
                                              
                                              <?php endif; ?>
                                              <?php if($ficha_coment->comentarios->isNotEmpty()): ?>
                                                <a id="comentario" value="<?php echo e($ficha_coment->comentarios->first()->id); ?>" class="btn btn-outline btn-success btn-sm"><i class="fa fa-comment-o"></i></a>
                                                <?php endif; ?>
                                                <?php if($advertencia > 0): ?>
                                                <a onclick="swal('','Existen pagos pendientes fuera de fecha','warning')" class="btn btn-danger btn-sm"><i class="fa fa-exclamation"></i></a>
                                                <?php endif; ?>
                                        </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Total:</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>

            <div id="all_creditos" class="tab-pane fade">
              <div class="input-group">
                <input type="text" placeholder="Ejemplo Cliente: Nombre, Ruta: Alfa, Cod.: Cre-123, Estado: Pendiente" class="input form-control" id="busqueda-creditos-vencidos">
                <span class="input-group-btn">
                  <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Buscar</button>
                </span>
              </div>
              <div class="table-responsive">
                <table class='table table-striped table-hover' id="tabla-creditos-vencidos">
                  <thead>
                    <tr>
                      <th>DPI</th>
                      <th>Cliente</th>
                      <th>Ruta</th>
                      <th>Hora</th>
                      <th>Cod. prestamo</th>
                      <th class="sum">Cuota</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if($prestamos_mora->isEmpty()): ?>
                      <h3> No hay prestamos vencidos en mora<h3>
                          <?php else: ?>
                          <?php $__currentLoopData = $prestamos_mora; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ruta): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <tr>
                            <td><?php echo e($ruta->prestamo->cliente->persona->dpi); ?></td>
                            <td><a class="client-link" href="<?php echo e(route('clientes.show',$ruta->prestamo->cliente->id)); ?>"><?php echo e($ruta->prestamo->cliente->persona->nombre ." " .
                                              $ruta->prestamo->cliente->persona->apellido); ?></a></td>
                            <td><a class="client-link" href="<?php echo e(route('rutas.show',$ruta->prestamo->ruta->first()->hoja_ruta->id)); ?>">
                                <?php echo e($ruta->prestamo->ruta->first()->hoja_ruta->nombre); ?></a></td>
                            <td><?php echo e($ruta->prestamo->ruta->first()->hora); ?></td>
                            <td><a class="client-link" href="<?php echo e(route('creditos.show', $ruta->prestamo->id)); ?>">Cre-<?php echo e($ruta->prestamo->id); ?></a></td>
                            <?php
                            $prestamo_real = App\Prestamo::find($ruta->prestamo->id);
                            $pagosSaldo=0;
                            $pagosMora=0;
                            $totalPagosFicha=[];
                            $listadoPagos=[];
                            $sumaTotalPagos=0;
                            $saldoTotal=0;
                            $pagosSaldo = \App\Pago::where('prestamo_id',$prestamo_real->id)->sum('monto');
                            $pagosMora = \App\Pago::where('prestamo_id',$prestamo_real->id)->sum('mora');
                            $totalPagosFicha=\App\Ficha_pago::where('prestamo_id',$prestamo_real->id)->whereNotNull('pago_id')->get();
                            $sumaTotalPagos=0;
                            foreach ($totalPagosFicha as $pagosList) {
                              $listadoPagos[]=\App\Pago::selectRaw('sum(monto) as total')->where('id',$pagosList->pago_id)->first()->total;
                              // $sumaPagos=$sumaPagos+$listadoPagos->mont o;
                            }
                            if(isset($listadoPagos))
                            {
                              foreach ($listadoPagos as $sumaPagos) {
                                $sumaTotalPagos=$sumaTotalPagos+$sumaPagos;
                              }
                            }
                            else {
                              $sumaTotalPagos=0;
                            }
                            // $totalRealPago=\App\Ficha_pago::where('')
                             $saldoTotal=$sumaTotalPagos-$pagosMora;

                             $capitalVencido=round(($prestamo_real->monto - $saldoTotal)+($prestamo_real->mora - $prestamo_real->mora_recuperada), 2);
                             if($capitalVencido<0)
                             {
                               $capitalFinal=0;
                             }
                             else {
                               $capitalFinal=$capitalVencido;
                             }
                            ?>
                            <td><?php echo e($capitalFinal); ?></td>
                            <td><a id="pago-vencido" class="btn btn-primary btn-sm pagar-btn" type="button" value="<?php echo e($ruta->prestamo->id); ?>" data-toggle="modal" data-target=".pago-vencido"><i class=""></i>Pagar</a></td>
                          </tr>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          <?php endif; ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th>Total:</th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
        <?php echo Form::close(); ?>
      </div>
    </div>
  </div>
</div>


<div class="modal inmodal fade" id="modal-tabla-pago" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div id="cont-modal-tabla-pago">
      </div>
    </div>
  </div>
</div>

<?php echo $__env->make('Creditos.pago', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('Pagos.pagoVencido', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('Pagos.partials.modal-pago-pendiente', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

  <script type="text/javascript" src="<?php echo e(asset('js/dataTables/datatables.min.js')); ?>"></script>
  <script type="text/javascript" src="<?php echo e(asset('js/sweetalert/sweetalert.min.js')); ?>"></script>

<script type="text/javascript">
function ModalTabla(valor)
{
  var id = $(this).attr('value');
  var url = '<?php echo e(route("pago.modal.tabla.pago", ":slug")); ?>';
  url = url.replace(':slug', valor);
  $('#cont-modal-tabla-pago').html(' ');
  console.log('modal pago');
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        $('#cont-modal-tabla-pago').html(response);
      }
  });
}
</script>
<script>
$(document).ready(function(){

  var oTable4 = $('#tabla-creditos-vencidos').DataTable({

    "language": {

        "url": "<?php echo e(asset('fonts/dataTablesEsp.json')); ?>",

    },

    "paging":   true,

    "ordering": false,

    "info":     false,

    'dom' : 'tip',

    "footerCallback": function ( row, data, start, end, display ) {

  var api = this.api(), data;



  var intVal = function ( i ) {

      return typeof i === 'string' ?

          i *1 :

          typeof i === 'number' ?

              i : 0;

  };



  // Total over all pages

  total = api

      .column( 1 )

      .data()

      .reduce( function (a, b) {

          return intVal(a) + intVal(b);

      }, 0 );



      console.log(total);



      // Update footer

      api.columns('.sum', { page: 'current'}).every( function () {

        var sum = this

          .data()

          .reduce( function (a, b) {

              return intVal(a) + intVal(b);

          }, 0 );



        this.footer().innerHTML = parseFloat(sum).toFixed(2);

      } );

    }

  });



  $('#busqueda-creditos-vencidos').keyup(function(){

        oTable4.search($(this).val()).draw();

  });


      var oTable3 = $('#tabla-creditos-hoy').DataTable({
        "language": {
            "url": "<?php echo e(asset('fonts/dataTablesEsp.json')); ?>",
        },
        "paging":   true,
        "ordering": false,
        "info":     false,
        'dom' : 'tip',
        "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data;

      var intVal = function ( i ) {
          return typeof i === 'string' ?
              i *1 :
              typeof i === 'number' ?
                  i : 0;
      };

      // Total over all pages
      total = api
          .column( 1 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );

          console.log(total);

          // Update footer
          api.columns('.sum', { page: 'current'}).every( function () {
            var sum = this
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
              }, 0 );

            this.footer().innerHTML = parseFloat(sum).toFixed(2);
          } );
        }
      });

      $('#busqueda-creditos-hoy').keyup(function(){
            oTable3.search($(this).val()).draw();
      });

      $("tbody").on('click', '#pagar-prestamo', function(){
        var valor = $(this).attr("value");
        var url = '<?php echo e(route("pagos.info", ":id")); ?>';
        url = url.replace(':id', valor);
        $.ajax({
            type: "GET",
            url: url,
            success: function( response ) {
              $('#nom_cliente').val(response['cliente']);
              $('#cliente').val(response['prestamo']);
              $('#no_cuota').val(response['no_cuota']);
              $('#total').val(response['cuota']);
              $('#prestamo').val(response['prestamo']);
              if(response['dia_perdon'] == '1'){
                $('#btn-perdon').hide();
              } else{
                $('#btn-perdon').show();
              }
              $('#modal-pago').modal('show');
            }
        });
      });
    });


$("tbody").on('click', '#pago-vencido', function(){
var valor = $(this).attr("value");
var url = '<?php echo e(route("info.credito", ":id")); ?>';
console.log(valor);
url = url.replace(':id', valor);
$.ajax({
type: "GET",
url: url,
success: function( response ) {
  console.log(response);
  $('#nom_cliente_v').val(response['cliente']);
  $('#prestamo_v').val(response['prestamo']);
  $('#no_prestamo_v').val(response['no_prestamo']);
  $('#capital_v').val(response['capital_vencido']);
  $('#mora_v').val(response['mora']);
  $('#interes_v').val(response['interes']);
  $('#total_v').val(response['cuota']);

}
});
});

    $('tr td').on('click', '#comentario', function(){
      var id = $(this).attr('value');
      $.ajax({
        url: '<?php echo e(route('pagos.updComentario')); ?>',
        type: 'GET',
        data: {id: id},
        success: function (data) {
          comentario(data,id);
        }
      })
    })

    function updateComentario(){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var contenido = $('#txt-observacion').val();
      var id = $('#txt-observacion').attr('name');
      $.ajax({
            /* the route pointing to the post function */
            url: '<?php echo e(route('pagos.updComentario')); ?>',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, observacion: contenido, id: id},
            // dataType: 'JSON',
            /* remind that 'data' is the response of the AjaxController */
            success: function (data) {
              swal("Observacion actualizada", {
                icon: "success",
              });
            }
        });
    }


    function comentario(texto, value){
      swal({
        'title': "Observacion",
        content: {
          element: "input",
          attributes: {
            placeholder: texto,
            name: value,
            id: 'txt-observacion',
          },
        },
        buttons: {
        cancel: 'Cancelar',
        confirm: "Actualizar",
        },
      })
      .then((confirm) => {
        if (confirm) {
          updateComentario();
        } else {
          swal("No se actualizo la observacion");
        }
      });
    }
</script>

<script type="text/javascript">
$('.revertir').click(function(){
  var valor = $(this).attr('value');
  revertir(valor);
})

function revertir(valor){
  console.log(valor);
  var url = "<?php echo e(route('pagos.revertir.hoy')); ?>";
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  swal({
    title: "Esta seguro de cambiar el estado a pendiente?",
    text: "Se van a revertir los cambios!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  }).then((willDelete) => {
    if (willDelete) {
      $.ajax({
        method: 'GET',
        url: url,
        data: {
          valor : valor,
        },
        success: function( response ) {
          console.log(response);
          console.log('peligro');
          swal("Cambios revertidos, estado a pendiente", {
            icon: "success",
          });
           location.reload();
        }
      });
    } else {
      swal("No se cambio el estado",{icon: "warning",});
    }
  });
}
</script>


<script type="text/javascript">
function refreshPendientes(){
  var url = '<?php echo e(route("refresh.pendientes")); ?>';
  $('#resumen-refresh-pendientes').html(' ');
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        $('#resumen-refresh-pendientes').html(response);
      }
  });
}

  $("#botonSiPago").click(function(event)
  {
    event.preventDefault();
    var form = $("#PagoModalPendiente");
    var data = form.serialize();
    var url = "<?php echo e(route('pagos.store.SIpago')); ?>";
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


         $.ajax({
           method: 'POST',
           url: url,
           data: data,
           cache: false,
           success: function( response ) {
             // console.log(response);
             swal({
               icon: 'success',
               title: 'Pagado!',
               text: "Se efectuó el pago con éxito!",
               timer: 1000
             });
             // DatatablesExtensionButtons.t.draw('page');
             $('#pagomodalpendiente').modal('hide');
             var t = $("#datatable-pagos-pendientes").DataTable();
             t.ajax.reload( null, false );
             // var t1 = $("#tabla-creditos-hoy").DataTable();
             // t1.reload();
             refreshPendientes();

           },
           error: function( response ) {
             console.log(response);
             console.log('peligro');
             swal({
               icon: 'error',
               title: 'Lo sentimos...',
               text: 'Ocurrió un error!',
               timer: 1500
             });

           }
         });
  });


  $("#botonNoPago").click(function(event)
  {
    event.preventDefault();
    var form = $("#PagoModalPendiente");
    var data = form.serialize();
    var url = "<?php echo e(route('pagos.store.NOpago')); ?>";
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


         $.ajax({
           method: 'POST',
           url: url,
           data: data,
           cache: false,
           success: function( response ) {
             // console.log(response);
             swal({
               icon: 'success',
               title: 'Pagado!',
               text: "Se efectuó el pago con éxito!",
               timer: 1000
             });
             // DatatablesExtensionButtons.t.draw('page');
             $('#pagomodalpendiente').modal('hide');
             var t = $("#datatable-pagos-pendientes").DataTable();
             t.ajax.reload( null, false );
             // var t1 = $("#tabla-creditos-hoy").DataTable();
             // t1.reload();
             refreshPendientes();


           },
           error: function( response ) {
             console.log(response);
             console.log('peligro');
             swal({
               icon: 'error',
               title: 'Lo sentimos...',
               text: 'Ocurrió un error!',
               timer: 1500
             });
             $('#pagomodalpendiente').modal('hide');
             var t = $("#datatable-pagos-pendientes").DataTable();
             t.ajax.reload( null, false );

           }
         });

  });
</script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>