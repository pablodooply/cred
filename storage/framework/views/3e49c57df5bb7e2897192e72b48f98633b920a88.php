<head>


<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:1;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-536859905 -1073732485 9 0 511 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:8.0pt;
	margin-left:0cm;
	line-height:106%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
p.MsoNoSpacing, li.MsoNoSpacing, div.MsoNoSpacing
	{mso-style-priority:1;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
p.msonormal0, li.msonormal0, div.msonormal0
	{mso-style-name:msonormal;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
p.msochpdefault, li.msochpdefault, div.msochpdefault
	{mso-style-name:msochpdefault;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Calibri",sans-serif;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
p.msopapdefault, li.msopapdefault, div.msopapdefault
	{mso-style-name:msopapdefault;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	margin-bottom:8.0pt;
	margin-left:0cm;
	line-height:106%;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
span.GramE
	{mso-style-name:"";
	mso-gram-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:"Calibri",sans-serif;
	mso-ascii-font-family:Calibri;
	mso-hansi-font-family:Calibri;
	mso-bidi-font-family:Calibri;}
.MsoPapDefault
	{mso-style-type:export-only;
	margin-bottom:8.0pt;
	line-height:106%;}
@page  WordSection1
	{size:612.0pt 792.0pt;
	margin:70.85pt 3.0cm 70.85pt 3.0cm;
	mso-header-margin:35.4pt;
	mso-footer-margin:35.4pt;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Tabla normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin-top:0cm;
	mso-para-margin-right:0cm;
	mso-para-margin-bottom:8.0pt;
	mso-para-margin-left:0cm;
	line-height:106%;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Calibri",sans-serif;}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="1026"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>


<div class=WordSection1>

<p class=MsoNormal align=center style='text-align:center'><b>Compromiso</b><b><span
lang=EN-US style='mso-ansi-language:EN-US'> de </span>pago</b></p>

<p class=MsoNoSpacing style='text-indent:35.4pt'>En la ciudad
Coatepeque, el <?php echo e(Carbon\Carbon::now()->format('Y-d-m')); ?> YO: <u><?php echo e($credito->cliente->persona->nombre); ?> <?php echo e($credito->cliente->persona->apellido); ?></u> quien me identifico con documento
personal de identificacion con numero de CUI <u><?php echo e($credito->cliente->persona->dpi); ?></u>
extendida por el Registro Nacional de personas, el dia de hoy recibo la
cantidad <u>Q<?php echo e($credito->capital_activo); ?></u> a mi entera satisfaccion, y me comprometo a pagar
de forma <u><?php echo e($credito->plan->nombre); ?> en <?php echo e($credito->plan->periodo->tiempo); ?></u> cuotas de: <u>Q.<?php echo e(($credito->monto / $credito->plan->periodo->tiempo)); ?></u>
cada una, las cuales hacen un total de: <u>Q.<?php echo e($credito->monto); ?></u> a favor de CREDI-ESTAR,
en un plazo maximo de 28 dias, que corresponde a la fecha <u>&quot;<?php echo e($credito->fecha_fin ? $credito->fecha_fin : "Pendiente"); ?>&quot;
</u>al incumplimiento del pago de cada cuota habra un recargo de <u>Q.<?php echo e($credito->plan->mora); ?></u>,
el cual se pagara juntamente con la cuota en atraso.</p>

<p class=MsoNormal align=center style='text-align:center'><b><span lang=EN-US
style='mso-ansi-language:EN-US'>&nbsp;<o:p></o:p></span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span lang=EN-US
style='mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span lang=EN-US
style='mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal align=center style='text-align:center'><span class=GramE><b><span
lang=EN-US style='mso-ansi-language:EN-US'>f:_</span></b></span><b><span
lang=EN-US style='mso-ansi-language:EN-US'>____________________________<o:p></o:p></span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span lang=EN-US
style='mso-ansi-language:EN-US'><span class=SpellE><?php echo e($credito->cliente->persona->nombre); ?> <?php echo e($credito->cliente->persona->apellido); ?> </span></b><u><o:p></o:p></u></p>

</div>
