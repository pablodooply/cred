<li class="<?php echo e(isActiveRoute('home')); ?>">
    <a href="<?php echo e(route('home')); ?>"><i class="fa fa-th-large"></i> <span class="nav-label">Principal</span></a>
</li>
<li class="<?php echo e(isActiveRoute('clientes.index') . isActiveRoute('clientes.cumple')); ?>">
    <a><i class="fa fa-address-book-o"></i>
      <span class="nav-label">Clientes</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="<?php echo e(route('clientes.index')); ?>">Listado de clientes</a></li>
        <li><a href="<?php echo e(route('clientes.cumple')); ?>">Cumpleañeros</a></li>
    </ul>
</li>
<li class="<?php echo e(isActiveRoute('creditos')); ?>">
    <a><i class="fa fa-folder"></i>
      <span class="nav-label">Creditos</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
      <li><a href="<?php echo e(route('creditos.index')); ?>">Listado de creditos</a></li>
      <!--<li><a href="<?php echo e(route('creditos.entregados')); ?>">Listado de entregables</a></li>-->
      <li><a href="<?php echo e(route('creditos.finalizar')); ?>">Creditos a finalizar</a></li>
    </ul>
</li>
<li class="<?php echo e(isActiveRoute('prospecto.index')); ?>">
    <a href="<?php echo e(route('prospecto.index')); ?>"><i class="fa fa-archive"></i> <span class="nav-label">Prospectos</span> </a>
</li>
<li class="<?php echo e(isActiveRoute('geo.promotor')); ?>">
    <a href="<?php echo e(route('geo.promotor')); ?>"><i class="fa fa-map-marker"></i> <span class="nav-label">Geolocalizacion</span> </a>
</li>

<li class="<?php echo e(isActiveRoute('graficas.index')); ?>">
  <a><i class="fa fa-bar-chart-o"></i>
    <span class="nav-label">Graficas</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li><a href="<?php echo e(route('graficas.promotor')); ?>">Promotores</a></li>
      <li><a href="<?php echo e(route('graficas.ruta')); ?>">Ruta</a></li>
  </ul>
</li>
