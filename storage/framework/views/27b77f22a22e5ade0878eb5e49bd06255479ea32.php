<div class="row">
  <div class="col-md-12">
      <div class="col-md-12">
        <p><strong>Datos de cliente personales:</strong></p>
        <div class="col-md-6">
          <ul class="list-group clear-list">
            <li class="list-group-item fist-item">
              Nombre:
              <span class="pull-right"><?php echo e($cliente->persona->nombre); ?> </span>
            </li>
            <li class="list-group-item">
              DPI:
              <span class="pull-right"><?php echo e($cliente->persona->dpi); ?></span>
            </li>
            <li class="list-group-item">
              NIT:
              <span class="pull-right"><?php echo e($cliente->persona->nit); ?></span>
            </li>
            <li class="list-group-item">
              Fecha nacimiento:
              <span class="pull-right"><?php echo e($cliente->persona->fecha_nacimiento); ?> </span>
            </li>
            <li class="list-group-item">
              Direccion domicilio:
              <span class="pull-right"><?php echo e($cliente->persona->domicilio); ?></span>
            </li>
            <li class="list-group-item">
              Tipo de casa:
              <span class="pull-right"><?php echo e($cliente->tipo_casa); ?></span>
            </li>
            <li class="list-group-item">
              Nombre recibo de luz:
              <span class="pull-right"><?php echo e($cliente->nombre_recibo); ?></span>
            </li>
          </ul>
        </div>
        <div class="col-md-6">
          <ul class="list-group clear-list">
            <li class="list-group-item fist-item">
              Teléfono:
              <span class="pull-right"><?php echo e($cliente->persona->telefono_r); ?> </span>
            </li>
            <li class="list-group-item">
              Celular 1:
              <span class="pull-right"><?php echo e($cliente->persona->celular1); ?></span>
            </li>
            <li class="list-group-item">
              Celular 2:
              <span class="pull-right"><?php echo e($cliente->persona->celular2); ?></span>
            </li>
            <li class="list-group-item">
              Genero:
              <?php if($cliente->persona->genero==1): ?>
               <span class="pull-right">Masculino</span>
              <?php else: ?>
               <span class="pull-right">Femenino</span>
              <?php endif; ?>
            </li>
            <li class="list-group-item">
              Estado civil:
              <span class="pull-right"><?php echo e($cliente->estado_civil); ?></span>
            </li>
            <li class="list-group-item">
              Actividad economica:
              <span class="pull-right"><?php echo e($cliente->actividad); ?></span>
            </li>
            <li class="list-group-item">
              Direccion recibo de luz:
              <span class="pull-right"><?php echo e($cliente->direccion_recibo); ?> </span>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-12">
        <p><strong>Datos Trabajo:</strong></p>
        <div class="col-md-6">
          <ul class="list-group clear-list">
            <li class="list-group-item fist-item">
              Empresa:
              <span class="pull-right"><?php echo e($cliente->empresa_trabajo); ?></span>
            </li>
            <li class="list-group-item">
              Tiempo trabajando:
              <span class="pull-right"><?php echo e($cliente->tiempo_trabajando); ?></span>
            </li>
          </ul>
        </div>
        <div class="col-md-6">
          <ul class="list-group clear-list">
            <li class="list-group-item fist-item">
              Telefono empresa:
              <span class="pull-right"><?php echo e($cliente->empresa_trabajo); ?></span>
            </li>
            <li class="list-group-item">
              Sueldo:
              <span class="pull-right"><?php echo e($cliente->salario); ?></span>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-12">
        <p><strong>Referencia personales:</strong></p>
        <?php $__currentLoopData = $cliente->referencias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $referencia): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="col-md-4">
            <ul class="list-group clear-list">
              <li class="list-group-item fist-item">
                Nombre:
                <span class="pull-right"><?php echo e($referencia->nombre); ?> <?php echo e($referencia->apellido); ?></span>
              </li>
              <li class="list-group-item fist-item">
                Direccion:
                <span class="pull-right"><?php echo e($referencia->direccion); ?></span>
              </li>
              <li class="list-group-item fist-item">
                Telefono:
                <span class="pull-right"><?php echo e($referencia->telefono); ?></span>
              </li>
            </ul>
          </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
  </div>
</div>
