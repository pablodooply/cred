<div class="row">
  <div id="modal-vencido" class="modal fade bd-example-modal-lg pago-vencido" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5> Pago </h5>
                </div>
                <div class="ibox-content">
                  <?php echo Form::open(['method' => 'POST', "route" => "pagos.vencidos"]); ?>
                    <div class="form-row col-sm-12">
                      <div class="form-group col-sm-4">
                          <?php echo e(Form::label('name', 'Cliente')); ?>
                          <?php echo e(Form::text('nom_cliente',null , ['class' => 'form-control valor', 'id' => 'nom_cliente_v', "readonly"])); ?>
                          <?php echo e(Form::hidden('prestamo_v', null, ['id' => 'prestamo_v'])); ?>
                      </div>
                      <div class="form-group col-sm-4">
                        <?php echo e(Form::label('name', 'No. Prestamo')); ?>
                        <?php echo e(Form::text('no_prestamo_v', null, ['class' => 'form-control valor', 'id' => 'no_prestamo_v', "readonly" ])); ?>
                      </div>
                      <div class="form-group col-sm-4">
                          <?php echo e(Form::label('name', 'Capital Vencido')); ?>
                          <?php echo e(Form::text('capital_v',null, ['class' => 'form-control valor', 'id' => 'capital_v', "readonly" ])); ?>
                      </div>
                    </div>
                    <div class="form-row col-sm-12">
                      <div class="form-group col-sm-4">
                        <?php echo e(Form::label('name', 'Mora')); ?>
                        <?php echo e(Form::text('mora_v',null, ['class' => 'form-control valor', 'id' => 'mora_v' , "readonly"])); ?>
                      </div>
                      <div class="form-group col-sm-4">
                        <?php echo e(Form::label('name', 'Interes')); ?>
                        <?php echo e(Form::text('interes_v',null, ['class' => 'form-control valor', 'id' => 'interes_v' , "readonly"])); ?>
                      </div>
                      <div class="form-group col-sm-4">
                        <?php echo e(Form::label('name', 'Total')); ?>
                        <?php echo e(Form::text('total_v','', ['class' => 'form-control valor', 'id' => 'total_v' ])); ?>
                      </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group" id="guardar">
                          <?php echo e(Form::submit('Pago', ['class' => 'btn btn-md btn-primary', 'name' => 'submitbutton'])); ?>
                          
                          <?php echo e(Form::submit('No pago',['class' => 'btn btn-md btn-danger', 'name' => 'submitbutton'])); ?>


                      </div>
                    </div>
                <?php echo Form::close(); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
