<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?php echo $__env->yieldContent('nombre'); ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo e(route('home')); ?>">Inicio</a>
            </li>
            <?php echo $__env->yieldContent('ruta'); ?>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
