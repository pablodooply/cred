<div class="row">
  <div id="modal-pago" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5>Pago</h5>
                </div>
                <div class="ibox-content">
                  <?php echo Form::  open(['id'=> 'formulario', 'method' => 'POST','route' => 'pagos.store']); ?>
                    <div class="form-row col-sm-12">
                        <div class="form-group col-sm-6">
                            <?php echo e(Form::label('name', 'Cliente')); ?>
                            <?php echo e(Form::text('nom_cliente','Alex', ['class' => 'form-control', 'id' => 'nom_cliente', 'readonly'])); ?>
                            <?php echo e(Form::hidden('cliente', null, array('id' => 'cliente'))); ?>
                        </div>
                        <div class="form-group col-sm-6">
                            <?php echo e(Form::label('name', 'Prestamo')); ?>
                            <?php echo e(Form::text('prestamo','3', ['class' => 'form-control', 'id' => 'prestamo', 'readonly'])); ?>
                        </div>
                    </div>
                    <div class="form-row col-sm-12">
                        <div class="form-group col-sm-6">
                            <?php echo e(Form::label('name', 'No. Cuota')); ?>
                            <?php echo e(Form::text('no_cuota', '10', ['class' => 'form-control', 'id' => 'no_cuota' , 'readonly'])); ?>
                        </div>
                        <div class="form-group col-sm-6">
                            <?php echo e(Form::label('name', 'Total')); ?>
                            
                            <input type="number" name="cuota" min="1" class="form-control" id="total" oninput="this.value = Math.abs(this.value)">
                        </div>
                    </div>
                    <div class="form-row col-sm-12">
                        <div class="form-group col-sm-12">
                            <?php echo e(Form::label('name', 'Observaciones')); ?>
                            <?php echo e(Form::text('observaciones', null, ['class' => 'form-control', 'id' => 'observacion' ])); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group" id="guardar">
                          <?php echo e(Form::submit('Pago', ['class' => 'btn btn-md btn-primary', 'name' => 'submitbutton', 'value' => 'pago'])); ?>
                          
                          <?php echo e(Form::submit('No pago',['class' => 'btn btn-md btn-danger','name' => 'submitbutton', 'value' => 'no_pago'])); ?>

                          
                      </div>
                    </div>
                  <?php echo Form::close(); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
