<div class="row">
  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
          <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Pago</h5>
        </div>
        <div class="ibox-content">
          <?php echo Form::  open(['id'=> 'formulario','route' => 'pagos.store', 'method' => 'POST']); ?>
          <div class="form-row col-sm-12">
              <div class="form-group col-sm-6">
                <?php echo e(Form::label('name', 'Cliente')); ?>
                <?php echo e(Form::text('nom_cliente', null, ['class' => 'form-control', 'id' => 'nom_cliente'])); ?>
                <?php echo e(Form::hidden('cliente', null, array('id' => 'cliente'))); ?>
              </div>
              <div class="form-group col-sm-6">
                <?php echo e(Form::label('name', 'No. cuota')); ?>
                <?php echo e(Form::text('no_cuota', null, ['class' => 'form-control', 'id' => 'no_cuota'])); ?>
              </div>
          </div>

          <div class="form-row col-sm-12">
            <div class="form-group col-sm-4">
              <?php echo e(Form::label('name', 'Cuota')); ?>
              <?php echo e(Form::text('cuota', null, ['class' => 'form-control', 'id' => 'cuota'])); ?>
            </div>
            <div class="form-group col-sm-8">
                <?php echo e(Form::label('name', 'Observaciones')); ?>
                <?php echo e(Form::text('observaciones', null, ['class' => 'form-control', 'id' => 'nit' ])); ?>
            </div>
          </div>

          <div class="form-row">
              <div class="form-group" id="guardar">
                <?php echo e(Form::submit('Pago', ['class' => 'btn btn-md btn-primary', 'name' => 'submitbutton', 'value' => 'pago'])); ?>
                <?php echo e(Form::submit('No pago',['class' => 'btn btn-md btn-danger','name' => 'submitbutton', 'value' => 'no_pago'])); ?>
                
            </div>
          </div>
        </div>
      <?php echo Form::close(); ?>
      </div>
    </div>
        </div>
      </div>
    </div>
  </div>
</div>
