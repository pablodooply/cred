<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                  <div class="dropdown-messages-box">
                    <a href="" class="pull-left">
                        <img alt="image" class="img-circle" src="<?php echo e(isset(Auth::user()->persona->foto_perfil) ? asset('/images/empleados/perfil/').'/' . Auth::user()->persona->foto_perfil : asset('images/default/perfil.jpg')); ?>">
                    </a>
                  </div>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold"><?php echo e(Auth::user()->name); ?></strong>
                            </span>
                        </span>
                    </a>
                </div>
                <div class="logo-element">
                    <img alt="image" class="img-md" src="<?php echo e(asset('images/system/logo2.png')); ?>"></img>
                </div>
            </li>

          <?php if(Auth::user()->hasRole('Administrador')): ?>
            <?php echo $__env->make('layouts.navegacion_admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
          <?php endif; ?>
          <?php if(Auth::user()->hasRole('Supervisor')): ?>
            <?php echo $__env->make('layouts.navegacion_supervisor', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
          <?php endif; ?>
          <?php if(Auth::user()->hasRole('Secretaria')): ?>
            <?php echo $__env->make('layouts.navegacion_secre', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
          <?php endif; ?>
          <?php if(Auth::user()->hasRole('Promotor')): ?>
            <?php echo $__env->make('layouts.navegacion_rutero', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
          <?php endif; ?>

        </ul>
    </div>
</nav>
