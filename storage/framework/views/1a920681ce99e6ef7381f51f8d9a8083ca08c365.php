<?php $__env->startSection('title', 'Cumpleañeros'); ?>


<?php $__env->startSection("link"); ?>
<link href="<?php echo e(asset('css/rangedatepicker/daterangepicker.css')); ?>" rel="stylesheet"  media="screen">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>


<?php $__env->startSection('nombre','Cumpleañeros'); ?>
<?php $__env->startSection('ruta'); ?>
  <li class="active">
      <strong>Cumpleañeros</strong>
  </li>
<?php $__env->stopSection(); ?>


<?php
  use Carbon\Carbon;
  setlocale(LC_TIME, 'Spanish');
  Carbon::setUtf8(true);

?>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
    <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-sm-12">
                      <div class="ibox float-e-margins">
                        <div class="ibox-title">
                          <h5>Clientes cumpleañeros</h5>
                        </div>
                        <div class="ibox-content">
                          <div class="row">
                            <div class="col-md-6">
                              <label>Tipo:</label>
                                <select class="form-control" name="tipo" id="tipo">
                                  <option value="1">Activos</option>
                                  <option value="0">No activos</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                              <label>Rango:</label>
                                <div class="input-group">
                                  <div class="form-group">
                                    <input id="rangedate" class="form-control" type="text" name="" value="">
                                  </div>
                                  <span class="input-group-btn">
                                    <a id="busqueda_fecha" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                                  </span>
                              </div>
                            </div>
                            <div class="col-xs-12 col-md-12">
                              <div class="table-responsive">
                                <table id="tabla-cumple" class='table table-striped table-hover'>
                                  <thead>
                                    <tr>
                                      <th>Codigo</th>
                                      <th>Cliente</th>
                                      <th>Clasificacion</th>
                                      <th>Fecha cumpleaños</th>
                                      <th>Prestamo</th>
                                      <th>Monto</th>
                                      <th>Ruta</th>
                                      <th>Promotor</th>
                                      <th>Acciones</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php $__currentLoopData = $personas->sortBy('fecha_nacimiento'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $persona): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <?php if($persona->cliente->first() == null): ?>
                                      <?php else: ?>
                                        <tr>
                                          <td><a class="client-link" href="<?php echo e(route('clientes.show', $persona->cliente->first()->id)); ?>">Cli-<?php echo e($persona->cliente->first()->id); ?></a></td>
                                          <td><?php echo e($persona->nombre); ?> <?php echo e($persona->apellido); ?></td>
                                          <?php
                                            $ClienteClas = \App\Cliente::find($persona->cliente->first()->id);
                                            $ClasCliente = \App\Prestamo::where('cliente_id',$ClienteClas->id)->orderBy('id','DESC')->first();
                                          ?>
                                          <?php switch($ClasCliente->clasificacion_id ? $ClasCliente->clasificacion_id : 0):
                                            case (0): ?>
                                            <td><span class="badge badge-primary">A</span></td>
                                            <?php break; ?>
                                            <?php case (1): ?>
                                            <td><span class="badge badge-primary">A</span></td>
                                            <?php break; ?>
                                            <?php case (2): ?>
                                            <td><span class="badge badge-warning">B</span></td>
                                            <?php break; ?>
                                            <?php case (3): ?>
                                            <td><span class="badge badge-danger">C</span></td>
                                            <?php break; ?>
                                          <?php endswitch; ?>
                                          <td><?php echo e(Carbon::parse($persona->fecha_nacimiento)->format('d-m-Y')); ?></td>
                                          <td><?php echo e(isset($persona->cliente->first()->prestamos->last()->id) ? "Cre-".$persona->cliente->first()->prestamos->last()->id : "No hay"); ?></td>
                                          <td><?php echo e(isset($persona->cliente->first()->prestamos->last()->monto) ? $persona->cliente->first()->prestamos->last()->monto : "No hay"); ?></td>
                                          <td><?php echo e(isset($persona->cliente->first()->prestamos->last()->ruta) ? $persona->cliente->first()->prestamos->last()->ruta->first()->hoja_ruta->nombre : "No hay"); ?></td>
                                          <td><?php echo e(isset($persona->cliente->first()->prestamos->last()->ruta) ? $persona->cliente->first()->prestamos->last()->ruta->first()->hoja_ruta->user->persona->nombre . " " . $persona->cliente->first()->prestamos->first()->ruta->first()->hoja_ruta->user->persona->apellido : "No hay"); ?></td>
                                          
                                          
                                          <td><a class="btn btn-sm btn-outline btn-primary" href="<?php echo e(route('clientes.show',$persona->cliente->first()->id)); ?>"><span class="fa fa-eye"></span></a>
                                            <?php if(Auth::user()->hasRole('Administrador')): ?>
                                              <a value="<?php echo e($persona->cliente->first()->id); ?>" class="btn btn-outline btn-sm btn-danger quitar_cumple"><span class="fa fa-times"></span></a></td>
                                            <?php endif; ?>
                                        </tr>
                                      <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

  <script type="text/javascript" src="<?php echo e(asset('js/dataTables/datatables.min.js')); ?>"></script>
  <script type="text/javascript" src="<?php echo e(asset('js/moment/moment.js')); ?>"></script>
  <script type="text/javascript" src="<?php echo e(asset('js/rangedatepicker/daterangepicker.js')); ?>"></script>
  
  <script type="text/javascript" src="<?php echo e(asset('js/sweetalert/sweetalert.min.js')); ?>"></script>

<script type="text/javascript">
var oTable;
$(document).ready(function(){
  oTable = $('#tabla-cumple').DataTable({
    "language": {
        "url": "<?php echo e(asset('fonts/dataTablesEsp.json')); ?>",
    },
    "paging":   true,
    "info":     false,
    'dom' : 'tip',
    "processing": true,
    // "serverSide": true,
    dom: '<"html5buttons" B>tip',
    buttons: [
        {extend: 'excel',
        title: 'Creditos',
        },
        {extend: 'pdf',
        title: 'Creditos activos',
        },
        {extend: 'print',
         customize: function (win){
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');

                $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
        }
      },
    ]
  });

$('#busqueda-cumple').keyup(function(){
        oTable.search($(this).val()).draw();
  });

  $("#rangedate").daterangepicker({
    "locale": {
      "format": "YYYY-MM-DD",
      "fromLabel": "Desde",
      "toLabel": "Hasta",
      "applyLabel": "Aplicar",
      "cancelLabel": "Cancelar",
      "daysOfWeek": [
        "Dom",
        "Lun",
        "Mar",
        "Mie",
        "Jue",
        "Vie",
        "Sab"
      ],
      "monthNames": [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre"
      ],
    }
  });

  $("#busqueda_fecha").click(function(e){
    obtenerClientesCumple();
  });

  function obtenerClientesCumple(){
    var tabla = $("#tabla-cumple tbody")
    var rango_fechas = $("#rangedate").val();
    var valores = rango_fechas.split(' - ');
    var inicio = valores[0];
    var fin = valores[1];
    // console.log(inicio);
    var url = "<?php echo e(route('info.cumpleanios')); ?>";
    var tipo = $('#tipo :selected').val();
    $.ajax({
        type: "GET",
        url: url,
        data: {
          'inicio' : inicio,
          'fin'    : fin,
          'tipo'  : tipo,
      },
        success: function( response ) {
          var table = $('#tabla-cumple').DataTable();
          datos = response;
          table.clear().draw();
          console.log(response);
          if(response==null){

          } else{
            var url_cliente = "<?php echo e(route('clientes.show', ':id')); ?>";
            for (var i = 0; i < datos.length; i++) {
              var datos_ac = datos[i];
              var url2 = url_cliente;
              var temp = "";
              url2 = url2.replace(':id', datos_ac.codigo);
              if(datos_ac.tipo == 1){
                temp = "<a class='btn btn-outline btn-sm btn-success' href=" + url2 + "><span class='fa fa-eye'></span></a><a value=" + datos_ac.codigo +" class='btn btn-outline btn-sm btn-danger quitar_cumple'><span class='fa fa-times'></span></a>";
              } else{
                temp = "<a class='btn btn-outline btn-sm btn-success' href=" + url2 + "><span class='fa fa-eye'></span></a>";
              }
              table.row.add([
                'Cre-' + datos_ac.codigo,
                datos_ac.cliente,
                datos_ac.clasificacion,
                datos_ac.fecha,
                datos_ac.prestamo,
                datos_ac.monto,
                datos_ac.hoja_ruta,
                datos_ac.promotor,
                temp,
              ]).draw();
            }
          }
        }
    });
  }

  $('#tabla-cumple').on('click','.quitar_cumple',function(){
    var id = $(this).attr('value');
    swal({
      title: "Esta seguro de quitar al cliente de la lista de cumpleañeros?",
      text: "el cliente sera quitado de la lista",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        quitarCumple(id);
        // $("#formulario-del").submit();
      } else {
        swal("Cliente sigue en la lista", {
          icon: "warning"
        });
      }
    });
  });
  function quitarCumple(id){
    var url = "<?php echo e(route('clientes.quitarCumple',':id')); ?>";
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    url = url.replace(':id',id);
    $.ajax({
      type: "POST",
      url: url,
      data: {_token: CSRF_TOKEN},
      // dataType: 'JSON',
      success: function () {
        swal("Cliente quitado de la lista", {
          icon: "success",
        });
        obtenerClientesCumple();
      }
    });
  }
});


</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>