<?php $__env->startSection('title', 'Clientes'); ?>



<?php $__env->startSection('content'); ?>



<?php $__env->startSection('nombre','Clientes'); ?>

<?php $__env->startSection('ruta'); ?>

  <li class="active">

      <strong>Listado de clientes</strong>

  </li>

<?php $__env->stopSection(); ?>

<div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">

                <div class="col-sm-8">

                  <div class="ibox float-e-margins">

                    <div class="ibox-title">

                      <h5>Listado de clientes</h5>

                    </div>

                    <div class="ibox-content">

                      <div class="tabs-container">

                        <ul class="nav nav-tabs">

                          <li class="active"><a data-toggle="tab" href="#clientes">Clientes</a></li>

                          <li><a data-toggle="tab" href="#listaNegra">Lista de bloqueados</a></li>

                        </ul>

                        <div class="tab-content">

                          <div id="clientes" class="tab-pane active">

                            <div class="input-group">

                              <input type="text" placeholder="Codigo: Cli-123, Cliente: Nombre, Prestamo: Cre-123" class="input form-control" id="busqueda-clientes">

                              <span class="input-group-btn">

                                <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Buscar</button>

                              </span>

                            </div>

                            <ul class="nav">

                              <span class="pull-left text-muted"> <?php echo e(count($clientes)); ?> Clientes</span>

                            </ul>

                            <div class="table-responsive">

                              <table class='table table-striped table-hover' id="tabla-clientes">

                                <thead>

                                  <tr>

                                    <th>Codigo</th>

                                    <th>Clasificación</th>

                                    <th>Nombre</th>

                                    <th>DPI</th>

                                    <th>Prestamo</th>

                                    <th>Acciones</th>

                                  </tr>

                                </thead>

                                

                                  </table>

                                </div>

                          </div>

                          <div id="listaNegra" class="tab-pane">

                            <div class="input-group">

                              <input type="text" placeholder="Codigo: Cre-123, Cliente: Nombre del cliente" class="input form-control" id="busqueda-clientes-listaNegra">

                              <span class="input-group-btn">

                                <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Buscar</button>

                              </span>

                            </div>

                            <ul class="nav">

                              <span class="pull-left text-muted"> <?php echo e(count($clientes_listaNegra)); ?> Clientes</span>

                            </ul>

                            <div class="table-responsive">

                              <table class='table table-striped table-hover' id="tabla-cliente-listaNegra">

                                <thead>

                                  <tr>

                                    <th>Codigo</th>

                                    <th>Nombre</th>

                                    <th>DPI</th>

                                    <th>Prestamo</th>

                                    <th>Acciones</th>

                                  </tr>

                                </thead>

                                <tbody>

                                  <?php if($clientes_listaNegra->isEmpty()): ?>

                                    <h2> No hay clientes </h2>

                                  <?php else: ?>

                                    <?php $__currentLoopData = $clientes_listaNegra; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cliente): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                      <tr>

                                        <td><a class="client-link" value='<?php echo e($cliente->id); ?>'>Cli-<?php echo e($cliente->id); ?></a></td>

                                        <td><?php echo e($cliente->persona->nombre); ?> <?php echo e($cliente->persona->apellido); ?></td>

                                        <td><?php echo e($cliente->persona->dpi); ?></td>

                                          <td><?php echo e((isset($cliente->prestamos->last()->id)) ? "Cred-" .$cliente->prestamos->last()->id : "No hay"); ?>


                                            

                                            <?php echo ($cliente->prestamos->last()!==null) ? $cliente->prestamos->last()->obtenerEstado2() : "No hay status"; ?></td>

                                            <td class="project-actions">

                                              <a href="<?php echo e(route('clientes.show',$cliente->id)); ?>" class="btn btn-outline btn-success btn-sm"><i class="fa fa-eye"></i></a>

                                              <?php if(Auth::user()->hasRole('Administrador')): ?>

                                                <a href="<?php echo e(route('clientes.edit',$cliente->id)); ?>" class="btn btn-outline btn-info btn-sm"><i class="fa fa-pencil"></i></a>

                                              <?php endif; ?>

                                            </td>

                                          </tr>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                      <?php endif; ?>

                                    </tbody>

                                  </table>

                                </div>

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>





                </div>

                <div class="col-sm-4">

                  <div class="ibox ">

                      <div id='resumen_perfil' class="ibox-content">

                      <div class="tab-content">

                        <?php if($clientes->isEmpty()): ?>

                          <h3>No hay clientes</h3>

                        <?php else: ?>

                        <div id="contact-1" class="tab-pane active">

                            <div class="row m-b-lg">

                                <div class="col-lg-12 text-center">

                                    <h2><?php echo e($clientes->first()->persona->nombre); ?> <?php echo e($clientes->first()->persona->apellido); ?></h2>

                                </div>

                            </div>

                            <div class="client-detail">

                              <div class="full-height-scroll">



                                  <strong>Datos Personales</strong>



                                  <ul class="list-group clear-list">

                                      <li class="list-group-item fist-item">

                                          <span class="pull-right"> 24 </span>

                                          Edad

                                      </li>

                                      <li class="list-group-item fist-item">

                                          <span class="pull-right"><?php echo e($clientes->first()->persona->dpi); ?></span>

                                          Dpi

                                      </li>

                                      <li class="list-group-item">

                                          <span class="pull-right"><?php echo e($clientes->first()->persona->nit); ?></span>

                                          Nit

                                      </li>

                                      <li class="list-group-item">

                                          <span class="pull-right"><?php echo e($clientes->first()->persona->telefono); ?></span>

                                          Telefono

                                      </li>

                                      <li class="list-group-item">

                                          <span class="pull-right"><?php echo e($clientes->first()->persona->celular1); ?></span>

                                          Celular 1

                                      </li>

                                      <li class="list-group-item">

                                          <span class="pull-right"><?php echo e($clientes->first()->persona->celular2); ?></span>

                                          Celular 2

                                      </li>

                                  </ul>

                                  <strong>Prestamos</strong>

                                  <table class='table table-striped table-hover'>

                                    <thead>

                                      <tr>

                                        <th>Codigo</th>

                                        <th>Estado</th>

                                        <th>Clasificacion</th>

                                      </tr>

                                    </thead>

                                    <tbody>

                                      <?php $__currentLoopData = $clientes->first()->prestamos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prestamo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                          <tr>

                                            <td><?php echo e($prestamo->id); ?></td>

                                            <?php echo $prestamo->obtenerEstado(); ?>


                                            <?php echo $prestamo->obtenerClasificacion(); ?>


                                          </tr>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </tbody>



                                  </table>



                              </div>

                            </div>

                        </div>

                      <?php endif; ?>



                      </div>



                    </div>

                  </div>

                </div>

            </div>

        </div>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('scripts'); ?>



<script type="text/javascript" src="<?php echo e(asset('js/dataTables/datatables.min.js')); ?>"></script>





<script type="text/javascript">



$(document).ready(function(){


    $.fn.dataTable.ext.errMode = 'none';
      var oTable = $('#tabla-clientes').DataTable({

        "order": [[ 0, "desc" ]],

        "language": {

          "url": '<?php echo e(asset('fonts/dataTablesEsp.json')); ?>',

          },

        "paging":   true,

        "info":     false,

        'dom' : 'tip',

        processing: true,

        serverSide: true,

        ajax: '<?php echo e(url('dataClientes')); ?>',

        columns: [

            {data: 'Codigo', name: 'Codigo'},

            {data: 'clasificacion', name: 'clasificacion', className: 'text-center'},

            {data: 'Nombre', name: 'Nombre'},

            {data: 'DPI', name: 'DPI'},

            {data: 'Prestamo', name: 'Prestamo'},

            {data: 'Acciones', name: 'Acciones'},

        ]

      });



      $('#busqueda-clientes').keyup(function(){

            oTable.search($(this).val()).draw() ;

      });



      var oTable2 = $('#tabla-cliente-listaNegra').DataTable({

        "language": {

          "url": '<?php echo e(asset('fonts/dataTablesEsp.json')); ?>',

          },

        "paging":   true,

        "info":     false,

        'dom' : 'tip',

      });



      $('#busqueda-clientes-listaNegra').keyup(function(){

            oTable2.search($(this).val()).draw() ;

      });





    	$("#tabla-clientes tbody").on('click', '.client-link',function(){

          var valor = $(this).attr("value");

          var url = '<?php echo e(route("clientes.show", ":id")); ?>';

          url = url.replace(':id', valor);

          $.ajax({

              type: "GET",

              url: url,

              success: function( response ) {

                $('#resumen_perfil').html(response);

              }

          });



    	});

});



</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>