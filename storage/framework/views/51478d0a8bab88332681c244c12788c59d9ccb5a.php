    

<?php $__env->startSection('meta'); ?>
  

<?php $__env->stopSection(); ?>
<?php $__env->startSection('title', 'Credito'); ?>

<?php $__env->startSection('content'); ?>

<?php $__env->startSection('nombre','Datos de credito'); ?>
<?php $__env->startSection('ruta'); ?>
  <li class="active">
      <strong>Credito</strong>
  </li>
<?php $__env->stopSection(); ?>
<?php
  use Carbon\Carbon;
  setlocale(LC_TIME, 'es_AR.utf8');
  Carbon::setUtf8(false);
?>

<?php $__env->startSection('link'); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.1.1/bootstrap-social.css">
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Creditos.pagoModal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('Creditos.comentario', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

<body class=" pace-done ">
  <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
              <div class="col-sm-4">
                  <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Datos de credito</h5>
                            <?php echo $credito->obtenerClasificacion2(); ?>

                        </div>
                        <div>
                            <div class="ibox-content profile-content">
                              <h4>Codigo: <?php echo e($credito->id); ?></h4>
                              <strong class="text-success">Datos de credito:</strong>

                              <ul class="list-group clear-list">
                                <li class="list-group-item fist-item">
                                    <span class="pull-right"> <?php echo e($credito->monto); ?> </span>
                                    Monto
                                </li>
                                  <li class="list-group-item">
                                      <span class="pull-right"> <?php echo e($credito->plan->mora); ?> </span>
                                      Mora por retraso
                                  </li>
                                  <li class="list-group-item">
                                      <span class="pull-right"> <?php echo e($credito->plan->periodo->tiempo); ?> </span>
                                      No. de pagos
                                  </li>
                                  <li class="list-group-item">
                                      <span class="pull-right"> <?php echo e($credito->plan->nombre); ?></span>
                                      Plan
                                  </li>
                                  <li class="list-group-item">
                                      <span class="pull-right"> <?php echo e($credito->ruta->first()->hoja_ruta->nombre); ?></span>
                                      Hoja de ruta
                                  </li>
                              </ul>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-lg-12">
                      <div class="ibox float-e-margins">
                          <div class="ibox-title">
                              <h5>Datos del cliente</h5>
                              <div class="ibox-tools">
                                  <a class="collapse-link">
                                      <i class="fa fa-chevron-up"></i>
                                  </a>
                              </div>
                          </div>
                          <div class="ibox-content">
                            <ul class="list-group clear-list">
                              <li class="list-group-item fist-item">
                                  <span class="pull-right">
                                    <a href="<?php echo e(route('clientes.show', $credito->cliente->id)); ?>" class="client-link"> <?php echo e($credito->cliente->persona->nombre); ?> <?php echo e($credito->cliente->persona->apellido); ?> </span> </a>
                                    Nombre
                              </li>
                                <li class="list-group-item">
                                    <span class="pull-right"> <?php echo e($credito->cliente->id); ?> </span>
                                    Codigo
                                </li>
                                <li class="list-group-item">
                                    <span class="pull-right">
                                      <?php echo e($credito->cliente->persona->telefono ? $credito->cliente->persona->telefono : $credito->cliente->persona->celular1); ?> </span>
                                    Telefono
                                </li>
                            </ul>

                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-sm-8">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">
                          <h5>Ficha de pago</h5>
                          <div class="ibox-tools">
                            <?php if(Auth::user()->hasAnyRole(['Administrador','Supervisor'])): ?>
                              
                            <?php endif; ?>

                          </div>
                      </div>
                      <div class="ibox-content">
                        <div class="tabs-container">
                          <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#ficha_pago">Ficha</a></li>
                            <li><a data-toggle="tab" href="#informacion">Informacion</a></li>
                            <?php if(Auth::user()->hasAnyRole(['Administrador','Secretaria','Supervisor'])): ?>
                            <li><a data-toggle="tab" href="#documentos">Documento</a></li>
                            <?php endif; ?>
                            <?php if(Auth::user()->hasAnyRole(['Administrador','Secretaria','Supervisor'])): ?>
                            <li><a data-toggle="tab" href="#pagos">Pagos</a></li>
                            <?php endif; ?>
                            <?php if(Auth::user()->hasAnyRole(['Administrador','Secretaria','Supervisor'])): ?>
                            <li><a data-toggle="tab" href="#compromiso">Compromiso</a></li>
                            <?php endif; ?>
                            <?php if(Auth::user()->hasAnyRole(['Administrador','Secretaria','Supervisor'])): ?>
                              <li><a data-toggle="tab" href="#imagenes">Imagenes</a></li>
                            <?php endif; ?>
                          </ul>
                          <div class="tab-content">
                            <div id="ficha_pago" class="tab-pane active">
                                <div class="table-responsive">
                                  <table class='table '>
                                  <thead>
                                    <tr>
                                      <th>No.</th>
                                      <th>Fecha</th>
                                      <th>Cuota</th>
                                      <th>Mora</th>
                                      <th>Total Pago</th>
                                      <th>Estado</th>
                                      <th>Monto pagado</th>
                                      <th>Fecha ingreso</th>
                                      <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                        <th>Acciones</th>
                                      <?php endif; ?>
                                    </tr>
                                  </thead>
                                  <?php if(is_null($credito->ficha_pago)): ?>

                                  <?php else: ?>
                                    <tbody>
                                      <?php
                                        $contador = 0;
                                        $pendiente = $credito->fichas_reales()->where('estado_p',0)->first();
                                        $noefectuado = $credito->fichas_reales()->where('estado',0)->where('estado_p', '!=', 5)->first();
                                        $pagoAdelantado = $credito->fichas_reales()->where('estado',0)->where('estado_p', 5)->first();
                                        $diaPago=0;
                                        //$revocado=$credito->fichas_reales()->where('estado',1)->last();
                                        $revocado=\App\Ficha_Pago::where('prestamo_id',$credito->id)->where('estado',1)->orderBy('id','desc')->first();
                                        $revocadoTodos=$credito->fichas_reales()->where('estado',1)->toArray();
                                        $idRevocado=0;
                                        $ultimopago = $credito->fichas_reales()->last();
                                        $restriccionEliminar = $credito->fichas_reales();



                                         // dd($ultimopago);
                                        if (isset($noefectuado)) {
                                          $diaPago=$noefectuado->no_dia+1;
                                        }
                                        $DiaSigAdelantado = 0;
                                        if (isset($pagoAdelantado)) {
                                          $DiaSigAdelantado = $pagoAdelantado->no_dia+1;
                                        }
                                        if (isset($revocado)) {
                                          $idRevocado=$revocado->no_dia - 1;
                                        }
                                        // dd($DiaSigAdelantado);
                                       // $credito = $credito->fichas_reales()->sortByDesc('fecha')
                                      ?>

                                        
                                      <?php $__currentLoopData = $credito->fichas_reales()->sortBy('id'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ficha_pago): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        

                                        <tr>
                                          <td><?php echo e($ficha_pago->no_dia); ?></td>
                                          <td >
                                            
                                           <?php echo e($ficha_pago->fecha); ?>

                                          </td>
                                          <td><?php echo e($ficha_pago->cuota - $ficha_pago->mora); ?></td>
                                          <td><?php echo e($ficha_pago->mora); ?></td>
                                          <td class="text-center"><B><?php echo e($ficha_pago->cuota); ?></B></td>
                                          <?php switch($ficha_pago->estado_p ? $ficha_pago->estado_p : 0):
                                            case (0): ?>

                                              <td><a><span class="badge badge-warning pull-center">Pendiente</span></a></td>
                                              <td>0</td>
                                              <td>
                                                  <?php echo e(isset($ficha_pago->pago) ? $ficha_pago->pago->created_at : "Sin fecha"); ?>

                                              </td>
                                              <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor') or Auth::user()->hasRole('Promotor')): ?>
                                                <?php if($ficha_pago->id == $pendiente->id && $ficha_pago->no_dia != $diaPago && $ficha_pago->no_dia != $DiaSigAdelantado): ?>

                                                  <td class="project-actions">
                                                    <?php
                                                      $comentarios=\App\Comentario::all();
                                                    ?>

                                                      <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                          <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <a class="btn btn-primary btn-outline btn-sm pagar-btn" type="button" value="<?php echo e($ficha_pago->id); ?>"
                                                        data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fa"><strong>Q</strong></i></a>
                                                        
                                                        <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                        <a class="btn btn-success btn-sm" disabled><i class="fa fa-reply"></i></a>
                                                        <?php
                                                        $diaAnterior = $ficha_pago->no_dia - 1;

                                                        ?>

                                                        <?php if(isset($diaAnterior)): ?>

                                                          <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 0): ?>

                                                            <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                          <?php endif; ?>
                                                          <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado') == 1): ?>
                                                            <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                          <?php endif; ?>

                                                        <?php else: ?>
                                                          <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                            <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                          <?php endif; ?>
                                                        <?php endif; ?>
                                                        <?php endif; ?>
                                                  </td>
                                                <?php else: ?>
                                                  <td class="project-actions">
                                                    <?php
                                                      $comentarios=\App\Comentario::all();
                                                    ?>

                                                      <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                          <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                  <a class="btn btn-primary btn-outline btn-sm pagar-btn" type="button"
                                                      disabled><i class="fa"><strong>Q</strong></i></a>
                                                      
                                                      <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                      <a class="btn btn-success btn-sm" disabled><i class="fa fa-reply"></i></a>
                                                      <?php if($ficha_pago->no_dia == $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->no_dia): ?>
                                                        <?php
                                                        $diaAnterior = $ficha_pago->no_dia - 1;

                                                        ?>
                                                        <?php if(isset($diaAnterior)): ?>
                                                          <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 0): ?>
                                                            <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                          <?php endif; ?>
                                                        <?php else: ?>
                                                          <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                            <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                          <?php endif; ?>
                                                        <?php endif; ?>
                                                      <?php endif; ?>
                                                      <?php endif; ?>
                                                      </td>
                                                <?php endif; ?>
                                              <?php endif; ?>
                                            <?php break; ?>
                                            <?php case (1): ?>
                                              <td><a><span class="badge badge-primary pull-center">Pagado</span></a></td>
                                              <td>
                                                <?php if(isset($ficha_pago->pago)): ?>
                                                  <?php echo e(isset($ficha_pago->total_pago) ? $ficha_pago->total_pago : $ficha_pago->pago->monto); ?>

                                                <?php else: ?>
                                                  0
                                                <?php endif; ?>
                                              </td>
                                              <td>
                                                  <?php echo e(isset($ficha_pago->pago) ? $ficha_pago->pago->created_at : "Sin fecha"); ?>

                                              </td>
                                              
                                              <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor') or Auth::user()->hasRole('Promotor')): ?>
                                                <?php if($credito->estado_p_id !=9): ?>
                                                  <?php if($ficha_pago->id == $revocado->id): ?>
                                                  <td class="project-actions">
                                                    <?php
                                                      $comentarios=\App\Comentario::all();
                                                    ?>

                                                      <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                          <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <a class="btn btn-outline btn-primary btn-sm" disabled><i class="fa"></i><strong>Q</strong></a>
                                                    <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                    <a value="<?php echo e($ficha_pago->id); ?>" class="btn btn-success btn-outline btn-sm revertir"><i class="fa fa-reply"></i></a>
                                                    <?php
                                                    $diaAnterior = $ficha_pago->no_dia - 1;

                                                    ?>
                                                    <?php if(isset($diaAnterior)): ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 1): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php else: ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                  </td>
                                                  <?php elseif($ficha_pago->id == $ultimopago->id): ?>

                                                    <td class="project-actions">
                                                      <?php
                                                        $comentarios=\App\Comentario::all();
                                                      ?>

                                                        <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                          <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                            <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                          <?php endif; ?>
                                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      <a class="btn btn-outline btn-primary btn-sm" disabled><i class="fa"></i><strong>Q</strong></a>
                                                      <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                      <a value="<?php echo e($ficha_pago->id); ?>" class="btn btn-success btn-outline btn-sm revertir"><i class="fa fa-reply"></i></a>
                                                      <?php
                                                      $diaAnterior = $ficha_pago->no_dia - 1;

                                                      ?>
                                                      <?php if(isset($diaAnterior)): ?>
                                                        <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 0): ?>
                                                          <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                        <?php endif; ?>
                                                      <?php else: ?>
                                                        <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                          <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                        <?php endif; ?>
                                                      <?php endif; ?>
                                                      <?php endif; ?>
                                                    </td>
                                                  <?php else: ?>

                                                    <td class="project-actions">
                                                      <?php
                                                        $comentarios=\App\Comentario::all();
                                                      ?>

                                                        <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                          <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                            <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                          <?php endif; ?>
                                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      <a class="btn btn-outline btn-primary btn-sm" disabled><i class="fa"></i><strong>Q</strong></a>
                                                      <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                      <a class="btn btn-success btn-outline btn-sm" disabled><i class="fa fa-reply"></i></a>
                                                      <?php
                                                      $diaAnterior = $ficha_pago->no_dia - 1;

                                                      ?>
                                                      <?php if(isset($diaAnterior)): ?>
                                                        <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 0): ?>
                                                          <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                        <?php endif; ?>
                                                      <?php else: ?>
                                                        <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                          <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                        <?php endif; ?>
                                                      <?php endif; ?>
                                                      <?php endif; ?>
                                                    </td>
                                                  <?php endif; ?>
                                                <?php else: ?>
                                                  <td class="project-actions">
                                                    <?php
                                                      $comentarios=\App\Comentario::all();
                                                    ?>

                                                      <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                          <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <a class="btn btn-outline btn-primary btn-sm" disabled><i class="fa"></i><strong>Q</strong></a>
                                                    <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                    <a value="<?php echo e($ficha_pago->id); ?>" class="btn btn-success btn-outline btn-sm revertir"><i class="fa fa-reply"></i></a>
                                                    <?php
                                                    $diaAnterior = $ficha_pago->no_dia - 1;

                                                    ?>
                                                    <?php if(isset($diaAnterior)): ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 0): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php else: ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                  </td>
                                                <?php endif; ?>
                                              <?php endif; ?>
                                            <?php break; ?>
                                            <?php case (2): ?>
                                            <?php if($contador == 0): ?>
                                              <?php if($ficha_pago->cont == 1): ?>
                                                <td><a><span class="badge badge-danger pull-center">No Pagado</span></a></td>
                                                <td>0</td>
                                                <td>
                                                    <?php echo e(isset($ficha_pago->updated_at) ? $ficha_pago->updated_at : "Sin fecha"); ?>

                                                </td>
                                              <?php else: ?>
                                                <td><a><span class="badge badge pull-center">Perdon</span></a></td>
                                                <td>0</td>
                                                <td>
                                                    <?php echo e(isset($ficha_pago->updated_at) ? $ficha_pago->updated_at : "Sin fecha"); ?>

                                                </td>
                                                <?php
                                                  $contador = $contador + 1;
                                                ?>
                                              <?php endif; ?>
                                            <?php else: ?>
                                              <td><a><span class="badge badge-danger pull-center">No Pagado</span></a></td>
                                              <td>0</td>
                                              <td>
                                                  <?php echo e(isset($ficha_pago->updated_at) ? $ficha_pago->updated_at : "Sin fecha"); ?>

                                              </td>
                                            <?php endif; ?>
                                              <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor') or Auth::user()->hasRole('Promotor')): ?>
                                                <?php if($credito->estado_p_id !=9): ?>
                                                  <?php if($ficha_pago->id == $revocado->id): ?>
                                                  <td class="project-actions">
                                                    <?php
                                                      $comentarios=\App\Comentario::all();
                                                    ?>

                                                      <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                          <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <a class="btn btn-outline btn-primary btn-sm" type="button" value="<?php echo e($ficha_pago->id); ?>"
                                                              disabled><i class="fa"></i><strong>Q</strong></a>
                                                              
                                                      <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                    <a value="<?php echo e($ficha_pago->id); ?>" class="btn btn-success btn-outline btn-sm revertir"><i class="fa fa-reply"></i></a>
                                                    <?php
                                                    $diaAnterior = $ficha_pago->no_dia - 1;

                                                    ?>
                                                    <?php if(isset($diaAnterior)): ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 2): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php else: ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                  </td>
                                                <?php else: ?>
                                                  <td class="project-actions">
                                                    <?php
                                                      $comentarios=\App\Comentario::all();
                                                    ?>

                                                      <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                          <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <a class="btn btn-outline btn-primary btn-sm" disabled><i class="fa"></i><strong>Q</strong></a>
                                                    <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                    <a class="btn btn-success btn-outline btn-sm" disabled><i class="fa fa-reply"></i></a>
                                                    <?php
                                                    $diaAnterior = $ficha_pago->no_dia - 1;

                                                    ?>
                                                    <?php if(isset($diaAnterior)): ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 0): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php else: ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                  </td>
                                                  <?php endif; ?>
                                                <?php else: ?>
                                                  <td class="project-actions">
                                                    <?php
                                                      $comentarios=\App\Comentario::all();
                                                    ?>

                                                      <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                          <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <a class="btn btn-outline btn-primary btn-sm" disabled><i class="fa"></i><strong>Q</strong></a>
                                                    <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                    <a class="btn btn-success btn-outline btn-sm" disabled><i class="fa fa-reply"></i></a>
                                                    <?php
                                                    $diaAnterior = $ficha_pago->no_dia - 1;

                                                    ?>
                                                    <?php if(isset($diaAnterior)): ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 0): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php else: ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                  </td>
                                                <?php endif; ?>
                                              <?php endif; ?>
                                            <?php break; ?>
                                            <?php case (3): ?>
                                              <td><a><span class="badge badge-info pull-center">Pago parcial</span></a></td>
                                              <td><?php echo e($ficha_pago->pago ? $ficha_pago->pago->monto : 0); ?></td>
                                              <td>
                                                  <?php echo e(isset($ficha_pago->pago) ? $ficha_pago->pago->created_at : "Sin fecha"); ?>

                                              </td>
                                              <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor') or Auth::user()->hasRole('Promotor')): ?>
                                                <?php if($credito->estado_p_id !=9): ?>
                                                  <?php if($ficha_pago->id == $revocado->id): ?>
                                                  <td class="project-actions">
                                                    <?php
                                                      $comentarios=\App\Comentario::all();
                                                    ?>

                                                      <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                          <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <a class="btn btn-outline btn-primary btn-sm" disabled><i class="fa"></i><strong>Q</strong></a>
                                                    <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                    <a value="<?php echo e($ficha_pago->id); ?>" class="btn btn-success btn-outline btn-sm revertir"><i class="fa fa-reply"></i></a>
                                                    <?php
                                                    $diaAnterior = $ficha_pago->no_dia - 1;

                                                    ?>
                                                    <?php if(isset($diaAnterior)): ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 3): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php else: ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                  </td>
                                                <?php else: ?>
                                                  <td class="project-actions">
                                                    <?php
                                                      $comentarios=\App\Comentario::all();
                                                    ?>

                                                      <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                          <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <a class="btn btn-outline btn-primary btn-sm" disabled><i class="fa"></i><strong>Q</strong></a>
                                                    <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                    <a class="btn btn-success btn-outline btn-sm" disabled><i class="fa fa-reply"></i></a>
                                                    <?php
                                                    $diaAnterior = $ficha_pago->no_dia - 1;

                                                    ?>
                                                    <?php if(isset($diaAnterior)): ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 0): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php else: ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                  </td>
                                                  <?php endif; ?>
                                                <?php else: ?>
                                                  <td class="project-actions">
                                                    <?php
                                                      $comentarios=\App\Comentario::all();
                                                    ?>

                                                      <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                          <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <a class="btn btn-outline btn-primary btn-sm" disabled><i class="fa"></i><strong>Q</strong></a>
                                                    <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                    <a class="btn btn-success btn-outline btn-sm" disabled><i class="fa fa-reply"></i></a>
                                                    <?php
                                                    $diaAnterior = $ficha_pago->no_dia - 1;

                                                    ?>
                                                    <?php if(isset($diaAnterior)): ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 0): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php else: ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                  </td>
                                                <?php endif; ?>
                                              <?php endif; ?>
                                            <?php break; ?>
                                            <?php case (4): ?>
                                              <td><a><span class="badge badge-success pull-center">Pago adelantado</span></a></td>
                                              
                                              <td><button class="btn btn-info btn-circle" type="button"><i class="fa fa-check"></i>
                            </button></td>
                            <td>
                                <?php echo e(isset($ficha_pago->pago) ? $ficha_pago->pago->created_at : "Sin fecha"); ?>

                            </td>
                                              <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor') or Auth::user()->hasRole('Promotor')): ?>
                                                <?php if($credito->estado_p_id !=9): ?>
                                                  <?php if($ficha_pago->id == $revocado->id): ?>
                                                  <td class="project-actions">
                                                    <?php
                                                      $comentarios=\App\Comentario::all();
                                                    ?>

                                                      <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                          <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <a class="btn btn-outline btn-primary btn-sm" disabled><i class="fa"><strong>Q</strong></i></a>
                                                    <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                    <a value="<?php echo e($ficha_pago->id); ?>" class="btn btn-success btn-outline btn-sm revertir"><i class="fa fa-reply"></i></a>
                                                    <?php
                                                    $diaAnterior = $ficha_pago->no_dia - 1;

                                                    ?>
                                                    <?php if(isset($diaAnterior)): ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 4): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php else: ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                  </td>
                                                <?php else: ?>
                                                <td class="project-actions">
                                                  <?php
                                                    $comentarios=\App\Comentario::all();
                                                  ?>

                                                    <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                      <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                        <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                      <?php endif; ?>
                                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                  <a class="btn btn-outline btn-primary btn-sm" disabled><i class="fa"></i><strong>Q</strong></a>
                                                  <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                  <a class="btn btn-success btn-outline btn-sm" disabled><i class="fa fa-reply"></i></a>
                                                  <?php
                                                  $diaAnterior = $ficha_pago->no_dia - 1;

                                                  ?>
                                                  <?php if(isset($diaAnterior)): ?>
                                                    <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 0): ?>
                                                      <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                    <?php endif; ?>
                                                  <?php else: ?>
                                                    <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                      <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                    <?php endif; ?>
                                                  <?php endif; ?>
                                                  <?php endif; ?>
                                                </td>
                                              <?php endif; ?>
                                                <?php else: ?>
                                                  <td class="project-actions">
                                                    <?php
                                                      $comentarios=\App\Comentario::all();
                                                    ?>

                                                      <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                          <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <a class="btn btn-outline btn-primary btn-sm" disabled><i class="fa"></i><strong>Q</strong></a>
                                                    <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                    <a class="btn btn-success btn-outline btn-sm" disabled><i class="fa fa-reply"></i></a>
                                                    <?php
                                                    $diaAnterior = $ficha_pago->no_dia - 1;

                                                    ?>
                                                    <?php if(isset($diaAnterior)): ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 0): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php else: ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                  </td>
                                                <?php endif; ?>
                                              <?php endif; ?>
                                            <?php break; ?>
                                            <?php case (5): ?>
                                              <td><a><span class="badge badge-success pull-center">Parcial adelantado</span></a></td>
                                              <td><a><span class="badge badge-primary pull-center"><i class="fa fa-star-half-empty"></i>  <?php echo e($ficha_pago->pago ? $ficha_pago->pago->monto : 0); ?></span></a></td>
                                              <td>
                                                  <?php echo e(isset($ficha_pago->pago) ? $ficha_pago->pago->created_at : "Sin fecha"); ?>

                                              </td>
                                              <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor') or Auth::user()->hasRole('Promotor')): ?>
                                                <?php if($credito->estado_p_id !=9): ?>
                                                  <?php if($ficha_pago->id == $pagoAdelantado->id): ?>
                                                    <td class="project-actions">
                                                      <?php
                                                        $comentarios=\App\Comentario::all();
                                                      ?>

                                                        <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                          <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                            <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                          <?php endif; ?>
                                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      <a class="btn  btn-outline btn-primary btn-sm pagar-btn" type="button" value="<?php echo e($ficha_pago->id); ?>"
                                                          data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fa"></i><strong>Q</strong></a>
                                                          <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                      <a class="btn btn-success btn-outline btn-sm" disabled><i class="fa fa-reply"></i></a>
                                                      <?php
                                                      $diaAnterior = $ficha_pago->no_dia - 1;

                                                      ?>
                                                      <?php if(isset($diaAnterior)): ?>
                                                        <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 5): ?>
                                                          <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                        <?php endif; ?>
                                                      <?php else: ?>
                                                        <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                          <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                        <?php endif; ?>
                                                      <?php endif; ?>
                                                      <?php endif; ?>
                                                    </td>

                                                  <?php elseif($ficha_pago->id == $revocado->id): ?>
                                                    <td class="project-actions">
                                                      <?php
                                                        $comentarios=\App\Comentario::all();
                                                      ?>

                                                        <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                          <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                            <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                          <?php endif; ?>
                                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      <a class="btn  btn-outline btn-primary btn-sm pagar-btn" type="button" value="<?php echo e($ficha_pago->id); ?>"
                                                          data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fa"></i><strong>Q</strong></a>
                                                        <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                      <a value="<?php echo e($ficha_pago->id); ?>" class="btn btn-outline btn-success btn-sm revertir"><i class="fa fa-reply" ></i></a>
                                                      <?php
                                                      $diaAnterior = $ficha_pago->no_dia - 1;

                                                      ?>
                                                      <?php if(isset($diaAnterior)): ?>
                                                        <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 0): ?>
                                                          <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                        <?php endif; ?>
                                                      <?php else: ?>
                                                        <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                          <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                        <?php endif; ?>
                                                      <?php endif; ?>
                                                      <?php endif; ?>
                                                    </td>
                                                  <?php else: ?>
                                                    <td class="project-actions">
                                                      <?php
                                                        $comentarios=\App\Comentario::all();
                                                      ?>

                                                        <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                          <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                            <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                          <?php endif; ?>
                                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      <a class="btn btn-outline btn-primary btn-sm" disabled><i class="fa"></i><strong>Q</strong></a>
                                                      <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                      <a class="btn btn-success btn-outline btn-sm" disabled><i class="fa fa-reply"></i></a>
                                                      <?php
                                                      $diaAnterior = $ficha_pago->no_dia - 1;

                                                      ?>
                                                      <?php if(isset($diaAnterior)): ?>
                                                        <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 0): ?>
                                                          <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                        <?php endif; ?>
                                                      <?php else: ?>
                                                        <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                          <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                        <?php endif; ?>

                                                      <?php endif; ?>
                                                      <?php endif; ?>
                                                    </td>
                                                  <?php endif; ?>
                                                <?php else: ?>
                                                  <td class="project-actions">
                                                    <?php
                                                      $comentarios=\App\Comentario::all();
                                                    ?>

                                                      <?php $__currentLoopData = $comentarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($coment->commentable_id==$ficha_pago->id): ?>
                                                          <a value="<?php echo e($coment->commentable_id); ?>" class="btn btn-success btn-outline btn-sm comentario" data-toggle="modal" data-target=".bd-example-modal-lg-comentario"><i class="fa fa-comments"></i></a>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <a class="btn btn-outline btn-primary btn-sm" disabled><i class="fa"></i><strong>Q</strong></a>
                                                    <?php if(Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                                    <a class="btn btn-success btn-outline btn-sm" disabled><i class="fa fa-reply"></i></a>
                                                    <?php
                                                    $diaAnterior = $ficha_pago->no_dia - 1;

                                                    ?>
                                                    <?php if(isset($diaAnterior)): ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0  && $ficha_pago->where('no_dia', $diaAnterior)->value('estado_p') == 5): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php else: ?>
                                                      <?php if($ficha_pago->id != $credito->ficha_pago()->where('no_dia', $ficha_pago->no_dia)->first()->id  && $ficha_pago->estado_p == 0): ?>
                                                        <a class="btn btn-danger btn-sm eliminarPago" type="button" value="<?php echo e($ficha_pago->id); ?>"><i class="fa fa-trash"></i></a>
                                                      <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                  </td>
                                                <?php endif; ?>
                                              <?php endif; ?>

                                            <?php break; ?>
                                          <?php endswitch; ?>
                                        </tr>
                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                  <?php endif; ?>
                                </table>
                              </div>
                            </div>
                            <div id="informacion" class="tab-pane">
                              <div class="row">
                                <h4>Detalle de prestamo</h4>
                                <hr>
                                <?php
                                  $pagosSaldo = \App\Pago::where('prestamo_id',$credito->id)->sum('monto');
                                  $pagosMora = \App\Pago::where('prestamo_id',$credito->id)->sum('mora');
                                  $totalPagosFicha=\App\Ficha_pago::where('prestamo_id',$credito->id)->whereNotNull('pago_id')->get();
                                  $sumaTotalPagos=0;
                                  // dd($totalPagosFicha);
                                  foreach ($totalPagosFicha as $pagosList) {
                                    $listadoPagos[]=\App\Pago::selectRaw('sum(monto) as total')->where('id',$pagosList->pago_id)->first()->total;
                                    // $sumaPagos=$sumaPagos+$listadoPagos->mont o;
                                  }
                                  if(isset($listadoPagos))
                                  {
                                    foreach ($listadoPagos as $sumaPagos) {
                                      $sumaTotalPagos=$sumaTotalPagos+$sumaPagos;
                                    }
                                  }
                                  else {
                                    $sumaTotalPagos=0;
                                  }
                                  // $totalRealPago=\App\Ficha_pago::where('')
                                   $saldoTotal=$sumaTotalPagos-$pagosMora;
                                   // dd($sumaTotalPagos);
                                ?>
                                
                                <div class="col-sm-6 table-responsive panel">
                                  <table class="table table-striped">
                                    <tbody>
                                      <tr>
                                        <td><label>Monto</label></td>
                                        <td>Q.<?php echo e($credito->monto); ?></td>
                                      </tr>
                                      
                                      <?php
                                      $abonadoFinal=$saldoTotal;
                                      // dd($saldoFinal);
                                        if($abonadoFinal>$credito->monto)
                                        {
                                          $esteAbonado=$credito->monto;
                                        }
                                        else {
                                          $esteAbonado=$saldoTotal;
                                        }
                                      ?>
                                      <tr>
                                        <td><label>Abonado</label></td>
                                        
                                        <td class="text-danger">Q.<?php echo e($esteAbonado); ?></td>
                                      </tr>
                                      <tr>

                                        <td><label>Saldo</label></td>
                                        <?php
                                        $saldoFinal=$credito->monto - $saldoTotal;
                                        // dd($saldoFinal);
                                          if($saldoFinal<0)
                                          {
                                            $esteSaldo=0;
                                          }
                                          else {
                                            $esteSaldo=$saldoFinal;
                                          }
                                        ?>
                                        <td class="text-danger">Q.<?php echo e($esteSaldo); ?></td>
                                        
                                      </tr>
                                      <tr>
                                        <td><label>Total de Mora</label></td>
                                        <td>Q.<?php echo e($credito->mora); ?></td>
                                      </tr>
                                      <tr>
                                        <td><label>Mora recuperada</label></td>
                                        <td>Q.<?php echo e($credito->mora_recuperada); ?></td>
                                      </tr>
                                      <tr>
                                        <td><label>Mora pendiente</label></td>
                                        <td class="text-danger">Q.<?php echo e($credito->mora - $credito->mora_recuperada); ?></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                                
                                <div class="col-sm-6 table-responsive panel">
                                  <table class="table table-striped">
                                    <tbody>
                                      <tr>
                                        <td><label>Fecha de creacion:</label></td>
                                        <td><?php echo e(Carbon::parse($credito->created_at)->format('Y-m-d')); ?></td>
                                      </tr>
                                      <tr>
                                        <td><label>Fecha de inicio:</label></td>
                                        <td><?php echo e($credito->fecha_inicio ? $credito->fecha_inicio : "Pendiente"); ?></td>
                                      </tr>
                                      <tr>
                                        <td><label>Fecha fin</label></td>
                                        <td><?php echo e($credito->fecha_fin ? $credito->fecha_fin : "Pendiente"); ?></td>
                                      </tr>
                                      <tr>
                                        <td><label>Fecha 1er. Pago:</label></td>
                                        <td><?php echo e($credito->ficha_pago->first() ? $credito->ficha_pago->first()->fecha : "Pendiente"); ?></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                            <div id="documentos" class="tab-pane">
                              <div class="row">
                                <div class="col-md-12">
                                  <?php if($credito->estado_p_id == 1 || $credito->estado_p_id == 2 ): ?>
                                  <?php else: ?>
                                    <?php echo $__env->make('Documentos.ficha_pago_pdf', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                                    <a target="_blank" href="<?php echo e(route('pdf_ficha', $credito->id)); ?>" class="btn btn-danger">Imprimir ficha de pago</a>
                                    <a target="_blank" href="<?php echo e(route('printSol', $credito->id)); ?>" class="btn btn-success">Imprimir solicitud</a>
                                  <?php endif; ?>
                                </div>
                              </div>
                            </div>
                            <div id="pagos" class="tab-pane">
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="table-responsive">
                                <table class="table" id="tabla-pagos">
                                  <thead>
                                    <tr>
                                      <th>Fecha</th>
                                      <th class="sum">Capital</th>
                                      <th class="sum">Interes</th>
                                      <th class="sum">Mora</th>
                                      <th class="sum">Total</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php if($pagos->isEmpty()): ?>
                                      <h5> No hay pagos registrados</h5>
                                    <?php else: ?>
                                      <?php $__currentLoopData = $pagos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pago): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                          <td><?php echo e(Carbon::parse($pago->created_at)->format('d-m-Y')); ?></td>
                                          <td><?php echo e($pago->capital); ?></td>
                                          <td><?php echo e($pago->interes); ?></td>
                                          <td><?php echo e($pago->mora); ?></td>
                                          <td><?php echo e($pago->monto); ?></td>
                                        </tr>
                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                  </tbody>
                                  <tfoot>
                                    <tr>
                                      <th>Total: </th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                    </tr>
                                  </tfoot>
                                </table>

                              </div>
                                </div>
                              </div>
                            </div>
                            <div id="compromiso" class="tab-pane">
                              <div class="row">
                                <?php echo Form::open(['id'=> 'form-images', 'route' => ['creditos.addCompromiso',$credito->id],'method' => 'POST', 'files' => true]); ?>

                                <div class="col-md-12">
                                  <?php echo $__env->make('Documentos.compromiso2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                                  <a onclick="window.open('<?php echo e(route('printCom', $credito->id)); ?>')" class="btn btn-danger">Imprimir</a>
                                </div>

                                <?php if($credito->foto_compromiso == null && Auth::user()->hasRole('Administrador') or Auth::user()->hasRole('Supervisor')): ?>
                                  <div class="col-md-12">
                                    <h5>No hay imagen de compromiso de pago, seleccione la imagen:</h5>
                                    <?php echo e(Form::label('image', 'Imagen compromiso de pago')); ?>

                                    <?php echo e(Form::file('img_compromiso', null)); ?>

                                    <br>
                                    <button class="btn btn-success btn-md " type="submit" name="button">A???adir imagen</button>
                                  </div>
                                  <?php echo Form::close(); ?>

                                <?php else: ?>
                                  <div class="col-md-12">
                                    <img class="img-responsive" alt="image" src="<?php echo e(asset('images/clientes/documentos')."/".$credito->foto_compromiso); ?>">
                                  </div>
                                <?php endif; ?>
                              </div>
                            </div>
                            <div id="imagenes" class="tab-pane">
                              <?php echo Form::open(['id'=> 'form-images-creditos', 'route' => ['creditos.addImg',$credito->id],'method' => 'POST', 'files' => true]); ?>

                              <div class="">
                                <h4>Imagen Solicitud de prestamo </h4>

                              </div>
                              <hr class="hr-line-solid">
                              <?php if($credito->foto_solicitud): ?>
                                  <img class="img-responsive" height="1000" width="420" alt="image" src="<?php echo e(asset('images/clientes/documentos')."/".$credito->foto_solicitud); ?>">
                              <?php else: ?>
                                <?php echo Form::file('img_solicitud', null); ?>

                              <?php endif; ?>
                              <br>
                              
                              <h4>Imagen Recibo de luz</h4>
                              <hr class="hr-line-solid">
                              <?php if($credito->foto_recibo): ?>
                                <img class="img-responsive" width="420" height="380"  alt="image" src="<?php echo e(asset('images/clientes/documentos')."/".$credito->foto_recibo); ?>">
                              <?php else: ?>
                                <?php echo Form::file('img_recibo', null); ?>

                              <?php endif; ?>
                              <br>
                              <?php if($credito->foto_solicitud == null || $credito->foto_recibo == null): ?>
                                <button type="submit" value="1" class="btn btn-primary btn-sm add_img">Agregar imagenes</button>
                              <?php endif; ?>
                              <?php echo Form::close(); ?>

                            </div>
                        </div>
                        </div>
                      </div>
                  </div>

              </div>

        </div>
</body>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
  <script type="text/javascript" src="<?php echo e(asset('js/dataTables/datatables.min.js')); ?>"></script>
  <script type="text/javascript" src="<?php echo e(asset('js/sweetalert/sweetalert.min.js')); ?>"></script>


  <script type="text/javascript">


  $("#botonPago").click(function(){
    verificarTOTAL();
  });

  function verificarTOTAL(){
    console.log('entro');
    var total = $('#total').val();
    var saldo = <?php echo e(round(($credito->monto - $saldoTotal)+($credito->mora - $credito->mora_recuperada), 2)); ?>;
    var url = "<?php echo e(url('verificar/totalpago')); ?>";
    $.get({
      data:{
        total: total,
        saldo: saldo,
      },
      url: url,
      success: function( response ) {
        if(response=="true"){
        // swal("El monto no debe superar el saldo total",{icon: "warning",});

        swal({
          title: "Ingrese un monto correcto",
          icon: "warning",
          buttons: true,
          dangerMode: true,
          timer:5000
        });
      }
      }


    });
  }
  // $.ajaxSetup({
  //         headers: {
  //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  //         }
  //     });

    var oTable = $('#tabla-pagos').DataTable({
      "language": {
          "url": "<?php echo e(asset('fonts/dataTablesEsp.json')); ?>",
      },
      "paging":   true,
      "info":     false,
      'dom' : 'tip',
      "footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

      var intVal = function ( i ) {
          return typeof i === 'string' ?
              i *1 :
              typeof i === 'number' ?
                  i : 0;
                };

      // Total over all pages
      total = api
          .column( 1 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );

        // Update footer
        api.columns('.sum').every( function () {
          var sum = this
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

          this.footer().innerHTML = sum.toFixed(2);
            });
        }
    });

    $(".comentario").click(function(){
      var valor = $(this).attr('value');
      comentario(valor);
    });

    function comentario(valor){
      var url = '<?php echo e(route("comentario.info", ":id")); ?>';
      url = url.replace(':id', valor);
      $.ajax({
          type: "GET",
          url: url,
          success: function( response ) {
            $('#comentario').val(response['comentario']);
          }
      });
    }

    $(".pagar-btn").click(function(){
      var valor = $(this).attr('value');
      obtenerDatos(valor);
    });

    function obtenerDatos(valor){
      var url = '<?php echo e(route("pagos.info", ":id")); ?>';
      url = url.replace(':id', valor);
      $.ajax({
          type: "GET",
          url: url,
          data: {tipo: '2',},
          success: function( response ) {
            $('#nom_cliente').val(response['cliente']);
            $('#prestamo').val(response['prestamo_id']);
            $('#no_prestamo').val(response['no_prestamo']);
            $('#no_cuota').val(response['no_cuota']);
            $('#mora').val(response['mora']);
            $('#total').val(response['cuota']);
          }
      });
    }

    $('.revertir').click(function(){
      var valor = $(this).attr('value');
      revertir(valor);
    })

    function revertir(valor){
      console.log('click');
      var url = "<?php echo e(route('pagos.revertir')); ?>";
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      swal({
        title: "Esta seguro de cambiar el estado a pendiente?",
        text: "Se van a revertir los cambios!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then((willDelete) => {
        if (willDelete) {
          $.ajax({
            method: 'GET',
            url: url,
            data: {
              valor : valor,
            },
            success: function( response ) {
              console.log(response);
              console.log('peligro');
              swal("Cambios revertidos, estado a pendiente", {
                icon: "success",
              });
              location.reload();
            }
          });
        } else {
          swal("No se cambio el estado",{icon: "warning",});
          location.reload();
        }
      });
    }


    $('.eliminarPago').click(function(){
      var valor = $(this).attr('value');
      eliminarPago(valor);
    })

    function eliminarPago(valor){
      console.log('click');
      var url = "<?php echo e(route('pagos.eliminar')); ?>";
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      swal({
        title: "Esta seguro que desea eliminar esta ficha de pago?",
        text: "Este cambio es irreversible!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then((willDelete) => {
        if (willDelete) {
          $.ajax({
            method: 'GET',
            url: url,
            data: {
              valor : valor,
            },
            success: function( response ) {
              console.log(response);
              console.log('peligro');
              swal("Ficha Eliminada", {
                icon: "success",
              });
              location.reload();
            }
          });
        } else {
          swal("No se eliminó",{icon: "warning",});
        }
      });
    }

    $('.revertirCompleto').click(function(){
      var valor = $(this).attr('value');
      revertirCompleto(valor);
    })

    function revertirCompleto(valor){
      console.log('click');
      var url = "<?php echo e(route('pagos.revertirCompleto')); ?>";
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      swal({
        title: "¿Esta seguro que desea revertir todo el crédito?",
        text: "Este cambio es irreversible!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then((willDelete) => {
        if (willDelete) {
          $.ajax({
            method: 'GET',
            url: url,
            data: {
              valor : valor,
            },
            success: function( response ) {
              console.log(response);
              console.log('peligro');
              swal("Crédito revocado", {
                icon: "success",
              });
              location.reload();
            }
          });
        } else {
          swal("No se revocó",{icon: "warning",});
        }
      });
    }

  </script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>