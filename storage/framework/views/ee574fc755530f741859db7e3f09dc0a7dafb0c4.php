<li class="<?php echo e(isActiveRoute('home')); ?>">
    <a href="<?php echo e(route('home')); ?>"><i class="fa fa-th-large"></i> <span class="nav-label">Principal</span></a>
</li>
<li class="<?php echo e(isActiveRoute('usuarios.create') . isActiveRoute('usuarios.index') . isActiveRoute('usuarios.show')); ?>">
    <a ><i class="fa fa-address-book"></i>
       <span class="nav-label">Colaboradores</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li class="<?php echo e(isActiveRoute('usuarios.create')); ?>"><a href="<?php echo e(route('usuarios.create')); ?>">Registrar colaborador</a></li>
        <li class="<?php echo e(isActiveRoute('usuarios.index')); ?>"><a href="<?php echo e(route('usuarios.index')); ?>">Listado de colaboradores</a></li>
        <li class="<?php echo e(isActiveRoute('usuarios.posicionesView')); ?>"><a href="<?php echo e(route('usuarios.posicionesView')); ?>">Posiciones</a></li>

    </ul>
</li>
<li class="<?php echo e(isActiveRoute('clientes.index') . isActiveRoute('clientes.cumple')); ?>">
    <a><i class="fa fa-address-book-o"></i>
      <span class="nav-label">Clientes</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li class="<?php echo e(isActiveRoute('clientes.index')); ?>"><a href="<?php echo e(route('clientes.index')); ?>">Listado de clientes</a></li>
        <li class="<?php echo e(isActiveRoute('clientes.cumple')); ?>"><a href="<?php echo e(route('clientes.cumple')); ?>">Cumpleañeros</a></li>
    </ul>
</li>
<li class="<?php echo e(isActiveRoute('creditos.create') . isActiveRoute('creditos.index') . isActiveRoute('creditos.entregados')
             . isActiveRoute('creditos.finalizar') . isActiveRoute('creditos.fecha') . isActiveRoute('creditos.planes')); ?>">
    <a><i class="fa fa-folder"></i>
      <span class="nav-label">Créditos</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li class="<?php echo e(isActiveRoute('creditos.create')); ?>"><a href="<?php echo e(route('creditos.create')); ?>">Nueva solicitud</a></li>
        <li class="<?php echo e(isActiveRoute('creditos.index')); ?>"><a href="<?php echo e(route('creditos.index')); ?>">Listado de créditos</a></li>
        <li class="<?php echo e(isActiveRoute('creditos.entregados')); ?>"><a href="<?php echo e(route('creditos.entregados')); ?>">Créditos a entregar</a></li>
        <li class="<?php echo e(isActiveRoute('creditos.finalizar')); ?>"><a href="<?php echo e(route('creditos.finalizar')); ?>">Créditos a finalizar</a></li>
        <li class="<?php echo e(isActiveRoute('creditos.fecha')); ?>"><a href="<?php echo e(route('creditos.fecha')); ?>">Fechas de descanso</a></li>
        <li class="<?php echo e(isActiveRoute('creditos.planes')); ?>"><a href="<?php echo e(route('creditos.planes')); ?>">Planes</a></li>
    </ul>
</li>
<li class="<?php echo e(isActiveRoute('pagos.create') . isActiveRoute('pagos.index')); ?>">
  <a><i class="fa fa-edit"></i>
    <span class="nav-label">Pagos</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li><a href="<?php echo e(route('pagos.create')); ?>">Efectuar pago</a></li>
      <li><a href="<?php echo e(route('pagos.index')); ?>">Historial de pagos</a></li>
      <li><a href="<?php echo e(route('prospecto.index')); ?>">Prospecto</a></li>
  </ul>
</li>
<li class="<?php echo e(isActiveRoute('rutas.index') . isActiveRoute('rutas.create')); ?>">
  <a><i class="fa fa-arrows"></i>
    <span class="nav-label">Rutas</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li><a href="<?php echo e(route('rutas.index')); ?>">Listado de rutas</a></li>
      <li><a href="<?php echo e(route('rutas.create')); ?>">Nueva ruta</a></li>
  </ul>
</li>
<li class="<?php echo e(isActiveRoute('reportes.index')); ?>">
  <a><i class="fa fa-file-text"></i>
    <span class="nav-label">Reportes</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li><a href="<?php echo e(route('reportes.index', 1)); ?>">General</a></li>
      <li><a href="<?php echo e(route('reportes.index', 2)); ?>">Clientes</a></li>
      <li><a href="<?php echo e(route('reportes.index', 3)); ?>">Promotores</a></li>
      <li class="<?php echo e(isActiveRoute('reportes.index',1)); ?>"><a href="<?php echo e(route('reportes.index', 4)); ?>">Moras</a></li>
      <li><a href="<?php echo e(route('reportes.index', 5)); ?>">Clasificación</a></li>
      <li><a href="<?php echo e(route('reportes.index', 6)); ?>">Ruta</a></li>
  </ul>
</li>
<li class="<?php echo e(isActiveRoute('geolocalizacion.index') . isActiveRoute('geolocalizacion.ruta')); ?>">
  <a><i class="fa fa-map-marker"></i>
    <span class="nav-label">Geolocalización</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li><a href="<?php echo e(route('geolocalizacion.index')); ?>">Localizar promotor</a></li>
      <li><a href="<?php echo e(route('geolocalizacion.ruta')); ?>">Ruta recorrida</a></li>
  </ul>
</li>
<li class="<?php echo e(isActiveRoute('graficas.index')); ?>">
  <a><i class="fa fa-bar-chart-o"></i>
    <span class="nav-label">Graficas</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li><a href="<?php echo e(route('graficas.index')); ?>">General</a></li>
      <li><a href="<?php echo e(route('graficas.promotor')); ?>">Promotores</a></li>
      <li><a href="<?php echo e(route('graficas.ruta')); ?>">Ruta</a></li>
  </ul>
</li>

