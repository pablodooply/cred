<div class="row">
  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5> Pago </h5>
                </div>
                <div class="ibox-content">
                  <?php echo Form::open(['route' => 'pagos.store', 'method' => 'POST', 'id' => 'PagoModal']); ?>

                    <div class="form-row col-sm-12">
                        <div class="form-group col-sm-4">
                            <?php echo e(Form::label('name', 'Cliente')); ?>

                            <?php echo e(Form::text('nom_cliente',null , ['class' => 'form-control valor', 'id' => 'nom_cliente', "readonly"])); ?>

                            <?php echo e(Form::hidden('cliente', '', ['id' => 'prestamo'])); ?>

                        </div>
                        <div class="form-group col-sm-4">
                          <?php echo e(Form::label('name', 'No. Prestamo')); ?>

                          <?php echo e(Form::text('no_prestamo', null, ['class' => 'form-control valor', 'id' => 'no_prestamo', "readonly" ])); ?>

                        </div>
                        <div class="form-group col-sm-4">
                            <?php echo e(Form::label('name', 'No. Cuota')); ?>

                            <?php echo e(Form::text('no_cuota',null, ['class' => 'form-control valor', 'id' => 'no_cuota', "readonly" ])); ?>

                        </div>
                    </div>
                    <div class="form-row col-sm-12">
                      <div class="form-group col-sm-4">
                        <?php echo e(Form::label('name', 'Mora')); ?>

                        <?php echo e(Form::text('mora',null, ['class' => 'form-control valor', 'id' => 'mora' , "readonly"])); ?>

                      </div>
                      <div class="form-group col-sm-8">
                        <?php echo e(Form::label('name', 'Total')); ?>

                        
                        <input type="number" name="cuota" min="1" class="form-control valor" id="total" oninput="this.value = Math.abs(this.value)">

                      </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group" id="guardar">
                          <?php echo e(Form::submit('Pago', ['class' => 'btn btn-md btn-primary', 'id' => 'botonPago', 'name' => 'submitbutton', 'value' => 'pago'])); ?>

                          <?php echo e(Form::submit('No pago',['class' => 'btn btn-md btn-danger','name' => 'submitbutton', 'value' => 'no_pago'])); ?>

                          
                      </div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
