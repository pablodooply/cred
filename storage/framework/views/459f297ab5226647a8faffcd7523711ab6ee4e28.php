<?php $__env->startSection('title', 'Perfil cliente'); ?>

<?php $__env->startSection('content'); ?>

<?php $__env->startSection('nombre','Perfil Cliente'); ?>
<?php $__env->startSection('ruta'); ?>
  <li class="active">
      <strong>Perfil de cliente</strong>
  </li>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Clientes.transferencia', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
  <div class="wrapper wrapper-content animated fadeInRight">
      <div class="row border-bottom">
          <div class="col-lg-4">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                  <div class="ibox-title">
                      <h5>Perfil de cliente</h5>
                      <?php echo $cliente->obtenerClasificacion(); ?>

                  </div>
                  <div>
                      <div class="ibox-content profile-content">
                        <div class="col-xs-12">
                          <div class="profile-image">
                            <img src="<?php echo e($cliente->persona->foto_perfil ? $usuario->persona->foto_perfil : asset('images/default/default_perfil.jpg')); ?>" class="pull-center img-circle circle-border m-b-md" alt="profile">
                          </div>
                        </div>
                        <h4><?php echo e($cliente->persona->nombre); ?>  <?php echo e($cliente->persona->apellido); ?></h4>
                        <strong class="text-success">Datos Personales</strong>
                        <?php echo e(Form::hidden('id_cliente', $cliente->id, ["id" => "idCliente"])); ?>

                        <ul class="list-group clear-list">
                          <li class="list-group-item fist-item">
                              <span class="pull-right">Cli-<?php echo e($cliente->id); ?> </span>
                              Codigo
                          </li>
                          <li class="list-group-item fist-item">
                              <span class="pull-right"><?php echo e($cliente->obtenerEdad()); ?></span>
                              Edad
                          </li>
                            <li class="list-group-item">
                                <span class="pull-right"> <?php echo e($cliente->persona->dpi); ?> </span>
                                DPI
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right"> <?php echo e($cliente->persona->telefono ? $cliente->persona->telefono : "No hay telefono"); ?> </span>
                                Telefono
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right"> <?php echo e($cliente->persona->celular1 ?  $cliente->persona->celular1 : "No hay celular"); ?></span>
                                Celular 1
                            </li>
                            
                        </ul>
                          <?php if(Auth::user()->hasAnyRole(['Administrador', 'Supervisor', 'Secretaria','Promotor'])): ?>
                            <div class="user-button">
                              <div class="row">
                                  <div class="col-md-6">
                                      <?php if($cliente->listabn_id == 1): ?>
                                        <?php if(Auth::user()->hasAnyRole(['Administrador'])): ?>
                                          <button id="enviar"  value="<?php echo e($cliente->id); ?>"type="button" class="btn btn-warning btn-sm" ><i class="fa fa-envelope"></i><span id="enviar_ln" value="1"> Enviar a lista negra </span></button>
                                        <?php endif; ?>
                                        <?php if(Auth::user()->hasAnyRole(['Administrador','Supervisor'])): ?>
                                          <a class="btn btn-success btn-sm" id="update_gps">Actualizar ubicacion</a>
                                        <?php endif; ?>
                                        <?php if(($cliente->prestamos->last()!==null ? $cliente->prestamos->last()->estado_p_id : 9)  == '9'): ?>
                                          <a href="<?php echo e(route('creditos.renovacion', $cliente->id)); ?>" class="btn btn-success btn-sm" name="button">Renovacion</a>
                                        <?php else: ?>
                                          <?php if(Auth::user()->hasAnyRole(['Administrador', 'Secretaria'])): ?>
                                            <a href="<?php echo e(route('creditos.renovacion', $cliente->id)); ?>" class="btn btn-success btn-sm" name="button">Renovacion</a>
                                          <?php else: ?>
                                            <a  class="btn btn-success btn-sm" name="button" disabled>Renovacion</a>
                                          <?php endif; ?>
                                        <?php endif; ?>
                                      <?php else: ?>
                                        <?php if(Auth::user()->hasRole('Administrador')): ?>
                                          <button id="enviar" value="<?php echo e($cliente->id); ?>"type="button" class="btn btn-warning btn-sm" ><i class="fa fa-envelope"></i><span id="enviar_ln" value="2"> Enviar a lista blanca </span></button>
                                        <?php endif; ?>
                                        <?php if(Auth::user()->hasAnyRole(['Administrador','Supervisor'])): ?>
                                          <a class="btn btn-success btn-sm" id="update_gps">Actualizar ubicacion</a>
                                        <?php endif; ?>
                                          <a href="<?php echo e(route('creditos.renovacion', $cliente->id)); ?>" class="btn btn-success btn-sm" name="button">Renovacion</a>
                                      <?php endif; ?>
                                      <?php if(Auth::user()->hasAnyRole(['Administrador', 'Supervisor'])): ?>
                                          <button id="transferir" type="button" class="btn btn-primary btn-sm "
                                          data-toggle="modal" data-target=".bd-example-modal-lg">Transferir</button>
                                      <?php endif; ?>
                                  </div>
                              </div>
                          </div>
                          <?php endif; ?>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Direccion</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <address>
                            <strong>Domicilio:</strong><br>
                            <?php echo e($cliente->persona->domicilio); ?><br>
                        </address>
                        <address>
                            <strong>Trabajo:</strong><br>
                            <?php echo e($cliente->empresa_trabajo); ?><br>
                            <?php echo e($cliente->direccion_trabajo); ?><br>
                        </address>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Referecias Personales</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                      <?php $__currentLoopData = $cliente->referencias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $referencia): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <address>
                          <strong><?php echo e($referencia->nombre); ?></strong><br>
                          <?php echo e($referencia->direccion); ?><br>
                          <abbr title="Phone"></abbr> <?php echo e($referencia->telefono); ?>

                        </address>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
          </div>
          <div class="col-lg-8 col-xs-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Prestamos</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                  <div class="tabs-container">
                    <ul class="nav nav-tabs">
                      <li class="active"><a data-toggle="tab" href="#prestamos">Prestamos</a></li>
                      <li><a data-toggle="tab" href="#informacion">Informacion</a></li>
                      <li><a data-toggle="tab" href="#imagenes">Imagenes</a></li>
                    </ul>
                    <div class="tab-content">
                      <div id="prestamos" class="tab-pane active">
                        <div class="table-responsive">
                          <table class='table table-striped table-hover'>
                            <thead>
                              <tr>
                                <th>Codigo</th>
                                <th>Estado</th>
                                <th>Monto</th>
                                <th>Mora</th>
                                <th>Clasificacion</th>
                                <th>Acciones</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php $__currentLoopData = $cliente->prestamos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prestamo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                  <td>Cre-<?php echo e($prestamo->id); ?></td>
                                  <?php echo $prestamo->obtenerEstado(); ?>

                                  <td><?php echo e($prestamo->monto); ?></td>
                                  <td><?php echo e($prestamo->mora); ?></td>
                                  <?php echo $prestamo->obtenerClasificacion(); ?>

                                  <td>
                                    <?php if($prestamo->estado_p_id == 1 || $prestamo->estado_p_id == 2): ?>
                                      <a href="#" class="btn btn-outline btn-success btn-sm" disabled><i class="fa fa-eye"></i></a>
                                    <?php else: ?>
                                      <a href="<?php echo e(route('creditos.show',$prestamo->id)); ?>" class="btn btn-outline btn-success btn-sm"><i class="fa fa-eye"></i></a>
                                    <?php endif; ?>
                                  </td>
                                </tr>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div id="informacion" class="tab-pane">
                          <?php echo $__env->make('Clientes.infoCliente', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                      </div>

                      <div id="imagenes" class="tab-pane">
                        <div class="row">
                          <?php echo Form::open(['id'=> 'form-images', 'route' => ['clientes.add_img',$cliente->id],'method' => 'POST', 'files' => true]); ?>

                            <div class="col-sm-12">
                              <h4>Imagen DPI</h4>
                              <?php if($cliente->foto_dpi == null): ?>
                                <h5>No tiene imagen de DPI, seleccione la imagen de DPI</h5>
                                <?php echo Form::file('img_dpi', null); ?>

                              <?php else: ?>
                                <img class="img-responsive" alt="image" src="<?php echo e(asset('images/clientes/documentos')."/".$cliente->foto_dpi); ?>">
                              <?php endif; ?>
                              <br>
                              <button type="submit" value="1" class="btn btn-primary btn-sm add_img">Agregar imagenes</button>
                            </div>


                            <?php echo e(Form::close()); ?>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
            </div>
      </div>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
  <script src="<?php echo asset('js/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo e(asset('js/sweetalert/sweetalert.min.js')); ?>"></script>


<script type="text/javascript">
  $("#enviar").click(function(){
    var valor = $(this).attr("value");
    var url = '<?php echo e(route("clientes.ln", ":id")); ?>';
    var tipo_cliente = $('#enviar_ln').attr('value');
    if(tipo_cliente == 1){
      var title_swal = "Desea bloquear a este cliente?";
      var content = "Esta seguro de bloquear a este cliente?";
    } else{
      var title_swal = "Desea desbloquear a este cliente?";
      var content = "Esta seguro de desbloquear a este cliente?";
    }
    swal({
      title: title_swal ,
      text: content,
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        url = url.replace(':id', valor);
        $.ajax({
            type: "GET",
            url: url,
            success: function( response ) {
              if(response == 1){
                swal("Cliente desbloqueado", {
                  icon: "success",
                });
                 $('#enviar_ln').text(' Enviar a lista negra');
                 $('#enviar_ln').attr('value', 1);
              }else{
                swal("Cliente bloqueado", {
                  icon: "success",
                });
                $('#enviar_ln').text(' Enviar a lista blanca');
                $('#enviar_ln').attr('value', 2);
              }
            }
        });
      } else {
        swal("No se realizo ninguna accion");
      }
    });

  });

  $("#hoja_ruta").change(function(){
    var valor = $("#hoja_ruta :selected").val(); // The text content of the selected option
    var url = '<?php echo e(route("ruta.promotor", ":id")); ?>';
    url = url.replace(':id', valor);
    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          $("#nom_promotor").val(response);
        }
    });
  });


  $('[data-toggle="tab"]').click(function(e){
    // var now_tab = e.target // activated tab
    var valor = $(this);
    console.log(valor);
  });

  $('#update_gps').click(function(){
    swal({
      title: "Esta seguro de actualizar la ubicacion del cliente??",
      text: "Se cambiara la ubicacion del cliente",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        activeGps();
      } else {
        swal("No se realizaron cambios");
      }
    });
  })

  function activeGps(){
      var id = $("#idCliente").val();
      if (navigator.geolocation)
      {
        /* la geolocalizacion esta disponible */
        navigator.geolocation.getCurrentPosition(function(position){
          var latitud = position.coords.latitude;
          var longitud = position.coords.longitude;
          sendLocation(latitud, longitud, id);
        });

      }
      else
      {
        swal("No se puedo cambiar la actualizacion, verifique si tiene activo el gps.", {
          icon: "error",
        });
      }
  }

  function sendLocation(lat, lon, id){
    // var id = $(this).attr("value");
    console.log(lat + " " + lon + " " + id);
    url = '<?php echo e(url('coordenadas/cliente/')); ?>';
    $.ajax({
        type: "GET",
        url: url,
        data: {
          'latitude' : lat,
          'longitude': lon,
          'secret'  : id,
        },success: function(html){
          swal("Ubicacion cambiada!", {
            icon: "success",
          });
        }
    });
  }

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>