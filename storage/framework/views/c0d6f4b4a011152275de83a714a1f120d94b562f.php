<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
          <?php if(Auth::user()->hasAnyRole(['Administrador', 'Supervisor'])): ?>
            <li>
              <a href="<?php echo e(route('Notificacion.pendientes')); ?>" class="count-info">
                  <i class="fa fa-envelope"></i><span id='actualizar' class="label label-warning"></span>
              </a>
            </li>
          <?php else: ?>
          <?php endif; ?>
            <li>
              <?php if(Auth::user()): ?>
                <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out"></i>Salir
                </a>
              <?php else: ?>
                <a class="dropdown-item" href="<?php echo e(route('login')); ?>"><i class="fa fa-sign-out"></i> Salir</a>
              <?php endif; ?>
              <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="GET" style="display: none;">
                  <?php echo csrf_field(); ?>
              </form>
              </form>
            </li>
        </ul>
    </nav>
</div>
