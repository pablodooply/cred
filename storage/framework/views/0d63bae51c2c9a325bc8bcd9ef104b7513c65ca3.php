<?php $__env->startSection('title', 'Reportes'); ?>

<?php $__env->startSection('link'); ?>
  <link rel="stylesheet" href="<?php echo asset('css/dataTables/datatables.min.css'); ?>" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

  <?php $__env->startSection('nombre','Reporte'); ?>
  <?php $__env->startSection('ruta'); ?>
    <li class="active">
        <strong>Reporte general</strong>
    </li>
  <?php $__env->stopSection(); ?>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Reportes </h5>
      </div>
      <div class="ibox-content">
        <div class="row">

        <div class="col-sm-5 m-b-xs">
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Desde:</label>
              <?php echo e(Form::date('fecha_inicio', \Carbon\Carbon::now()->startOfMonth()->subMonth(),['class' => 'form-control', 'id' => 'inicioFecha'])); ?>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Hasta:</label>
              <?php echo e(Form::date('fecha_fin', \Carbon\Carbon::now(),['class' => 'form-control', 'id' => 'finFecha'])); ?>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label"> </label>
            <button type="submit" id="busqueda" class="btn btn-primary pull-center" name="button">Buscar</button>
          </div>
        </div>
        </div>
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover dataTables-example" >
          <thead>
          <tr>
              <th>Capital</th>
              <th>Capital recolectado</th>
              <th>Interes recolectado</th>
              <th>Mora recolectada</th>
              <th>Mora total</th>
            </tr>
            </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

  <script type="text/javascript" src="<?php echo e(asset('js/dataTables/datatables.min.js')); ?>"></script>

  <script>
      $(document).ready(function(){
          var  oTable = $('.dataTables-example').DataTable({
            "language": {
                "url": "<?php echo e(asset('fonts/dataTablesEsp.json')); ?>",
            },
              pageLength: 10,
              "processing": true,
              "serverSide": true,
              ajax: {
                   url: '<?php echo e(route('reportes.general')); ?>',
                   data: function (d) {
                       d.inicio = $('input[name=fecha_inicio]').val();
                       d.fin = $('input[name=fecha_fin]').val();
                   }
               },
              'columns': [
                {data: 'capital'},
                {data: 'capitalRecolectado'},
                {data: 'interesRecolectado'},
                {data: 'moraRecolectada'},
                {data: 'moraTotal'},
              ],
              dom: '<"html5buttons"B>lTfgitp',
              buttons: [
                  { extend: 'copy' , footer: true},
                  {extend: 'csv', footer: true},
                  {extend: 'excel', title: 'Reportes', footer: true},
                  {extend: 'pdf', title: 'Reportes', footer: true,
                },

                  {extend: 'print', footer: true,
                   customize: function (win){
                          $(win.document.body).addClass('white-bg');
                          $(win.document.body).css('font-size', '10px');

                          $(win.document.body).find('table')
                                  .addClass('compact')
                                  .css('font-size', 'inherit');
                                }
                  }
              ],
              "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i *1 :
                    typeof i === 'number' ?
                        i : 0;
            };

          // Total over all pages
          total = api
              .column( 1 )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
              }, 0 );

              // Update footer
              var que = api.columns('.sum', { page: 'current'}).every( function () {
                var sum = this
                  .data()
                  .reduce( function (a, b) {
                      return intVal(a) + intVal(b);
                  }, 0 );

                this.footer().innerHTML = sum;
              } );
        }
    });

          $('#busqueda').on('click', function(e) {
              oTable.draw();
              e.preventDefault();
          });
});

  </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>