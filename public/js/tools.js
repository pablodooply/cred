$(document).ready(function(){
    	$(".pendiente-link").click(function(){
          var valor = $(this).attr("value");
          var url = '{{ route("notificacion.show", ":id") }}';
          url = url.replace(':id', valor);
          $.ajax({
              type: "GET",
              url: url,
              success: function( response ) {
                console.log(url);
                $('#detalle_credito').html(response);
              }
          });

    	});

      $(".pendientes").click(function(){
          var valor = $(this).attr("value");
          var url = '{{ route("Notificacion.pendientes") }}';
          $.ajax({
              type: "GET",
              url: url,
              success: function( response ) {
                console.log(url);
                $('#titulo').val('Pendientes');
                $('#detalle_credito').html(response);
              }
          });
    	});

      $(".crear-notificacion").click(function(){
          var url = '{{ route("notificacion.create") }}';
          $.ajax({
              type: "GET",
              url: url,
              success: function( response ) {
                console.log(url);
                $('#detalle_credito').html(response);
                $('#titulo').text('Creacion de notificacion');
              }
          });
    	});
});
