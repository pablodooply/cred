<div class="row">
  <div class="modal fade bd-example-modal-lg-comentario" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5> Comentario </h5>
                </div>
                <div class="ibox-content">
                    <div class="form-row col-sm-12">
                        <div class="form-group col-sm-12">
                            {{ Form::label('name', 'Comentario') }}
                            {{ Form::text('comentario',null , ['class' => 'form-control valor', 'id' => 'comentario', "readonly"]) }}
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group" id="guardar">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                          {{-- {{ Form::submit('Dia de perdon',['class' => 'btn btn-md btn-warning','name' => 'submitbutton', 'value' => 'pendiente'])}} --}}
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
