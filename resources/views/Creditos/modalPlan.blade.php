<div class="row">
  <div id="modal-plan" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5> Editar Plan </h5>
                </div>
                <div class="ibox-content">
                  {!! Form::open(['route' => 'creditos.editar_plan', 'method' => 'POST']) !!}
                    <div class="form-row col-sm-12">
                        <div class="form-group col-sm-4">
                            {{ Form::label('name', 'Plan') }}
                            <select class="form-control" name="plan_plan" id="plan_plan">
                              <option value="Diario">Diario</option>
                              <option value="Semanal">Semanal</option>
                              <option value="Quicena">Quincena</option>
                            </select>
                            {{ Form::hidden('plan_edit', '', ['id' => 'plan_edit']) }}
                        </div>
                        <div class="form-group col-sm-4">
                          {{ Form::label('name', 'Capital') }}
                          {{ Form::text('capital_e', null, ['class' => 'form-control valor', 'id' => 'capital_e' ]) }}
                        </div>
                        <div class="form-group col-sm-4">
                            {{ Form::label('name', 'Interes') }}
                            {{ Form::text('interes_e',null, ['class' => 'form-control valor', 'id' => 'interes_e' ]) }}
                        </div>
                    </div>
                    <div class="form-row col-sm-12">
                      <div class="form-group col-sm-4">
                        {{ Form::label('name', 'Mora') }}
                        {{ Form::text('mora_e',null, ['class' => 'form-control valor', 'id' => 'mora_e']) }}
                      </div>
                      <div class="form-group col-sm-4">
                        {{ Form::label('name', 'No. Pagos') }}
                        {{ Form::text('nopagos_e',null, ['class' => 'form-control valor', 'id' => 'nopagos_e']) }}
                      </div>
                      <div class="form-group col-sm-4">
                        {{ Form::label('name', 'Cuota') }}
                        {{ Form::text('cuota_e','', ['class' => 'form-control valor', 'id' => 'cuota_e', 'readonly']) }}
                      </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group" id="guardar">
                          {{ Form::submit('Guardar', ['class' => 'btn btn-md btn-primary', 'name' => 'submitbutton', 'value' => 'pago']) }}
                          {{ Form::button('Cancelar',['class' => 'btn btn-md btn-danger','name' => 'submitbutton', 'value' => 'no_pago'])}}
                      </div>
                    </div>
                    {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
