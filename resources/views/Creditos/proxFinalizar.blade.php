@extends('layouts.app')

@section('title', 'Creditos')

@section('content')

@section('nombre','Creditos')
@section('ruta')
  <li class="active">
      <strong>Creditos a finalizar</strong>
  </li>
@endsection

@section("link")
<link href="{{asset('css/rangedatepicker/daterangepicker.css')}}" rel="stylesheet"  media="screen">
@endsection

@php
  use Carbon\Carbon;
  setlocale(LC_TIME, 'es_AR.utf8');
  Carbon::setUtf8(false);
@endphp
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
      <div class="col-sm-12">
        <div class="ibox float-e-margins">
          <div class="ibox-title">
            <h5>Prestamos a finalizar</h5>
          </div>
          <div class="ibox-content">
            <div class="row">
              <ul class="nav">
                <p><span class="pull-left text-muted">{{$prestamos->count()}} Prestamos</span></p>
              </ul>
              <label class="col-md-12">Rango:</label>
              <div class="col-md-12">
                <div class="col-sm-6">
                  <div class="input-group">
                    <div class="form-group">
                      <input id="rangedate" class="form-control" type="text" name="" value="">
                    </div>
                    <span class="input-group-btn">
                      <a id="busqueda_fecha" class="btn btn-primary"> <i class="fa fa-search"></i> Buscar</a>
                    </span>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="table-responsive">
                <table id="tabla_fin" class='table table-striped table-hover'>
                  <thead>
                    <tr>
                      <th>Prestamo</th>
                      <th>Monto</th>
                      <th>Clasificacion</th>
                      <th>Fecha final</th>
                      <th>Cliente</th>
                      <th>Promotor</th>
                    </tr>
                  </thead>
                  <tbody>
                     @foreach ($prestamos as $prestamo)
                      <tr>
                        <td>Cre-{{$prestamo->id}}</td>
                        <td>{{$prestamo->monto}}</td>
                        {!!$prestamo->obtenerClasificacion()!!}
                        <td>{{Carbon::parse($prestamo->fecha_fin)->formatLocalized('%A') ." " . Carbon::parse($prestamo->fecha_fin)->format('d-m-y')}}</td>
                        <td>{{$prestamo->cliente->persona->nombre}} {{$prestamo->cliente->persona->apellido}}</td>
                        <td>{{$prestamo->ruta->first()->hoja_ruta->user->persona->nombre}} {{$prestamo->ruta->first()->hoja_ruta->user->persona->apellido}}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('js/moment/moment.js')}}"></script>
<script type="text/javascript" src="{{asset('js/rangedatepicker/daterangepicker.js')}}"></script>

<script type="text/javascript">
var datos;
$("#rangedate").daterangepicker({
  "locale": {
    "format": "DD/MM/YYYY",
    "fromLabel": "Desde",
    "toLabel": "Hasta",
    "applyLabel": "Aplicar",
    "cancelLabel": "Cancelar",
    "daysOfWeek": [
      "Dom",
      "Lun",
      "Mar",
      "Mie",
      "Jue",
      "Vie",
      "Sab"
    ],
    "monthNames": [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre"
    ],
  }
});

$(document).ready(function(){
  $("#busqueda_fecha").click(function(){
    var tabla = $("#tabla_fin tbody")
    var rango_fechas = $("#rangedate").val();
    var valores = rango_fechas.split(' - ');
    var inicio = valores[0];
    var fin = valores[1];
    var url = "{{route('creditos.apifinalizar')}}";
    $.ajax({
        type: "GET",
        url: url,
        data: {
          'inicio' : inicio,
          'fin'    : fin,
      },
        success: function( response ) {
          tabla.html("");
          datos = response;
          var url_cliente = "{{route('clientes.show',":id")}}"
          for (var i = 0; i < datos.length; i++) {
            var datos_ac = datos[i][0];
            var url2 = url_cliente;
            url2 = url2.replace(':id', datos_ac.id_cliente);
            var new_tr =
            "<tr>" +
              "<td>Cre-" + datos_ac.id + "</td>" +
              "<td>" + datos_ac.monto + "</td>" +
                       datos_ac.clasificacion +
              "<td>" + datos_ac.fecha_final + "</td>" +
              "<td><a class='client-link' href="+ url2 +">" + datos_ac.cliente + "</a></td>" +
              "<td>" + datos_ac.promotor + "</td>" +
            "</tr>"
            tabla.append(new_tr);
          }
        }
    });
  });
});
</script>
@endsection
