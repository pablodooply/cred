@extends('layouts.app')

@section('title', 'Creditos')

@section('link')
<link rel="stylesheet" href="{{asset('css/datatables/datatables.min.css')}}">
@endsection

@section('content')

@section('nombre','Creditos')
@section('ruta')
  <li class="active">
      <strong>Listado de creditos</strong>
  </li>
@endsection

@php
  use Carbon\Carbon;
@endphp

<div id="espacio-modal">
  @include('Creditos.pago')
  @include('Pagos.pagoVencido')
</div>
<div class="row">
    <div class="col-sm-8">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Listado de creditos</h5>
        </div>
        <div class="ibox-content">

          <ul class="nav nav-tabs" role="tablist">
            @if(Auth::user()->hasAnyRole(['Administrador','Secretaria']))
            <li class="active"><a data-toggle="tab" id="clic_todos" href="#todos">Todos</a></li>
            <li><a data-toggle="tab" id="clic_activos" href="#home">Activos</a></li>
            <li><a data-toggle="tab" id="clic_vencidos" href="#menu1">Vencidos</a></li>
            <li><a data-toggle="tab" id="clic_mora" href="#menu2">En mora</a></li>
            <li><a data-toggle="tab" id="clic_pendientes" href="#menu3">Pendientes</a></li>
            <li><a data-toggle="tab" id="clic_hoy" href="#menu4">Aprobados hoy</a></li>

            @elseif (Auth::user()->hasAnyRole(['Promotor']))
            <li class="active"><a data-toggle="tab" id="clic_todos" href="#todos">Todos</a></li>
            <li><a data-toggle="tab" id="clic_activos" href="#home">Activos</a></li>
            <li><a data-toggle="tab" id="clic_vencidos" href="#menu1">Vencidos</a></li>
            <li><a data-toggle="tab" id="clic_mora" href="#menu2">En mora</a></li>


            @elseif (Auth::user()->hasAnyRole(['Supervisor']))
            <li class="active"><a data-toggle="tab" id="clic_todos" href="#todos">Todos</a></li>
            <li><a data-toggle="tab" id="clic_activos" href="#home">Activos</a></li>
            <li><a data-toggle="tab" id="clic_vencidos" href="#menu1">Vencidos</a></li>
            <li><a data-toggle="tab" id="clic_mora" href="#menu2">En mora</a></li>
          @endif
          </ul>

          <div class="tab-content">
            @if(Auth::user()->hasAnyRole(['Administrador','Secretaria']))
              <div role="tabpanel" id="todos" class="tab-pane active">
                <div id="todos">
                </div>
              </div>
              <div role="tabpanel" id="home" class="tab-pane">
                <div id="activos">
                </div>
              </div>
              <div role="tabpanel" id="menu1" class="tab-pane">
                <div id="vencidos">
                </div>
              </div>
              <div role="tabpanel" id="menu2" class="tab-pane">
                <div id="mora">
                </div>
              </div>
              <div role="tabpanel" id="menu3" class="tab-pane">
                <div id="pendientes">
                </div>
              </div>
              <div role="tabpanel" id="menu4" class="tab-pane">
                <div id="hoy">
                </div>
              </div>
            @elseif (Auth::user()->hasAnyRole(['Promotor']))
              <div role="tabpanel" id="todos" class="tab-pane active">
                <div id="todos">
                </div>
              </div>
              <div role="tabpanel" id="home" class="tab-pane">
                <div id="activos">
                </div>
              </div>
              <div role="tabpanel" id="menu1" class="tab-pane">
                <div id="vencidos">
                </div>
              </div>
              <div role="tabpanel" id="menu2" class="tab-pane">
                <div id="mora">
                </div>
              </div>
            @elseif (Auth::user()->hasAnyRole(['Supervisor']))
              <div role="tabpanel" id="todos" class="tab-pane active">
                <div id="todos">
                </div>
              </div>
              <div role="tabpanel" id="home" class="tab-pane">
                <div id="activos">
                </div>
              </div>
              <div role="tabpanel" id="menu1" class="tab-pane">
                <div id="vencidos">
                </div>
              </div>
              <div role="tabpanel" id="menu2" class="tab-pane">
                <div id="mora">
                </div>
              </div>
            @endif

          </div>


        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="ibox ">
        <div id='resumen_perfil' class="ibox-content">
          @if($creditos_activos->isEmpty())
            <h3>No hay creditos</h3>
          @else
            <div class="tab-content">
              <div id="contact-1" class="tab-pane active">
                <div class="row m-b-lg">
                    <div class="col-lg-12 text-center">
                        <h2>Codigo: Cre-{{$creditos_activos->first()->id}}</h2>
                    </div>
                </div>
                  <div class="client-detail">
                    <div class="full-height-scroll">
                        <strong>Datos de credito</strong>
                        <ul class="list-group clear-list">
                          <li class="list-group-item">
                            <span class="pull-right">{{$creditos_activos->first()->cliente->persona->nombre}}</span>
                            Cliente
                          </li>
                            <li class="list-group-item fist-item">
                                <span class="pull-right">{{$creditos_activos->first()->monto}} </span>
                                Monto
                            </li>
                            @php
                              $abonado = $creditos_activos->first()->capital_recuperado + $creditos_activos->first()->interes;
                            @endphp
                            <li class="list-group-item">
                                <span class="pull-right">{{$abonado}}</span>
                                Abonado:
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">{{$creditos_activos->first()->mora}}</span>
                                Mora actual
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">{{$creditos_activos->first()->fecha_desembolso}}</span>
                                Fecha de desembolso:
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">{{$creditos_activos->first()->fecha_inicio}}</span>
                                Fecha de Inicio:
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">{{$creditos_activos->first()->fecha_fin}}</span>
                                Fecha de Final:
                            </li>
                        </ul>
                        <strong>Prestamos</strong>
                        <table class='table table-striped table-hover'>
                          <thead>
                            <tr>
                              <th>Fecha</th>
                              <th>Estado</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach ($creditos_activos->first()->ficha_pago->where('tipo',1)->where('fecha','>=',Carbon::now()->format('Y-m-d'))->take(5) as $ficha_pago)
                              <tr>
                                <td>{{$ficha_pago->fecha}}</td>
                                <td>{!!$ficha_pago->estadoActual()!!}</td>
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                    </div>
                  </div>
              </div>

            </div>
          @endif
        </div>
      </div>
    </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>

<script type="text/javascript">

$(document).ready(function(){
  $('#clic_todos').trigger('click');
});
$("#clic_todos").on('click', function(){
  var url = '{{ route("tabla.todos") }}';
  console.log(url);
  $('#todos').html(' ');
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        $('#todos').html(response);
      }
  });
});

$("#clic_activos").on('click', function(){
  var url = '{{ route("tabla.activos") }}';
  console.log(url);
  $('#home').html(' ');
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        $('#home').html(response);
      }
  });
});

$("#clic_vencidos").on('click', function(){
  var url = '{{ route("tabla.vencidos") }}';
  console.log(url);
  $('#menu1').html(' ');
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        $('#menu1').html(response);
      }
  });
});

$("#clic_mora").on('click', function(){
  var url = '{{ route("tabla.mora") }}';
  console.log(url);
  $('#menu2').html(' ');
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        $('#menu2').html(response);
      }
  });
});

$("#clic_pendientes").on('click', function(){
  var url = '{{ route("tabla.pendientes") }}';
  console.log(url);
  $('#menu3').html(' ');
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        $('#menu3').html(response);
      }
  });
});

$("#clic_hoy").on('click', function(){
  var url = '{{ route("tabla.hoy") }}';
  console.log(url);
  $('#menu4').html(' ');
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        $('#menu4').html(response);
      }
  });
});
</script>

<script type="text/javascript">

function clickeliminar(){
$(".eliminarcredito").click(function(){
  var valor = $(this).attr('value');
  eliminarcredito(valor);
});
}

function eliminarcredito(valor){
  console.log('click');
  var url = "{{route('creditos.eliminarcredito')}}";
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  swal({
    type: 'info',
    title: "Esta seguro que desea eliminar el crédito?",
    text: "Se eliminaran toda la información de éste crédito, los pagos, la ficha de pago y la ruta!. Además, no se podran recuperar estos datos!",
    icon: "warning",
    buttons: true,
    dangerMode: false,
    footer: '<a href>Why do I have this issue?</a>',
  }).then((willDelete) => {
    if (willDelete) {
      $.ajax({
        method: 'GET',
        url: url,
        data: {
          valor : valor,
        },
        success: function( response ) {
          console.log(response);
          console.log('peligro');
          swal("Se eliminó toda la información del crédito, los pagos, la ficha de pago y la ruta.", {
            icon: "success",
          });
          setTimeout("location.reload()",1000);

        },
        error: function( response ) {
          console.log(response);
          console.log('peligro');
          swal("Lo sentimos, Ocurrió un error!", {
            icon: "warning",
          });

        }
      });
    } else {
      swal("No se eliminó el crédito",{icon: "warning",});
    }
  });
}

</script>

@endsection
