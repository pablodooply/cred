<div class="full-height-scroll animated fadeInRight">
 <div class="table-responsive" style="overflow-x:auto;">
  <table class='table table-striped table-hover' id="tabla-creditos-pendientes" style="width:100%">
    <thead>
      <tr>
        <th>Codigo</th>
        <th>Monto</th>
        <th>Estado</th>
        <th>Clasificacion</th>
        <th>Plan</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <th>Codigo</th>
        <th>Monto</th>
        <th>Estado</th>
        <th>Clasificacion</th>
        <th>Plan</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Acciones</th>
      </tr>
    </tfoot>
  </table>
</div>
</div>
<script type="text/javascript">
var rCliente=$('#tabla-creditos-pendientes').DataTable({
  order: [[ 0, "desc" ]],
  language: {
      "url": "{{asset('fonts/dataTablesEsp.json')}}",
  },
  paging: true,
  info: false,
  dom : 'tip',
  processing: true,
  serverSide: true,
  ajax: {
    "url": '{{route('api.tabla.pendientes')}}',
    // 'beforeSend': function (request) {
    //  request.setRequestHeader("X-CSRF-TOKEN", '{{ csrf_token() }}');
    //  },
     // data: function(d) {
     //
     //
     // d.fecha1 = $('input[name=fecha1]').val();
     // d.fecha2 = $('input[name=fecha2]').val();
     // }
  },
  pageLength: 10,
  responsive: true,
  dom: '<"html5buttons"B>lTfgitp',
  buttons: [
    {extend: 'copy'},
    {extend: 'csv'},
    {extend: 'excel', title: 'Asesores'},
    {extend: 'pdf', title: 'Asesores'},
    {extend: 'print',
       customize: function (win)
       {
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
                  .addClass('compact')
                  .css('font-size', 'inherit');
      }
    }
  ],
  columns: [
      {data: 'id', name: 'id'},
      {data: 'monto', name: 'monto'},
      {data: 'Estado', name: 'Estado'},
      {data: 'Clasificacion', name: 'Clasificacion'},
      {data: 'plan.nombre', name: 'plan.nombre'},
      {data: 'cliente.persona.nombre', name: 'cliente.persona.nombre'},
      {data: 'cliente.persona.apellido', name: 'cliente.persona.apellido'},
      {data: 'Acciones', name: 'Acciones'},
  ],
//   "footerCallback": function( tfoot, data, start, end, display ) {
//   var api = this.api();
//   $( api.column( 5 ).footer() ).html(
//       api.column( 5 ).data().reduce( function ( a, b ) {
//           return a + b;
//       }, 0 )
//   );
// }
});

$("tbody").on('click', '#pagar-prestamo', function(){
  var valor = $(this).attr("value");
  var url = '{{ route("pagos.info", ":id") }}';
  url = url.replace(':id', valor);
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        $('#nom_cliente').val(response['cliente']);
        $('#cliente').val(response['prestamo']);
        $('#no_cuota').val(response['no_cuota']);
        $('#total').val(response['cuota']);
        $('#prestamo').val(response['prestamo']);
        $('#modal-pago').modal('show');
      }
  });
});

$("tbody").on('click','.client-link',function(){
    var valor = $(this).attr("value");
    var url = '{{ route("creditos.show", ":id") }}';
    url = url.replace(':id', valor);
    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          $('#resumen_perfil').html(response);
        }
    });

});
</script>
