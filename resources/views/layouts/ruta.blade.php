<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>@yield('nombre')</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('home')}}">Inicio</a>
            </li>
            @yield('ruta')
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
