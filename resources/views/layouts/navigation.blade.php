<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                  <div class="dropdown-messages-box">
                    <a href="" class="pull-left">
                        <img alt="image" class="img-circle" src="{{isset(Auth::user()->persona->foto_perfil) ? asset('/images/empleados/perfil/').'/' . Auth::user()->persona->foto_perfil : asset('images/default/perfil.jpg')}}">
                    </a>
                  </div>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{Auth::user()->name}}</strong>
                            </span>
                        </span>
                    </a>
                </div>
                <div class="logo-element">
                    <img alt="image" class="img-md" src="{{asset('images/system/logo2.png')}}"></img>
                </div>
            </li>

          @if(Auth::user()->hasRole('Administrador'))
            @include('layouts.navegacion_admin');
          @endif
          @if(Auth::user()->hasRole('Supervisor'))
            @include('layouts.navegacion_supervisor');
          @endif
          @if(Auth::user()->hasRole('Secretaria'))
            @include('layouts.navegacion_secre');
          @endif
          @if(Auth::user()->hasRole('Promotor'))
            @include('layouts.navegacion_rutero');
          @endif

        </ul>
    </div>
</nav>
