<li class="{{ isActiveRoute('home') }}">
    <a href="{{route('home')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Principal</span></a>
</li>
<li class="{{ isActiveRoute('clientes.index') . isActiveRoute('clientes.cumple') }}">
    <a><i class="fa fa-address-book-o"></i>
      <span class="nav-label">Clientes</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{route('clientes.index')}}">Listado de clientes</a></li>
        <li><a href="{{route('clientes.cumple')}}">Cumpleañeros</a></li>
    </ul>
</li>
<li class="{{ isActiveRoute('creditos') }}">
    <a><i class="fa fa-folder"></i>
      <span class="nav-label">Creditos</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
      <li><a href="{{route('creditos.index')}}">Listado de creditos</a></li>
      <!--<li><a href="{{route('creditos.entregados')}}">Listado de entregables</a></li>-->
      <li><a href="{{route('creditos.finalizar')}}">Creditos a finalizar</a></li>
    </ul>
</li>
<li class="{{ isActiveRoute('prospecto.index') }}">
    <a href="{{ route('prospecto.index') }}"><i class="fa fa-archive"></i> <span class="nav-label">Prospectos</span> </a>
</li>
<li class="{{ isActiveRoute('geo.promotor') }}">
    <a href="{{ route('geo.promotor') }}"><i class="fa fa-map-marker"></i> <span class="nav-label">Geolocalizacion</span> </a>
</li>
{{-- <li class="{{ isActiveRoute('') }}">
    <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Graficas</span> </a>
</li> --}}
<li class="{{ isActiveRoute('graficas.index') }}">
  <a><i class="fa fa-bar-chart-o"></i>
    <span class="nav-label">Graficas</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li><a href="{{ route('graficas.promotor')}}">Promotores</a></li>
      <li><a href="{{ route('graficas.ruta')}}">Ruta</a></li>
  </ul>
</li>
