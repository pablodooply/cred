<li class="{{ isActiveRoute('home') }}">
    <a href="{{route('home')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Principal</span></a>
</li>
<li class="{{ isActiveRoute('usuarios.create') . isActiveRoute('usuarios.index') . isActiveRoute('usuarios.show')}}">
    <a ><i class="fa fa-address-book"></i>
       <span class="nav-label">Colaboradores</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{route('usuarios.index')}}">Listado de colaboradores</a></li>
    </ul>
</li>
<li class="{{ isActiveRoute('clientes') }}">
    <a><i class="fa fa-address-book-o"></i>
      <span class="nav-label">Clientes</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{route('clientes.index')}}">Listado de clientes</a></li>
        <li><a href="{{route('clientes.cumple')}}">Cumpleañeros</a></li>
    </ul>
</li>
<li class="{{ isActiveRoute('creditos') }}">
    <a><i class="fa fa-folder"></i>
      <span class="nav-label">Creditos</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{route('creditos.create')}}">Nueva solicitud</a></li>
        <li><a href="{{route('creditos.index')}}">Listado de creditos</a></li>
    </ul>
</li>
<li class="{{ isActiveRoute('pagos') }}">
  <a><i class="fa fa-edit"></i>
    <span class="nav-label">Pagos</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li><a href="{{ route('pagos.index')}}">Historial de pagos</a></li>
  </ul>
</li>
<li class="{{ isActiveRoute('pagos') }}">
  <a><i class="fa fa-arrows"></i>
    <span class="nav-label">Rutas</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li><a href="{{ route('rutas.index') }}">Listado de rutas</a></li>
  </ul>
</li>
<li class="{{ isActiveRoute('reportes.index') }}">
  <a><i class="fa fa-file-text"></i>
    <span class="nav-label">Reportes</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li><a href="{{ route('reportes.index', 1) }}">Clientes</a></li>
      <li><a href="{{ route('reportes.index', 2) }}">Promotores</a></li>
      <li><a href="{{ route('reportes.index', 3) }}">Moras</a></li>
      <li><a href="{{ route('reportes.index', 4) }}">Clasificacion</a></li>
  </ul>
</li>
