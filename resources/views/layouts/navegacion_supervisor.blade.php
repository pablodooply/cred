<li class="{{ isActiveRoute('home') }}">
    <a href="{{route('home')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Principal</span></a>
</li>
<li class="{{ isActiveRoute('usuarios.create') . isActiveRoute('usuarios.index') . isActiveRoute('usuarios.show')}}">
    <a ><i class="fa fa-address-book"></i>
       <span class="nav-label">Colaboradores</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li class="{{isActiveRoute('usuarios.index')}}"><a href="{{route('usuarios.index')}}">Listado de colaboradores</a></li>
        <li class="{{isActiveRoute('usuarios.posicionesView')}}"><a href="{{route('usuarios.posicionesView')}}">Posiciones</a></li>

    </ul>
</li>
<li class="{{ isActiveRoute('clientes.index') . isActiveRoute('clientes.cumple') }}">
    <a><i class="fa fa-address-book-o"></i>
      <span class="nav-label">Clientes</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li class="{{isActiveRoute('clientes.index')}}"><a href="{{route('clientes.index')}}">Listado de clientes</a></li>
        <li class="{{isActiveRoute('clientes.cumple')}}"><a href="{{route('clientes.cumple')}}">Cumpleañeros</a></li>
    </ul>
</li>
<li class="{{ isActiveRoute('creditos.create') . isActiveRoute('creditos.index') . isActiveRoute('creditos.entregados')
             . isActiveRoute('creditos.finalizar') . isActiveRoute('creditos.fecha') . isActiveRoute('creditos.planes')}}">
    <a><i class="fa fa-folder"></i>
      <span class="nav-label">Creditos</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        {{-- <li class="{{isActiveRoute('creditos.create')}}"><a href="{{route('creditos.create')}}">Nueva solicitud</a></li> --}}
        <li class="{{isActiveRoute('creditos.index')}}"><a href="{{route('creditos.index')}}">Listado de creditos</a></li>
        <li class="{{isActiveRoute('creditos.entregados')}}"><a href="{{route('creditos.entregados')}}">Creditos a entregar</a></li>
        <li class="{{isActiveRoute('creditos.finalizar')}}"><a href="{{route('creditos.finalizar')}}">Creditos a finalizar</a></li>
        {{-- <li class="{{isActiveRoute('creditos.planes')}}"><a href="{{route('creditos.planes')}}">Planes</a></li> --}}
    </ul>
</li>
<li class="{{ isActiveRoute('pagos.create') . isActiveRoute('pagos.index') }}">
  <a><i class="fa fa-edit"></i>
    <span class="nav-label">Pagos</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li><a href="{{ route('pagos.create') }}">Efectuar pago</a></li>
      <li><a href="{{ route('pagos.index')}}">Historial de pagos</a></li>
  </ul>
</li>
<li class="{{ isActiveRoute('rutas.index') . isActiveRoute('rutas.create')  }}">
  <a><i class="fa fa-arrows"></i>
    <span class="nav-label">Rutas</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li><a href="{{ route('rutas.index') }}">Listado de rutas</a></li>
      {{-- <li><a href="{{ route('rutas.create')}}">Nueva ruta</a></li> --}}
  </ul>
</li>
<li class="{{ isActiveRoute('reportes.index') }}">
  <a><i class="fa fa-file-text"></i>
    <span class="nav-label">Reportes</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li><a href="{{ route('reportes.index', 1) }}">General</a></li>
      <li><a href="{{ route('reportes.index', 2) }}">Clientes</a></li>
      <li><a href="{{ route('reportes.index', 3) }}">Promotores</a></li>
      <li class="{{ isActiveRoute('reportes.index',1)}}"><a href="{{ route('reportes.index', 4) }}">Moras</a></li>
      <li><a href="{{ route('reportes.index', 5) }}">Clasificacion</a></li>
      <li><a href="{{ route('reportes.index', 6) }}">Ruta</a></li>
  </ul>
</li>
<li class="{{ isActiveRoute('geolocalizacion.index') . isActiveRoute('geolocalizacion.ruta')  }}">
  <a><i class="fa fa-map-marker"></i>
    <span class="nav-label">Geolocalizacion</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li><a href="{{ route('geolocalizacion.index') }}">Localizar promotor</a></li>
      <li><a href="{{ route('geolocalizacion.ruta') }}">Ruta recorrida</a></li>
  </ul>
</li>
<li class="{{ isActiveRoute('graficas.index') }}">
  <a><i class="fa fa-bar-chart-o"></i>
    <span class="nav-label">Graficas</span><span class="fa arrow"></span></a>
  <ul class="nav nav-second-level collapse">
      <li><a href="{{ route('graficas.index') }}">General</a></li>
      <li><a href="{{ route('graficas.promotor')}}">Promotores</a></li>
      <li><a href="{{ route('graficas.ruta')}}">Ruta</a></li>
  </ul>
</li>
