@extends('layouts.app')

@section('title', 'Editar Empleado')

@section('content')

  <div class="wrapper wrapper-content animated fadeInRight">

              <div class="row">
                  <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                      @if ($errors->any())
                          <div class="alert alert-danger">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          </div>
                      @endif
                      <div class="ibox-title">
                        <h5>Solicitud</h5>
                      </div>
                      <div class="ibox-content">
                        <h5 class="text-warning">Datos Personales</h5>
                        <hr>
                        <div class="form-row col-sm-12">
                          {!! Form::  open(['id'=> 'formulario','route' => ['clientes.update', $cliente->id], 'method' => 'PUT']) !!}

                            <!--*****************Nombre******************-->
                            <div class="form-group col-sm-4">
                                {{ Form::label('name', 'Nombre') }}
                                {{ Form::text('nombre', $cliente->persona->nombre, ['class' => 'form-control', 'id' => 'nombre' ]) }}
                            </div>
                            <!--*****************Apellido******************-->
                            <div class="form-group col-sm-4">
                                {{ Form::label('name', 'Apellido') }}
                                {{ Form::text('apellido', $cliente->persona->apellido, ['class' => 'form-control', 'id' => 'apellido' ]) }}
                            </div>
                            <!--*****************DPI******************-->
	                    <div class="form-group col-sm-4">
	                     {{ Form::label('name', 'DPI o Pasaporte') }}
	                     {{ Form::text('dpi', $cliente->persona->dpi, ['class' => 'form-control', 'id' => 'dpi' ]) }}
	                    </div>
                        </div>


                        <div class="form-row col-sm-12">
                            <!--*****************NIT******************-->
                            <div class="form-group col-sm-4">
                                {{ Form::label('name', 'NIT') }}
                                {{ Form::text('nit', $cliente->persona->nit, ['class' => 'form-control', 'id' => 'nit' ]) }}
                            </div>
                            <!--*****************Fecha_nacimiento******************-->
                            <div class="form-group col-sm-4">
                                {{ Form::label('name', 'Fecha de Nacimiento') }}
                                {{ Form::date('fecha_nacimiento', $cliente->persona->fecha_nacimiento,['class' => 'form-control', ])}}
                            </div>
                            <div class="form-group col-sm-4">
                              {{ Form::label('name', 'Genero') }}
                              <select class="form-control" name="genero">
                                @if($cliente->persona->genero == 1)
                                  <option value="1">Hombre</option>
                                  <option value="0">Mujer</option>
                                @else
                                  <option value="0">Mujer</option>
                                  <option value="1">Hombre</option>
                                @endif
                              </select>
                            </div>
                        </div>

                        <div class="form-row col-sm-12">
                          <!--*****************estado******************-->
                          <div class="form-group col-sm-4">
                              {{ Form::label('name', 'Departamento') }}
                              <select name="state" id="departamento" class="form-control">
                                <option value="{{$cliente->persona->location->states->id}}">
                                  {{$cliente->persona->location->states->name}}</option>
                                @foreach ($states as $state)
                                  @if($state->name == $cliente->persona->location->states->name)
                                  @else
                                    <option value="{{$state->id}}">{{$state->name}}</option>
                                  @endif
                                @endforeach
                              </select>
                          </div>
                          <!--*****************ciudad******************-->
                          <div class="form-group col-sm-4">
                              {{ Form::label('name', 'Municipio') }}
                              <select name="city" id="city" class="form-control" >
                              </select>
                          </div>
                          <div class="form-group col-sm-4">
                            {{ Form::label('name', 'Nacionalidad') }}
                            <select name="nacionalidad" class="form-control" >
                              <option value="Guatemalteca">Guatemalteca</option>
                              <option value="Beliceña">Mexicana</option>
                              <option value="Estadounidense">Estadounidense</option>
                              <option value="Costarricense">Costarricense</option>
                              <option value="Hondureña">Hondureña</option>
                              <option value="Nicaraguense">Nicaraguense</option>
                              <option value="Panameña">Panameña</option>
                              <option value="Salvadoreña">Salvadoreña</option>
                              <option value="Beliceña">Beliceña</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-row col-sm-12">
                          <!--*****************Telefono 1******************-->
                          <div class="form-group col-sm-4">
                            {{ Form::label('name', 'No. hijos') }}
                            {{ Form::number('no_hijos', $cliente->no_hijos, ['class' => 'form-control', 'id' => 'telefono1' ]) }}
                          </div>
                          <div class="form-group col-sm-4">
                            {{ Form::label('name', 'Telefono 1') }}
                            {{ Form::text('telefono', $cliente->persona->telefono, ['class' => 'form-control', 'id' => 'telefono1' ]) }}
                          </div>
                          <!--*****************Celular1******************-->
                          <div class="form-group col-sm-4">
                            {{ Form::label('name', 'Telefono Móvil 1') }}
                            {{ Form::text('celular1', $cliente->persona->celular1, ['class' => 'form-control', 'id' => 'celular1' ]) }}
                          </div>
                        </div>
                        <div class="form-row col-sm-12">
                          <!--*****************Estado civil******************-->
                          <div class="form-group col-sm-4">
                            {{ Form::label('foto', 'Estado civil') }}
                            <select class="form-control" name="estado_civil">
                              <option value="Soltero">Soltero</option>
                              <option value="Casado">Casado</option>
                              <option value="Viudo">Divorciado</option>
                              <option value="Viudo">Viudo</option>
                            </select>
                          </div>
                          <!--*****************Tipo de casa******************-->
                          <div class="form-group col-sm-4">
                            {{ Form::label('name', 'Tipo de casa') }}
                            {{ Form::text('tipo_casa', $cliente->tipo_casa, ['class' => 'form-control', 'id' => 'nombre' ])}}
                          </div>
                          <!--*****************Actividad economica******************-->
                          <div class="form-group col-sm-4">
                            {{ Form::label('name', 'Actividad economica') }}
                            {{ Form::text('actividad', $cliente->actividad, ['class' => 'form-control', 'id' => 'nombre' ]) }}
                          </div>
                        </div>
                        <!--*****************Direccion Domicilio******************-->
                        <div class="form-row col-sm-12">
                          <div class="form-group col-sm-12">
                              {{ Form::label('name', 'Dirección domicilio') }}
                              {{ Form::text('direccion', $cliente->persona->domicilio, ['class' => 'form-control', 'id' => 'direccion' ]) }}
                          </div>
                          <div class="form-group col-sm-12">
                            {{ Form::label('name', 'Dirección a cobrar') }}
                            {{ Form::text('direccion_cobrar', $cliente->direccion_cobrar, ['class' => 'form-control', 'id' => 'direccion']) }}
                          </div>
                        </div>
                        <div class="form-row col-sm-12">

                          <!--*****************Foto de dpi******************-->
                          <div class="form-group col-sm-4">
                            {{ Form::label('foto', 'Imagen de DPI') }}
                            {{ Form::file('foto_dpi', ['class' => 'form-control', 'value' => "c:/passwords.txt"]) }}
                          </div>
                          <!--*****************Imagen recibo de luz******************-->
                          <div class="form-group col-sm-4">
                            {{ Form::label('foto', 'Imagen de recibo de luz') }}
                            {{ Form::file('foto_recibo', ['class' => 'form-control']) }}
                          </div>
                          <!--*****************Imagen recibo de luz******************-->
                          <div class="form-group col-sm-4">
                            {{ Form::label('foto', 'Imagen firma') }}
                            {{ Form::file('foto_firma', ['class' => 'form-control']) }}
                          </div>
                        </div>
                        <div class="form-row col-sm-12">
                          <!--*****************Nombre recibo de luz******************-->
                          <div class="form-group col-sm-6">
                              {{ Form::label('name', 'Nombre recibo de luz') }}
                              {{ Form::text('recibo_luz', $cliente->nombre_recibo, ['class' => 'form-control', 'id' => 'nombre' ]) }}
                          </div>
                          <!--*****************Direccion Recibo Luz******************-->
                          <div class="form-group col-sm-6">
                              {{ Form::label('name', 'Dirección recibo de luz') }}
                              {{ Form::text('direccion_luz', $cliente->direccion_recibo, ['class' => 'form-control', 'id' => 'direccion' ]) }}
                          </div>
                        </div>

                        {{--*************************************************--}}
                        <h5 class='text-default'>Refencias personales</h5>
                        <hr>
                        @php
                          $cont = 1;
                        @endphp
                        @foreach ($cliente->referencias as $referencia)
                          <div class="form-row col-sm-12">
                            <div class="form-group">
                          <!--****************Referencia Personal 1******************-->
                              <div class="form-group col-sm-4">
                                  {{ Form::label('name', 'Nombre Referencia' . $cont) }}
                                  {{ Form::text('nombre_r'.$cont, $referencia->nombre, ['class' => 'form-control', 'id' => 'nombre' ]) }}
                              </div>
                              <div class="form-group col-sm-4">
                                  {{ Form::label('name', 'Telefono Referencia' . $cont) }}
                                  {{ Form::text('telefono_r'.$cont, $referencia->telefono, ['class' => 'form-control', 'id' => 'nombre' ]) }}
                              </div>
                              <div class="form-group col-sm-4">
                                  {{ Form::label('name', 'Direccion Referencia' .$cont) }}
                                  {{ Form::text('direccion_r'.$cont, $referencia->direccion, ['class' => 'form-control', 'id' => 'nombre' ]) }}
                              </div>
                            </div>
                          </div>
                          @php
                          $cont=$cont+1
                          @endphp
                        @endforeach
                        {{--***********************************************  --}}
                        <h5 class='text-success'>Datos de Trabajo</h5>
                        <hr>
                        <div class="form-row col-sm-12">
                          <div class="form-group">
                        <!--****************Empresa******************-->
                            <div class="form-group col-sm-4">
                                {{ Form::label('name', 'Empresa') }}
                                {{ Form::text('Empresa', $cliente->empresa_trabajo, ['class' => 'form-control', 'id' => 'nombre' ]) }}
                            </div>
                        <!--*****************Telefono empresa******************-->
                            <div class="form-group col-sm-4">
                                {{ Form::label('name', 'Telefono empresa') }}
                                {{ Form::text('telefono_empresa', $cliente->telefono_empresa, ['class' => 'form-control', 'id' => 'nombre' ]) }}
                            </div>
                        <!--*****************Tiempo trabajando******************-->
                            <div class="form-group col-sm-4">
                                {{ Form::label('name', 'Tiempo Trabajando') }}
                                {{ Form::text('tiempo_trabajando', $cliente->tiempo_trabajando, ['class' => 'form-control', 'id' => 'nombre' ]) }}
                            </div>
                          </div>
                        </div>
                        <div class="form-row col-sm-12">
                          <div class="form-group">
                        <!--****************Salario******************-->
                            <div class="form-group col-sm-4">
                                {{ Form::label('name', 'Salario') }}
                                {{ Form::text('Salario', $cliente->salario, ['class' => 'form-control', 'id' => 'nombre' ]) }}
                            </div>
                        <!--*****************Observaciones******************-->
                            <div class="form-group col-sm-8">
                                {{ Form::label('name', 'Observaciones') }}
                                {{ Form::text('oberservaciones', 'No tiene', ['class' => 'form-control', 'id' => 'nombre' ]) }}
                            </div>
                          </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group" id="guardar">
                              {{ Form::submit('Guardar', ['class' => 'btn btn-md btn-primary']) }}
                              {{ Form::button('Cancelar',['class' => 'btn btn-md btn-danger', 'id' => 'btn_cancel'])}}
                          </div>
                        </div>
                      </div>
                    {!! Form::close() !!}

                    </div>


                  </div>
              </div>
          </div>
  @endsection

  @section('scripts')

    <script type="text/javascript">

$("#btn_cancel").click(function(){
  window.history.back();
});

        $("#departamento").change(function(){
          obtenerDatos();
        });

      function obtenerDatos(){
        var valor = $("#departamento :selected").attr("value"); // The text content of the selected option
        var url = '{{ route("api.municipios", ":id") }}';
        url = url.replace(':id', valor);
        $.ajax({
            type: "GET",
            url: url,
            success: function( response ) {
              $("#city").empty();
              $.each(response, function(id,name){
                  $("#city").append('<option value="'+name+'">'+ id  +'</option>');
                });
            }
          });
        }

        obtenerDatos();

    </script>


  @show