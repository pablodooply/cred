<div class="row">
  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5> Transferencia </h5>
                </div>
                <div class="ibox-content">
                  {!! Form::open(['route' => ['clientes.transferir', $cliente->id], 'method' => 'PUT']) !!}
                    <div class="form-row col-sm-12">
                        <div class="form-group col-sm-4">
                            {{ Form::label('name', 'Cliente') }}
                            {{ Form::text('cliente', $cliente->persona->nombre, ['class' => 'form-control valor', 'id' => 'valor', "readonly"]) }}
                        </div>
                        <div class="form-group col-sm-4">
                          {{ Form::label('name', 'Prestamo actual') }}
                          {{ Form::text('cliente', $cliente->prestamos->last() ? $cliente->prestamos->last()->id : 'No tiene', ['class' => 'form-control valor', 'id' => 'valor', "readonly" ]) }}
                        </div>
                        <div class="form-group col-sm-4">
                            {{ Form::label('name', 'Promotor') }}
                            {{ Form::text('comision_cliente',$cliente->prestamos->last() ? $cliente->prestamos->last()->ruta->first()->hoja_ruta->user->persona->nombre : "No tiene", ['class' => 'form-control valor', 'id' => 'valor', "readonly" ]) }}
                        </div>
                    </div>
                    <div class="form-row col-sm-12">
                      <div class="form-group col-sm-4">
                        {{ Form::label('name', 'Hoja ruta actual') }}
                        {{ Form::text('comision_cliente',$cliente->prestamos->last() ? $cliente->prestamos->last()->ruta->first()->hoja_ruta->nombre : "no tiene", ['class' => 'form-control valor', 'id' => 'valor' , "readonly"]) }}
                      </div>
                      <div class="form-group col-sm-4">
                          {{ Form::label('name', 'Hoja ruta nueva: ') }}
                          <select class="form-control" name="hoja_ruta_nueva" id="hoja_ruta">
                            @foreach ($hoja_rutas as $hoja_ruta)
                              <option value="{{$hoja_ruta->id}}">{{$hoja_ruta->nombre}}</option>
                            @endforeach
                          </select>

                      </div>
                      <div class="form-group col-sm-4">
                          {{ Form::label('name', 'Nuevo Promotor:') }}
                          {{ Form::text('comision_cliente',$cliente->prestamos->last() ? $cliente->prestamos->last()->ruta->first()->hoja_ruta->user->persona->nombre : "no tiene", ['class' => 'form-control valor', 'id' => 'nom_promotor' , "readonly"]) }}
                      </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group" id="guardar">
                          {{ Form::submit('Transferir', ['class' => 'btn btn-md btn-primary']) }}
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                      </div>
                    </div>
                    {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
