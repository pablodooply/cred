@extends('layouts.app')

@section('title', 'Reportes')

@section('content')

  @section('nombre','Geolocalizacion')
  @section('ruta')
    <li class="active">
        <strong>Ruta recorrida</strong>
    </li>
  @endsection
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-md-12">
    <div class="ibox ">
        <div class="ibox-title">
            <h5>Mapa</h5>
        </div>
        <div class="ibox-content">
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group col-md-4">
                <label>Promotor</label>
                <select class="form-control" name="" id="promotor">
                  @if($empleados->isEmpty())
                    <option value="">No hay promotores</option>
                  @else
                    @foreach ($empleados as $promotor )
                      <option value="{{$promotor->id}}">{{$promotor->persona->nombre}} {{$promotor->persona->apellido}}</option>
                    @endforeach
                  @endif
                </select>
              </div>
                <div class="form-group col-md-3">
                  <label>Fecha: </label>
                  <div class="input-group">
                    <span class="input-group-btn">
                      {{ Form::date('fecha_recorrido', \Carbon\Carbon::now(),['class' => 'form-control', 'id' => 'fecha_recorrido'])}}
                      <button id="trazar" type="button" class="btn btn btn-primary"> <i class="fa"></i>Mostrar recorrido</button>
                    </span>
                  </div>
                </div>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="google-map" style="height: 500px;" id="map1"></div>
        </div>
    </div>
</div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6igmhHiOWFAOGcb9OJ8nlicsEIpWkgtg"></script>
<script type="text/javascript" src="{{asset('js/markerclusterer/markerclusterer.js')}}"></script>


<script type="text/javascript">
google.maps.event.addDomListener(window, 'load', init);
var mapa;
var marcador;
var recorrido = [];
var datos;
var prueba;
var flightPlanCoordinates = [];
var flightPath;
var pinColor1 = "00ffff";
var pinColor2 = "00ff00";
var pinColor3 = "ff0000";
var markerCluster;
var infowindow;


var pinImage1 = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor1,
    new google.maps.Size(21, 34),
    new google.maps.Point(0,0),
    new google.maps.Point(10, 34));

var pinImage2 = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor2,
    new google.maps.Size(21, 34),
    new google.maps.Point(0,0),
    new google.maps.Point(10, 34));

var pinImage3 = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor3,
    new google.maps.Size(21, 34),
    new google.maps.Point(0,0),
    new google.maps.Point(10, 34));


var iconoMoto = {
  url: "{{ asset('images/moto.png') }}",
  size: new google.maps.Size(45, 45),
  origin: new google.maps.Point(0, 0),
  anchor: new google.maps.Point(0, 32),
  scaledSize: new google.maps.Size(25, 25)
};

//Icono casa
var iconoCasa = {
    url: "{{ asset('images/casa-icon.png') }}",
    size: new google.maps.Size(45, 45),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 32),
    scaledSize: new google.maps.Size(45, 45)
  };


var shape = {
coords: [1, 1, 1, 20, 18, 20, 18, 1],
type: 'poly'
};

function init() {
  var mapOptions1 = {
      zoom: 14,
      center: new google.maps.LatLng(14.8485397, -91.5268578),
      // Style for Google Maps
  };

  var mapElement1 = document.getElementById('map1');
  mapa = new google.maps.Map(mapElement1, mapOptions1);

  @php
    $colors = array("red", "fuchsia", "blue", "green", "purple", "dodgerblue", "sienna", "black", "aqua", "orange", "red", "fuchsia", "blue");
    $count = 0; //temporary variable counting number of tracklog (each user hawe own Polyline)
  @endphp


  obtenerDatos();

    // Información de la ruta (coordenadas, color de línea, etc...)
  flightPath = new google.maps.Polyline({
    path: flightPlanCoordinates,
    geodesic: true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 2
  });

  // Creando la ruta en el mapa
  flightPath.setMap(mapa);

};

var datos;

function trazarRuta(datos){
  console.log(datos);
  flightPath = new google.maps.Polyline({
    path: datos,
    geodesic: true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 3
  });

  flightPath.setMap(mapa);

}

function reiniciarMarcadores(){
  for (var i = 0; i < recorrido.length; i++) {
      recorrido[i].setMap(null);
  }
  flightPath.setMap(null);
  flightPlanCoordinates = [];
}

function actualizarMarcadores(data){
  reiniciarMarcadores();
  prueba = data;

  var marker = new google.maps.Marker({
  position: new google.maps.LatLng(data[0].lat,
  data[0].lon),
  map: mapa,
  size: new google.maps.Size(10, 10),
  icon: pinImage1,
  animation: google.maps.Animation.DROP,
  title: data[0].fecha,
  });

  var line = {lat: parseFloat(data[0].lat), lng: parseFloat(data[0].lon)};
  flightPlanCoordinates.push(line);
  recorrido.push(marker);

  google.maps.event.addListener(marker,'click', function() {
      createInfo(this.title,this);
    });

  // var line = {lat: parseFloat(data[i].lat), lng: parseFloat(data[i].lon)};
  // flightPlanCoordinates.push(line);
  // recorrido.push(marker);

  for (var i = 1; i < data.length-1; i+=1) {
    var marker = new google.maps.Marker({
    position: new google.maps.LatLng(data[i].lat,
    data[i].lon),
    map: mapa,
    size: new google.maps.Size(10, 10),
    icon: pinImage3,
    animation: google.maps.Animation.DROP,
    title: data[i].fecha,
  });

  var line = {lat: parseFloat(data[i].lat), lng: parseFloat(data[i].lon)};
  flightPlanCoordinates.push(line);
  recorrido.push(marker);

  google.maps.event.addListener(marker,'click', function() {

      createInfo(this.title,this);
    });
  }

  var marker = new google.maps.Marker({
  position: new google.maps.LatLng(data[data.length-1].lat,
  data[data.length-1].lon),
  map: mapa,
  size: new google.maps.Size(10, 10),
  icon: pinImage2,
  animation: google.maps.Animation.DROP,
  title: data[data.length-1].fecha,
  });

  var line = {lat: parseFloat(data[data.length-1].lat), lng: parseFloat(data[data.length-1].lon)};
  flightPlanCoordinates.push(line);
  recorrido.push(marker);


  google.maps.event.addListener(marker,'click', function() {
      createInfo(this.title,this);
    });
  // var line = {lat: parseFloat(data[i].lat), lng: parseFloat(data[i].lon)};
  // flightPlanCoordinates.push(line);
  // recorrido.push(marker);

  //markerCluster = new MarkerClusterer(mapa, recorrido,imagePath = "{{asset('imgages/m')}}");

}



$('#trazar').click(function(){
  obtenerDatos();
});

function obtenerDatos(){
  var promo = $('#promotor option:selected').val();
  var fecha = $('#fecha_recorrido').val();
  var url = '{{ route("api.recorrido", ":id") }}';
  url = url.replace(':id', promo);
  // actualizar();
  $.ajax({
      type: "GET",
      url: url,
      data: {
        'fecha' : fecha,
      },
      success: function( data ) {
        actualizarMarcadores(data);
        trazarRuta(flightPlanCoordinates);
      }
      });
}


function createInfo(hora, marker){
        var contentString =
        '<div id="contentInfoWindow" style="width: 200px;">'
          +'<h4>' + "Hora: " + hora + '</h4'
        +'</div>';
        infowindow = new google.maps.InfoWindow({
          content: contentString
        });
        infowindow.open(mapa, marker);
}

</script>


@endsection
