@extends('layouts.app')

@section('title', 'Geolocalizacion')

@section('content')

  @section('nombre','Geolocalizacion')
  @section('ruta')
    <li class="active">
        <strong>Localizacion</strong>
    </li>
  @endsection
  <style>
  .map-info-window{
 background:#333;
 border-radius:4px;
 box-shadow:8px 8px 16px #222;
 color:#fff;
 max-width:200px;
 max-height:300px;
 text-align:center;
 padding:5px 20px 10px;
 overflow:hidden;
 position:absolute;
 text-transform:uppercase;
}
  </style>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-md-12">
    <div class="ibox ">
        <div class="ibox-title">
            <h5>Mapa</h5>
        </div>
        <div class="ibox-content">
          <div class="row">
            {{ Form::hidden('id_promotor', $empleado->id, ['id' => 'id_promotor']) }}
            <div class="col-xs-12">
              <div class="google-map" style="height: 500px;" id="map1"></div>
            </div>
        </div>
    </div>
</div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYQEzpo6c96KowjNSIGxp7RRmtwBdS5Ss"></script>


<script type="text/javascript">
google.maps.event.addDomListener(window, 'load', init);
var mapa;
var marcador;
var clientes = [];
var datos;

var iconoMoto = {
  url: "{{ asset('images/moto.png') }}",
  size: new google.maps.Size(45, 45),
  origin: new google.maps.Point(0, 0),
  anchor: new google.maps.Point(0, 32),
  scaledSize: new google.maps.Size(25, 25)
};

//Icono casa
var iconoCasa = {
    url: "{{ asset('images/casa-icon.png') }}",
    size: new google.maps.Size(45, 45),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 32),
    scaledSize: new google.maps.Size(45, 45)
  };

//Icono casa prestamo vencido
var iconoCasa_vencido = {
    url: "{{ asset('images/casa_vencido.png') }}",
    size: new google.maps.Size(45, 45),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 32),
    scaledSize: new google.maps.Size(45, 45)
  };


var shape = {
coords: [1, 1, 1, 20, 18, 20, 18, 1],
type: 'poly'
};

function init() {
  var mapOptions1 = {
      zoom: 15,
      center: new google.maps.LatLng({{$empleado->timeline_gps->last() ? $empleado->timeline_gps->last()->gps->coords_lat : 14.8485397}}, {{$empleado->timeline_gps->last() ? $empleado->timeline_gps->last()->gps->coords_lon : -91.5268578}}),
      // Style for Google Maps
  };

  var mapElement1 = document.getElementById('map1');
  mapa = new google.maps.Map(mapElement1, mapOptions1);

  @php
    $colors = array("red", "fuchsia", "blue", "green", "purple", "dodgerblue", "sienna", "black", "aqua", "orange", "red", "fuchsia", "blue");
    $count = 0; //temporary variable counting number of tracklog (each user hawe own Polyline)
  @endphp

  //Marcadores de ubicacion de los empleados
    marcador = new google.maps.Marker();
    actualizar();

    //Marcadores de ubicacion de los clientes
  @foreach($rutas as $ruta)
    @if($ruta->prestamo->cliente->gps == null)
    @else
    @if($ruta->prestamo->estado_p_id == 10)
      var icono_casa = iconoCasa;
    @else
      var icono_casa = iconoCasa_vencido;
    @endif
      var marker = new google.maps.Marker({position: new google.maps.LatLng({{$ruta->prestamo->cliente->gps->coords_lat}},
      {{$ruta->prestamo->cliente->gps->coords_lon}}),
      map: mapa,
      size: new google.maps.Size(10, 10),
      shape: shape,
      icon: icono_casa,
      id: {{$ruta->prestamo->cliente->id}},
      title: "{{$ruta->prestamo->cliente->persona->nombre}}"  });

      clientes.push(marker);
      google.maps.event.addListener(marker,'click', function() {
          var id = this.id;
          createInfo(id,this);
        });
    @endif
  @endforeach
};

function actualizar(){
  marcador.setMap(null);
  var promo = $('#id_promotor').val();
  var url = "{{route('geolocalizacion.getgps', ':id')}}";
  url = url.replace(':id', promo);
  $.ajax({
      type: "GET",
      url: url,
      success: function( data ) {
        marcador = new google.maps.Marker({
        position: new google.maps.LatLng(data['latitud'],
        data['longitud']),
        map: mapa,
        size: new google.maps.Size(10, 10),
        shape: shape,
        icon: iconoMoto,
        title: data['nombre'],
        animation: google.maps.Animation.BOUNCE});
      }
  });
};

var infowindow;

function reiniciarMarcadores(){
  for (var i = 0; i < clientes.length; i++) {
      clientes[i].setMap(null);
  }
}


function actualizarMarcadores(data){
  reiniciarMarcadores();

  for (var i = 0; i < data.length; i+=1) {
    var marker = new google.maps.Marker({
    position: new google.maps.LatLng(data[i][0].lat,
    data[i][0].lon),
    map: mapa,
    size: new google.maps.Size(10, 10),
    shape: shape,
    animation: google.maps.Animation.DROP,
    icon: iconoCasa,
    title: data[i][0].nombre,
    id: data[i][0].id_cliente,
  });

  clientes.push(marker);
  google.maps.event.addListener(marker,'click', function() {
      var id = this.id;
      createInfo(id,this);
    });
}

}

function createInfo(id, marker){
  var url = "{{route('info.cliente', ":id")}}";
  var datos;
  url = url.replace(':id', id);
  var url_cliente = "{{route('clientes.show',":id")}}";
  url_cliente = url_cliente.replace(':id', id);
  console.log(url_cliente);
  $.ajax({
      type: "GET",
      url: url,
      success: function( data ) {
        var contentString =
        '<div id="contentInfoWindow" style="width: 200px;">'
          +'<table class="table">'
            +'<tbody>'
              +'<tr>'
                +'<th> Nombre: </th>'
                +'<td>' + data.nombre + '</td>'
              +'</tr>'
              +'<tr>'
                +'<th> Prestamo: </th>'
                +'<td>' + data.prestamo + '</td>'
              +'</tr>'
              +'<tr>'
                +'<th> Monto: </th>'
                +'<td>' + data.monto + '</td>'
              +'</tr>'
            +'<tbody>'
          +'</table>'
          +'<a href=' + url_cliente + ' class="btn btn-outline btn-sm btn-primary"><span class="fa fa-eye"></span></a>'
        +'</div>';
        infowindow = new google.maps.InfoWindow({
          content: contentString
        });
        infowindow.open(mapa, marker);
      }
  });
}

window.setInterval("actualizar()", 30000);

</script>

@endsection
