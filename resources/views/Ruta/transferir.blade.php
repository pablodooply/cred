<div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Transferencia Ruta</h5>
        </div>
        <div class="ibox-content">
          {!! Form::open(['id'=> 'formulario', 'method' => 'PUT','route' => 'ruta.transferir']) !!}
            <div class="form-row col-sm-12">
                <div class="form-group col-sm-4">
                    {{ Form::label('name', 'Promotor actual') }}
                    {{ Form::text('promotor_actual',$hoja_ruta->user->persona->nombre, ['class' => 'form-control', 'id' => 'promotor_actual','readonly']) }}
                </div>
                <div class="form-group col-sm-4">
                    {{ Form::label('name', 'Ruta') }}
                    {{ Form::text('nombreRuta',$hoja_ruta->nombre, ['class' => 'form-control', 'id' => 'idRuta', 'readonly']) }}
                    {{ Form::hidden('idRuta', $hoja_ruta->id) }}

                </div>
                <div class="form-group col-sm-4">
                    {{ Form::label('name', 'Nuevo promotor') }}
                    <select class="form-control" name="idPromotor">
                      <option value="{{$hoja_ruta->user->id}}">{{$hoja_ruta->user->persona->nombre}}</option>
                      @foreach ($promotores as $promotor)
                        @if($promotor->id == $hoja_ruta->user->id)
                        @else
                          <option value="{{$promotor->id}}">{{$promotor->persona->nombre . " " . $promotor->persona->apellido}}</option>
                        @endif
                      @endforeach
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group" id="guardar">
                  {{ Form::submit('Transferir', ['class' => 'btn btn-md btn-primary', 'name' => 'submitbutton', 'value' => 'pago']) }}
                  {{ Form::button('Cancelar', ['class' => 'btn btn-md btn-danger', 'id' => 'btn-cancel'])}}
              </div>
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

<script type="text/javascript">

$('#btn-cancel').click(function() {
  console.log('hola');
  $('#modal-transferir').modal('hide');
});
</script>
