<table id="tablas-rutas" class='table table-striped table-hover'>
  <thead>
    <tr>
      <th>Codigo</th>
      <th>Nombre</th>
      <th>Promotor</th>
      <th>Supervisor</th>
      <th class="sum">No. Prestamos</th>
      <th class="sum">Total</th>
      <th>Acciones</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($rutas as $hoja_ruta)
      <tr>
        <td>{{$hoja_ruta->id}}</td>
        <td>{{$hoja_ruta->nombre}}</td>
        <td>{{$hoja_ruta->user->persona->nombre}}</td>
        <td>{{isset($hoja_ruta->supervisor->persona->nombre) ? $hoja_ruta->supervisor->persona->nombre : 'No asignado'}}</td>
        <td class="cantidad_pres">{{$hoja_ruta->ruta->count()}}</td>
        <td class="valor_total">{{$hoja_ruta->prestamos->whereIn('estado_p_id',[3,5])->sum('capital_activo')}}</td>
        <td>
          <a href="{{route('rutas.show', $hoja_ruta->id)}}" class="btn btn-outline btn-success btn-md"><i class="fa fa-eye"></i></a>
          @if(Auth::user()->hasRole('Administrador'))
            <a data-toggle="modal" data-target="#modal-transferir" class="btn btn-outline btn-primary btn-md" href="{{route('ruta.infoRuta',$hoja_ruta->id)}}"><i class="fa fa-retweet"></i></a>
          @endif
        </td>
      </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr class="info">
      <th></hd>
        <th></th>
        <th>Total:</th>
        <th></th>
        <th></th>
        <th></th>
      </tr>
    </tfoot>
  </table>

  {{-- <script type="text/javascript">
     $("#formulario").submit(function (event) {
       console.log('entro');
       var data = form.serialize();
       var url = "{{route('ruta.transferir')}}";
       // $.ajax({
       //   type: "POST",
       //   url: url,
       //   data: data,
       //   success: function (data) {
       //     $('tablas-rutas').html(data);
       //   },
       // });
     });
  </script> --}}
