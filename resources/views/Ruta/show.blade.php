@extends('layouts.app')

@section('title', 'Ruta')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Ruta {{$hoja_ruta->nombre}}</h5>
        </div>
        <div class="ibox-content">
          <div class="col-md-3">
            <select type="text" class="form-control" id="select-rutas">
              <option value="">Todos</option>
              <option value="Pendiente">Pendiente</option>
              <option value="Aprobada">Aprobada</option>
              <option value="Activo">Activo</option>
              <option value="Morosa">Morosa</option>
              <option value="Cancelado">Cancelado</option>
            </select>
          </div>
          <div class="input-group col-md-8">
            <input type="text" placeholder="Buscar credito " class="input form-control" id="busqueda-rutas">
          </div>
          <div class="table-responsive">
            <table class='table table-striped table-hover' id="tabla-rutas">
              <thead>
                <tr>
                  <th>Cliente</th>
                  <th>Prestamo</th>
                  <th>Estado</th>
                  <th class="sum">Capital activo</th>
                  <th>Pagos atrasados #</th>
                  <th>Pagos atrasados</th>
                  <th class="sum">Pagado</th>
                  <th class="sum">Mora</th>
                  <th class="sum">Saldo</th>
                </tr>
              </thead>
              <tbody>
                @php
                  $total_capital = 0;
                  $total_mora = 0;
                  $total_pendiente = 0;
                  $total_pagado = 0;
                @endphp
                @foreach ($hoja_ruta->prestamos as $prestamo)
                    <tr>
                      <td><a class="client-link" href="{{route('clientes.show', $prestamo->cliente->id)}}">{{$prestamo->cliente->persona->nombre}}</a></td>
                      <td><a class="client-link" href="{{route('creditos.show', $prestamo->id)}}">Cre-{{$prestamo->id}}</td>
                      {!!$prestamo->obtenerEstado()!!}
                      <td>{{$prestamo->capital_activo}}</td>
                      <td class="text-danger">{{($prestamo->no_pagados ?  $prestamo->no_pagados->count() : 0 )}}</td>
                      <td class="text-danger">{{$prestamo->no_pagados ? $prestamo->plan->cuota * $prestamo->no_pagados->count() : 0}}</td>
                      <td>{{$prestamo->pagado}}</td>
                      <td>{{$prestamo->mora - $prestamo->mora_recuperada}}</td>
                      <td>{{$prestamo->capital_activo + $prestamo->plan->interes + $prestamo->mora - $prestamo->pagado}}</td>
                    </tr>
                    @php
                      $total_capital = $total_capital + $prestamo->capital_activo;
                      $total_mora = $total_mora + $prestamo->mora;
                      $total_pagado = $total_pagado +  $prestamo->pagado;
                      $total_pendiente = $total_pendiente + $prestamo->capital_activo - $prestamo->capital_recuperado;
                    @endphp
                @endforeach
              </tbody>
              <tfoot>
                <tr class="success">
                    <th>{{$hoja_ruta->prestamos->count()}} Cliente</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection


@section('scripts')

  <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>


<script type="text/javascript">
$(document).ready(function(){
  var oTable = $('#tabla-rutas').DataTable({
    "language": {
        "url": "{{asset('fonts/dataTablesEsp.json')}}",
    },
    "paging":   true,
    "info":     false,
    'dom' : 'tip',
    "footerCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;

          var intVal = function ( i ) {
              return typeof i === 'string' ?
                  i *1 :
                  typeof i === 'number' ?
                      i : 0;
          };

            // Update footer
            var que = api.columns('.sum', { page: 'current'}).every( function () {
              var sum = this
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                },0 );

              this.footer().innerHTML = sum;
            } );
        },
  });

  $('#select-rutas').change(function(){
    var ruta = $( "#select-rutas option:selected" ).val();
    oTable.search(ruta).draw();
  });

  $('#busqueda-rutas').keyup(function(){
    var ruta = $( "#select-rutas option:selected" ).val();
    var text = ruta + ' ' + $(this).val();
    oTable.search(text).draw();
  });
});

</script>


@endsection
