@extends('layouts.app')

@section('title', 'Rutas')

@section('content')

@section('nombre','Rutas')
@section('ruta')
  <li class="active">
      <strong>Rutas</strong>
  </li>
@endsection
<meta name="csrf-token" content="{{ csrf_token() }}" />

  <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox">
                <div class="ibox-title">
                  <h5>Rutas:</h5>
                </div>
                <div class="ibox-content">
                  <h5 class="text-success">Agregar nueva Ruta:</h5>
                  <hr>
                  <div class="row">
                    {!! Form::  open(['id'=> 'form-credito', 'route' => 'rutas.store','method' => 'POST']) !!}
                    <div class="form-group col-sm-4">
                      {{ Form::label('name', 'Nombre de ruta') }}
                      {{ Form::text('nombre', null, ['class' => 'form-control', 'id' => 'nombre', 'required']) }}
                    </div>
                        @if($promotores->isEmpty())
                          <div class="form-group col-sm-8">
                            {{ Form::label('name', 'Asignar a:') }}
                          <select class="form-control" name="promotor">
                            <option value="">No hay promotores</option>
                          </select>
                          </div>
                          {!! Form::button('Crear', ['class' => 'btn btn-info pull-right disabled']) !!}
                        @else
                          <div class="form-group col-sm-8">
                            {{ Form::label('name', 'Asignar a:') }}
                          <select class="form-control" name="promotor">
                          @foreach ($promotores as $promotor)
                            <option value="{{$promotor->id}}">{{$promotor->persona->nombre}} {{$promotor->persona->apellido}}</option>
                          @endforeach
                        </select>
                            {!! Form::submit('Crear', ['class' => 'btn btn-info pull-right']) !!}
                          </div>
                        @endif
                    {{Form::close()}}
                    <div class="col-md-12">
                      <h5 class="text-info">Rutas actuales:</h5>
                      <div class="table-responsive">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>Codigo</th>
                            <th>Nombre</th>
                            <th>No. Prestamos</th>
                            <th>Promotor</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                          @if($rutas->isEmpty())
                            <h3>No hay rutas</h3>
                          @else
                            @foreach ($rutas as $ruta)
                              <tr>
                                <td>{{$ruta->id}}</td>
                                <td>{{$ruta->nombre}}</td>
                                <td>{{$ruta->prestamos ? $ruta->prestamos->count() : 0}}</td>
                                <td>{{$ruta->user->persona->nombre}} {{$ruta->user->persona->apellido}}</td>
                                <td><a href="{{route('rutas.show', $ruta->id)}}" class="btn btn-outline btn-primary btn-sm" name="button"><span class="fa fa-eye"></span></a>
                                    <a value="{{$ruta->nombre . ";" . $ruta->id}}"  class="btn btn-outline btn-success btn-sm edit_ruta"><span class="fa fa-pencil"></span></a>
                                </td>
                              </tr>
                            @endforeach
                          @endif
                        </tbody>
                      </table>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>

  <script type="text/javascript">

  $('.edit_ruta').click(function(){
    var cadena = $(this).attr('value');
    var valores = cadena.split(';')
    comentario(valores[0], valores[1]);
  })

  function comentario(nombre, valor){
    swal({
      'title': "Ruta",
      content: {
        element: "input",
        attributes: {
          placeholder: nombre,
          name: valor,
          id: 'txt-observacion',
        },
      },
      buttons: {
      cancel: 'Cancelar',
      confirm: "Actualizar",
      },
    })
    .then((confirm) => {
      if (confirm) {
          updateRuta();
      } else {
        swal("No se actualizo la ruta");
      }
    });
  }

  function updateRuta(){
    var contenido = $('#txt-observacion').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var id = $('#txt-observacion').attr('name');
    $.ajax({
          /* the route pointing to the post function */
          url: '{{route('rutas.editar_rutas')}}',
          type: 'POST',
          /* send the csrf-token and the input to the controller */
          data: {_token: CSRF_TOKEN, observacion: contenido, id: id},
          // dataType: 'JSON',
          /* remind that 'data' is the response of the AjaxController */
          success: function (data) {
            swal("Observacion actualizada", {
              icon: "success",
            });
            location.reload(true);
          }
      });
  }

  </script>


@endsection
