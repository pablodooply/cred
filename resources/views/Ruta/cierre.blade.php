@extends('layouts.app')

@section('title', 'Ruta')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Ruta</h5>
        </div>
        <div class="ibox-content">
          <div class="input-group col-md-12">
            <input type="text" placeholder="Buscar credito " class="input form-control" id="busqueda-rutas">
          </div>
          <div class="table-responsive">
            <table class='table table-striped table-hover' id="tabla-rutas">
              <thead>
                <tr>
                  <th>Cliente</th>
                  <th>Prestamo</th>
                  <th>Estado</th>
                  <th>Fecha inicio</th>
                  <th>Fecha fin</th>
                  <th>Estado</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($hoja_ruta->prestamos as $prestamo)
                  @if($prestamo->ficha_actual()->isEmpty())
                  @else
                    <tr>
                      <td>{{$prestamo->cliente->persona->nombre}}</td>
                      <td>{{$prestamo->id}}</td>
                      {!!$prestamo->obtenerEstado()!!}
                      <td>{{$prestamo->fecha_inicio}}</td>
                      <td>{{$prestamo->fecha_fin}}</td>
                      {!!$prestamo->obtenerEstado()!!}
                    </tr>
                  @endif
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                    <th></th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th></th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection


@section('scripts')

  {{-- <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>


<script type="text/javascript">
$(document).ready(function(){
  var oTable = $('#tabla-rutas').DataTable({
    "paging":   true,
    "info":     false,
    'dom' : 'pti',
  });

  $('#busqueda-rutas').keyup(function(){
        oTable.search($(this).val()).draw();
  });
}); --}}

</script>


@endsection
