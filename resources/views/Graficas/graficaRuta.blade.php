@extends('layouts.app')

@section('title', 'Perfil cliente')

@section("link")
  <link href="{{asset('css/rangedatepicker/daterangepicker.css')}}" rel="stylesheet"  media="screen">
@endsection

@section('content')

@section('nombre','Grafica')
@section('ruta')
  <li class="active">
      <strong>Grafica</strong>
  </li>
@endsection
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
       <div class="ibox float-e-margins">
          <div class="ibox-title">
              <h5 id="titulo">Grafica de todas las rutas</h5>
          </div>
          <div class="ibox-content">
            <div class="row">
              <div class="col-md-6">
                <label>Ruta:</label>
                <select class="form-control" name="" id="select-ruta">
                  <option value="0">Todas</option>
                  @foreach ($hojas_ruta as $hoja)
                    <option value="{{$hoja->id}}">{{$hoja->nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-6">
                <label>Rango:</label>
                  <div class="input-group">
                    <div class="form-group">
                      <input id="rangedate" class="form-control" type="text" name="" value="">
                    </div>
                    <span class="input-group-btn">
                      <a id="busqueda" class="btn btn-primary"> <i class="fa fa-bar-chart-o"></i> Graficar</a>
                      {{-- <a id="inverted" class="btn btn-primary"> <i class="fa fa-search"></i> Invertir</a>  --}}
                    </span>
                </div>
            </div>
            </div>
            <div class="row">

              <div class="col-sm-12">
                <div id="container"></div>
              </div>
            </div>
          </div>
      </div>
      </div>

    </div>
  </div>
@endsection

@section('scripts')
<script src="{!! asset('js/slimscroll/jquery.slimscroll.min.js') !!}"></script>

{{-- <script src="{!! asset('js/graph/highcharts.js')!!}"></script> --}}
{{-- <script src="{!! asset('js/graph/series-label.js')!!}"></script>
<script src="{!! asset('js/graph/exporting.js')!!}"></script> --}}

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script type="text/javascript" src="{!! asset('js/grafica.js')!!}"></script>
<script type="text/javascript" src="{{asset('js/moment/moment.js')}}"></script>
<script type="text/javascript" src="{{asset('js/rangedatepicker/daterangepicker.js')}}"></script>

<script type="text/javascript">
var variable;
$(document).ready(function(){

  $("#rangedate").daterangepicker({
    "locale": {
      "format": "DD/MM/YYYY",
      "fromLabel": "Desde",
      "toLabel": "Hasta",
      "applyLabel": "Aplicar",
      "cancelLabel": "Cancelar",
      "daysOfWeek": [
        "Dom",
        "Lun",
        "Mar",
        "Mie",
        "Jue",
        "Vie",
        "Sab"
      ],
      "monthNames": [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre"
      ],
    }
  });

  $("#busqueda").click(function(){
    var tipo = $("#select-ruta option:selected").val();
    var text_select = $("#select-ruta option:selected").text();
    if(tipo==0){
      var text1 = "Grafica de todas las rutas";
      $('#titulo').text(text1);
    } else{
      var text2 = "Grafica de la ruta " + text_select;
      $('#titulo').text(text2);
    }
    var text2 = ""
        graficar();
    });

    function graficar(){
      var url = "{{route('graficas.ruta')}}";
      var rango_fechas = $("#rangedate").val();
      var valores = rango_fechas.split(' - ');
      var tipo = $("#select-ruta option:selected").val();
      var inicio = valores[0];
      var fin = valores[1];
      $.ajax({
          type: "GET",
          url: url,
          data: {
            'inicio' : inicio,
            'fin' : fin,
            'tipo' : tipo,
          },
          success: function( response ) {
            console.log(response);
            cargar_grafica_barra(response);
          }
      });
    }
    // graficar();
});
</script>


@endsection
