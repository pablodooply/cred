@extends('layouts.app')

@section('title', 'Minor page')

@section('content')

  {{-- {{Form::text('text1', null, ['id_text1'])}}
  {{Form::text('text2', null, ['text2'])}}
  <a class="btn -btn">Probar</a> --}}
  <div class="wrapp
  er wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
              {!! $dataTable->table()  !!}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>

  <link rel="stylesheet" href="{{asset('css/dataTables/buttons.dataTables.min.css')}}">
  <script src="{{asset('js/dataTables/dataTables.buttons.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/dataTables/buttons.server-side.js')}}"></script>
  {!! $dataTable->scripts() !!}
@endsection
