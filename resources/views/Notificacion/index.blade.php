<table class="table table-hover">
  <tbody>
    @if(!isset($prestamos))
      @if(Auth::user()->hasRole('Administrador'))
        <h5>No hay prestamos examinados</h5>
      @else
        <h5>No hay prestamos pendientes</h5>
      @endif
    @else
      @foreach ($prestamos as $prestamo)
        <tr class="unread">
          <td>
          </td>
          <td value='{{$prestamo->id}}' class="mail-ontact pendiente-link"><a>Solicitud de credito <span class="text-success">cre-{{$prestamo->id}}</span> </a></td>
          <td class="mail-subject">Se ha solicitado un nuevo credito</td>
          <td class="text-center"><span class="text-warning text-center">RUTA-{{$prestamo->ruta->first()->hoja_ruta->nombre}}</span></td>
          <td class="text-right mail-date">{{$prestamo->created_at}}</td>
        </tr>
      @endforeach
    @endif
  </tbody>
</table>
