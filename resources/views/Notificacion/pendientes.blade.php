@extends('layouts.app')

@section('title', 'Pendientes')

@section('link')
  <link href="{{asset('css/ladda/ladda-themeless.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/timepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" media="screen">
@endsection
@section('content')

@section('nombre','Solicitudes pendientes')

@section('ruta')

<li class="active">
    <strong>Pendientes</strong>
</li>
@endsection

<body class="pace-done">

<div class="wrapper wrapper-content">
<div class="row">
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-content mailbox-content">
                <div class="file-manager">
                    <div class="space-25"></div>
                    <h5>Prestamos</h5>
                    <ul class="folder-list m-b-md" style="padding: 0">
                      {{-- @if(Auth::user()->hasRole('Administrador'))
                        <li><a href="{{route('Notificacion.examinados')}}" class="examinados"> <i class="fa fa-inbox "></i> Examinados <span class="label label-warning pull-right">{{$prestamos_examinados->count()}}</span> </a></li>
                      @endif --}}
                        <li><a href="{{route('Notificacion.pendientes')}}" class="pendientes"> <i class="fa fa-inbox "></i> Pendientes <span class="label label-warning pull-right">{{$prestamos->count()}}</span> </a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-9 animated fadeInRight">
      <div class="mail-box-header">
          <h2 id="titulo">
              Pendientes
          </h2>
      </div>
    <div class="mail-box">
      <div id=detalle_credito>
        @include('Notificacion.index', ['prestamos' => $prestamos])
      </div>
    </div>
  </div>
</div>
</div>

</body>
@endsection


@section('scripts')
  <script src="{{asset('js/ladda/spin.min.js')}}"></script>
  <script src="{{asset('js/ladda/ladda.min.js')}}"></script>
  <script src="{{asset('js/ladda/ladda.jquery.min.js')}}"></script>
  <script src="{{asset ('js/timepicker/bootstrap-datetimepicker.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function(){
      	$(".pendiente-link").click(function(){
            var valor = $(this).attr("value");
            var url = '{{ route("notificacion.show", ":id") }}';
            url = url.replace(':id', valor);
            $.ajax({
                type: "GET",
                url: url,
                success: function( response ) {
                  $('#detalle_credito').html(response);
                }
            });

      	});

        $(".crear-notificacion").click(function(){
            var url = '{{ route("notificacion.create") }}';
            $.ajax({
                type: "GET",
                url: url,
                success: function( response ) {
                  console.log(url);
                  $('#detalle_credito').html(response);
                  $('#titulo').text('Creacion de notificacion');
                }
            });
      	});
  });

  $('#timepicker').datetimepicker({
        format: 'hh:ii',
        language:  'es',
        weekStart: 1,
        todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 1,
    minView: 0,
    maxView: 1,
    forceParse: 0
    });

</script>

@endsection
