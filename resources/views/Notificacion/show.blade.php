<div class="col-md-12">

  @if($prestamos_ant->isEmpty())

    <h5>No hay registros de prestamos anteriores</h5>

  @else

    <table class='table table-striped table-hover'>

      <thead>

        <tr>

          <th>Prestamo</th>

          <th>Monto</th>

          <th>Clasificacion</th>

        </tr>

      </thead>

      <tbody>

          @foreach ($prestamos_ant as $prestamo_ant)

          <tr>

            <td><a class"client-link" href="{{route('creditos.show', $prestamo_ant->id)}}">Cre-{{$prestamo_ant->id}}</a></td>

            <td>{{$prestamo_ant->monto}}</td>

            {!!$prestamo_ant->obtenerClasificacion()!!}

          </tr>

          @endforeach

  @endif

      </tbody>

    </table>

</div>



<div class="row">

<div class="col-md-12">

    <div class="col-md-12">

      <p><strong>Datos de cliente personales:</strong></p>

      <div class="col-md-6">

        <ul class="list-group clear-list">

          <li class="list-group-item fist-item">

            Nombre Completo:

            <span class="pull-right">{{$prestamo->cliente->persona->nombre}} {{$prestamo->cliente->persona->apellido}}</span>

          </li>

          <li class="list-group-item">

            DPI:

            <span class="pull-right">{{$prestamo->cliente->persona->dpi}}</span>

          </li>

          <li class="list-group-item">

            NIT:

            <span class="pull-right">{{$prestamo->cliente->persona->nit}}</span>

          </li>

          <li class="list-group-item">

            Fecha nacimiento:

            <span class="pull-right">{{$prestamo->cliente->persona->fecha_nacimiento}} </span>

          </li>

          <li class="list-group-item">

            Direccion domicilio:

            <span class="pull-right">{{$prestamo->cliente->persona->domicilio}}</span>

          </li>

          <li class="list-group-item">

            Tipo de casa:

            <span class="pull-right">{{$prestamo->cliente->tipo_casa}}</span>

          </li>

          <li class="list-group-item">

            Nombre recibo de luz:

            <span class="pull-right">{{$prestamo->cliente->nombre_recibo}}</span>

          </li>

        </ul>

      </div>

      <div class="col-md-6">

        <ul class="list-group clear-list">

          <li class="list-group-item fist-item">

            Teléfono:

            <span class="pull-right">{{$prestamo->cliente->persona->telefono}} </span>

          </li>

          <li class="list-group-item">

            Celular 1:

            <span class="pull-right">{{$prestamo->cliente->persona->celular1}}</span>

          </li>

          <li class="list-group-item">

            Celular 2:

            <span class="pull-right">{{isset($prestamo->cliente->persona->celular2) ? $prestamo->cliente->persona->celular2 : 'No ingresado'}}</span>

          </li>

          <li class="list-group-item">

            Genero:

            @if($prestamo->cliente->persona->genero==1)

             <span class="pull-right">Masculino</span>

            @else

             <span class="pull-right">Femenino</span>

            @endif

          </li>

          <li class="list-group-item">

            Estado civil:

            <span class="pull-right">{{$prestamo->cliente->estado_civil}}</span>

          </li>

          <li class="list-group-item">

            Actividad economica:

            <span class="pull-right">{{$prestamo->cliente->actividad}}</span>

          </li>

          <li class="list-group-item">

            Direccion recibo de luz:

            <span class="pull-right">{{$prestamo->cliente->direccion_recibo}} </span>

          </li>

        </ul>

      </div>

    </div>

    <div class="col-md-12">

      <p><strong>Datos Trabajo:</strong></p>

      <div class="col-md-6">

        <ul class="list-group clear-list">

          <li class="list-group-item fist-item">

            Empresa:

            <span class="pull-right">{{$prestamo->cliente->empresa_trabajo}}</span>

          </li>

          <li class="list-group-item">

            Tiempo trabajando:

            <span class="pull-right">{{$prestamo->cliente->tiempo_trabajando}}</span>

          </li>

        </ul>

      </div>

      <div class="col-md-6">

        <ul class="list-group clear-list">

          <li class="list-group-item fist-item">

            Telefono empresa:

            <span class="pull-right">{{$prestamo->cliente->empresa_trabajo}}</span>

          </li>

          <li class="list-group-item">

            Sueldo:

            <span class="pull-right">{{$prestamo->cliente->salario}}</span>

          </li>

        </ul>

      </div>

    </div>

    <div class="col-md-12">

      <p><strong>Referencia personales:</strong></p>

      @foreach ($prestamo->cliente->referencias as $referencia)

        <div class="col-md-4">

          <ul class="list-group clear-list">

            <li class="list-group-item fist-item">

              Nombre:

              <span class="pull-right">{{$referencia->nombre}} {{$referencia->apellido}}</span>

            </li>

            <li class="list-group-item fist-item">

              Direccion:

              <span class="pull-right">{{$referencia->direccion}}</span>

            </li>

            <li class="list-group-item fist-item">

              Telefono:

              <span class="pull-right">{{$referencia->telefono}}</span>

            </li>

          </ul>

        </div>

      @endforeach

    </div>

    {!! Form::open(['route' => ['creditos.update',$prestamo->id], 'method' => 'PUT', 'id' => 'form-create']) !!}

            @php
                $month = date('m');
                $day = date('d');
                $year = date('Y');
                $today = $year . '-' . $month . '-' . $day;
            @endphp
    <div class="col-md-12">

      <p><strong>Datos Prestamo:</strong></p>

      <div class="col-md-6">

        <ul class="list-group clear-list">

          <li class="list-group-item ">

            Fecha posible desembolso:

            {{-- {{$prestamo->fecha_desembolso}} --}}
               <input type="date" id="bday" class="form-control" name="fecha_desembolso" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value="{{$prestamo->fecha_desembolso}}">


          </li>

          <li class="list-group-item ">

            Hora:

              {{ Form::time('hora', $prestamo->ruta->first()->hora, ['class' => 'form-control', 'id' => 'timepicker', 'autocomplete' => 'off'])}}

          </li>

          <li class="list-group-item">

            Plan:

            <select id="tipoPlan"class="form-control" name="plan">

              @if($prestamo->plan->nombre == 'Diario')

                <option id="1" value="Diario" selected>Diario</option>

                <option id="2" value="Semanal">Semanal</option>

                <option id="3" value="Quincena">Quincena</option>

              @endif

              @if($prestamo->plan->nombre == 'Semanal')

                <option id="2" value="Semanal" selected>Semanal</option>

                <option id="1" value="Diario">Diario</option>

                <option id="3" value="Quincena">Quincena</option>

              @endif

              @if($prestamo->plan->nombre == 'Quincena')

                <option id="3" value="Quincena" selected>Quincena</option>

                <option id="1" value="Diario">Diario</option>

                <option id="2" value="Semanal">Semanal</option>

              @endif

            </select>

          </li>

          <li class="list-group-item">

            Cantidad:

            {{-- <span class="pull-right">{{$prestamo->capital_activo}}</span> --}}

            {{-- <select id="monto" class="form-control" name="monto"> --}}

              {{-- <option value="{{$prestamo->plan->id}}">{{$prestamo->plan->capital}}</option> --}}

              {!! Form::select('monto', $planes, $prestamo->plan->id, ['class' => 'form-control', 'required' => 'required', 'id' => 'monto']) !!}

            {{-- </select> --}}

          </li>

          <li class="list-group-item">

            Promotor quien lo coloco:

            <span class="pull-right">{{$prestamo->user->persona->nombre}} </span>

          </li>

          <li class="list-group-item">

            Ruta:

            <span class="pull-right">{{$prestamo->ruta->first()->hoja_ruta->nombre}}</span>

          </li>

          <li class="list-group-item">

            Saldo Total Pendiente:

            <span class="pull-right text-danger">{{$saldototal}}</span>

          </li>

        </ul>

      </div>

      <div class="col-md-6">

        <ul class="list-group clear-list">

          <li class="list-group-item fist-item">

            Fecha posible 1er. Pago:

            @php

              $fecha_primer_pago = \Carbon\Carbon::parse($prestamo->posible_primer_pago);

            @endphp

            {{-- {{$fecha_primer_pago->addDay(1)->format('Y-m-d')}} --}}

            {{$fecha_primer_pago->format('Y-m-d')}}

          </li>

          <li class="list-group-item">

            Promotor de ruta:

            <span class="pull-right">{{$prestamo->ruta->first()->hoja_ruta->user->persona->nombre}}

            {{$prestamo->ruta->first()->hoja_ruta->user->persona->apellido}}</span>

          </li>

          <li class="list-group-item">

            No. pagos:

            {{-- <span class="pull-right">{{$prestamo->plan->nombre}}</span> --}}

            <select class="form-control" name="no_pagos" id="no_pagos">

              <option value="{{$prestamo->plan->periodo->id}}">{{$prestamo->plan->periodo->tiempo}}</option>

              @foreach ($periodos as $periodo)

                @if($periodo->id != $prestamo->plan->periodo->id)

                  <option value="{{$periodo->id}}">{{$periodo->tiempo}}</option>

                @endif

              @endforeach

            </select>

          </li>

          <li class="list-group-item">

            Mora:

            <span class="pull-right" id="mora">{{$prestamo->plan->mora}}</span>

          </li>

          <li class="list-group-item">

            Interes:

            <span class="pull-right" id="interes">{{$prestamo->plan->interes}}</span>

          </li>

          <li class="list-group-item">

            Total a pagar:

            <span class="pull-right" id="total"><b>{{$prestamo->plan->total}}</b></span>

          </li>

          <li class="list-group-item">

            {{-- <a class="btn btn-success" href="#">Cambiar plan</a> --}}

          </li>

        </ul>

      </div>

    </div>

    <div class="form-group col-md-12">

      <p><strong>Observaciones:</strong></p>

      <input type="text" class="form-control" name="observaciones" value="">

    </div>

    <div class="clearfix h-100"></div>

    {{-- <button class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in">Submit</button> --}}



    <input name="aprobado" type="hidden" value="true">

    @if(Auth::user()->hasAnyRole(['Administrador','Supervisor']))

      {{ Form::submit('Aprobar', ['class' => 'ladda-button ladda-button-demo btn btn-md btn-primary', 'name' => 'submitbutton', 'value' => 'aprobar']) }}

    @else

      @if($saldototal>0)

        <a name="button" class="btn btn-md btn-primary btn-aprobar-renovacion">Aprobar</a>

      @else

      {{ Form::submit('Aprobar', ['class' => 'ladda-button ladda-button-demo btn btn-md btn-primary', 'name' => 'submitbutton', 'value' => 'aprobar']) }}

      @endif

    @endif

    {{-- {{ Form::button('Prueba', ['class' => 'ladda-button ladda-button-demo btn btn-md btn-primary', 'name' => 'submitbutton','data-style' => 'zoom-in']) }} --}}

    {{ Form::submit('Rechazar',['class' => 'btn btn-md btn-danger','name' => 'submitbutton', 'value' => 'rechazar'])}}

    {!! Form::close() !!}

</div>

</div>



<script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset ('js/validate/jquery.validate.min.js')}}"></script>

<script type="text/javascript">

var l = $( '.ladda-button-demo' ).ladda();

var aprobar_renovacion = $('.btn-aprobar-renovacion');

var form = $('#form-create');



aprobar_renovacion.on('click', function(){

  swal({

  icon: 'error',

  title: 'Saldo Pendiente',

  text: 'Este cliente no puede renovar crédito debido a que tiene saldo pendiente total de '+ {{$saldototal}}+' en alguno de sus prestamos anteriores',

  footer: '<a href>Why do I have this issue?</a>'

})

});



l.on('click',function(){

    // Start loading

    l.ladda( 'start' );

    form.submit();



    // Timeout example

    // Do something in backend and then stop ladda

    setTimeout(function(){

        l.ladda('stop');

    },12000)

});



$("#tipoPlan").change(function(){

  obtenerNoPagos();

});



$("#no_pagos").change(function(){

  obtenerMontos();

});



$("#monto").change(function(){

  obtenerInfo();

});



function obtenerNoPagos(){

  var plan = $("#tipoPlan :selected").attr("value");

  $.ajax({

    type: "GET",

    url: "{{route('info.nopagos')}}",

    data: {

      tipo: plan,

    },

    success: function( response ) {

      $("#no_pagos").empty();

      $.each(response, function(id,tiempo){

          $("#no_pagos").append('<option value="'+ tiempo.id+'">'+  tiempo.tiempo +'</option>');

        });

        obtenerMontos();

    }

  });

}



function obtenerMontos(){

  var periodo = $("#no_pagos :selected").attr("value"); // The text content of the selected option



  $.ajax({

    type: 'GET',

    url: "{{route('info.montos')}}",

    data: {

      tipo: periodo,

    },

    success: function( response ) {

      $("#monto").empty();

      $.each(response, function(id,total){

          $("#monto").append('<option value="'+total.id+'">'+ total.capital  +'</option>');

        });

        obtenerInfo();

    }

  });

}





var valores;

function obtenerInfo(){

  var plan = $("#monto :selected").attr("value");

  $.ajax({

    type: "GET",

    url: "{{route('info.infoPlan')}}",

    data: {

      tipo: plan

    },

    success: function( response ) {

      valores = response;

      $('#interes').val(response['interes']);

      $('#interes').text(response['interes']);

      $('#mora').val(response['mora']);

      $('#mora').text(response['mora']);

      $('#total').val(response['total']);

      $('#total').text(response['total']);

    },

  });

}

var form = $("#form-create");
form.validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
        cantidad: {
            required: true,
            number:   true,
        },

        no_pagos: {
            required: true,
            number: true
        },

        interes: {
            required: true,
            number: true
        },

        mora: {
            required: true,
            number: true
        },

        hora: {
            required: true,
        },
    }
});

</script>
