@extends('layouts.app')

@section('title', 'Configuracion')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">

                <div class="row">
                    <div class="col-lg-12">
                      <div class="ibox float-e-margins">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="ibox-title">
                          <h5>Configuracion</h5>
                        </div>
                        <div class="ibox-content">
                          <div class="form-row col-sm-12">
                            {!! Form::  open(['id'=> 'formulario','route' => 'configuracion.store', 'method' => 'POST']) !!}

                              <!--*****************Acronimo******************-->
                              <div class="form-group col-sm-4">
                                  {{ Form::label('name', 'Acronimo') }}
                                  {{ Form::text('acronimo', null, ['class' => 'form-control', 'id' => 'no_cuota']) }}
                              </div>
                              <!--*****************Nombre******************-->
                              <div class="form-group col-sm-4">
                                  {{ Form::label('name', 'Nombre') }}
                                  {{ Form::text('nombre', null, ['class' => 'form-control', 'id' => 'no_cuota']) }}
                              </div>
                              <!--*****************Telefono******************-->
                              <div class="form-group col-sm-4">
                                  {{ Form::label('name', 'Capital') }}
                                  {{ Form::text('capital', null, ['class' => 'form-control', 'id' => 'cuota']) }}
                              </div>
                          </div>

                          <div class="form-row col-sm-12">
                              <!--*****************DPI******************-->
                              <div class="form-group col-sm-6">
                                  {{ Form::label('name', 'Direccion') }}
                                  {{ Form::text('direccion', null, ['class' => 'form-control', 'id' => 'cuota']) }}
                              </div>
                              <!--*****************NIT******************-->
                              <div class="form-group col-sm-6">
                                  {{ Form::label('name', 'Telefono') }}
                                  {{ Form::text('telefono', null, ['class' => 'form-control', 'id' => 'nit' ]) }}
                              </div>
                          </div>

                          <div class="form-row">
                              <div class="form-group" id="guardar">
                                {{ Form::submit('Configurar', ['class' => 'btn btn-md btn-primary', 'name' => 'submitbutton']) }}
                                {{ Form::button('Cancelar',['class' => 'btn btn-md btn-danger','name' => 'submitbutton', 'value' => 'no_pago'])}}
                            </div>
                          </div>
                        </div>
                      {!! Form::close() !!}

                      </div>


                    </div>
                </div>
            </div>
@endsection

@section('scripts')
{{-- <script>

$(document).ready(function(){
      $("#cliente").change(function(){
        var valor = $("#cliente :selected").attr("value"); // The text content of the selected option
        var url = '{{ route("pagos.info", ":id") }}';
        url = url.replace(':id', valor);
        $.ajax({
            type: "GET",
            url: url,
            success: function( response ) {
              $('#no_cuota').val(response['no_cuota']);
              $('#no_cuota').val(response['no_cuota']);
              $('#cuota').val(response['cuota']);
            }
        });
      });
});

</script> --}}


@endsection
