@extends('layouts.app')

@section('title', 'Reportes')

@section('link')
  <link rel="stylesheet" href="{!! asset('css/dataTables/datatables.min.css') !!}" />
@endsection

@section('content')

@section('nombre','Reportes')
@section('ruta')
  <li class="active">
      <strong>Reportes clasificacion</strong>
  </li>
@endsection
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Reportes </h5>
        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-wrench"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content">
        <div class="row">
          <div class="col-sm-3 m-b-xs">
            <div class="form-group">
              <label class='control-label'>Tipo</label>
                <select id="tipo" class="input-sm form-control input-s-sm inline">
                <option value="0">Colocado</option>
                <option value="1">Cobrado</option>
                <option value="2">Pendiente</option>
                <option value="3">En mora</option>
                <option value="4">Vencido</option>
            </select>
            </div>
        </div>
        <div class="col-sm-6 m-b-xs">
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Desde:</label>
              {{ Form::date('fecha_inicio', \Carbon\Carbon::now()->startOfMonth()->subMonth(),['class' => 'form-control', 'id' => 'inicioFecha'])}}
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Hasta:</label>
              {{ Form::date('fecha_fin', \Carbon\Carbon::now(),['class' => 'form-control', 'id' => 'finFecha'])}}
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
            <label class="control-label"> </label>
            <button type="submit" id="busqueda" class="btn btn-primary pull-center" name="button">Buscar</button>
          </div>
        </div>
        </div>
        <div class="row">
            <div class="col-xs-12 table-responsive">
          <table class="table table-striped table-bordered table-hover dataTables-example" >
            <thead>
              <tr>
                <th>Plan</th>
                <th class="sum">Capital</th>
                <th class="sum">Interes</th>
                <th class="sum">Mora</th>
                <th class="sum">Total</th>
              </tr>
            </thead>
          <tbody>

          </tbody>
          <tfoot>
          <tr>
            <th>Total: </th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
          </tfoot>
          </table>
        </div>
        </div>
      </div>
    </div>
  </div>

</div>
</div>
@endsection

@section('scripts')

  <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>

  <script>
      $(document).ready(function(){
          var  oTable = $('.dataTables-example').DataTable({
            "language": {
                "url": "{{asset('fonts/dataTablesEsp.json')}}",
            },
              pageLength: 10,
              "processing": true,
              "serverSide": true,
              ajax: {
                   dataType:"json",
                   url: '{{route('reportes.clasificacion')}}',
                   data: function (d) {
                       d.inicio = $('input[name=fecha_inicio]').val();
                       d.fin = $('input[name=fecha_fin]').val();
                       var idPlan = $("#plan option:selected").val();
                       var idTipo = $("#tipo option:selected").val();
                       d.plan = 0;
                       d.tipo = idTipo;
                   }
               },
              'columns': [
                {data: 'plan'},
                {data: 'capital'},
                {data: 'interes'},
                {data: 'mora'},
                {data: 'total'},
              ],
              dom: '<"html5buttons"B>lTfgitp',
              buttons: [
                  { extend: 'copy'},
                  {extend: 'csv'},
                  {extend: 'excel', title: 'Reportes'},
                  {extend: 'pdf', title: 'Reportes'},

                  {extend: 'print',
                   customize: function (win){
                          $(win.document.body).addClass('white-bg');
                          $(win.document.body).css('font-size', '10px');

                          $(win.document.body).find('table')
                                  .addClass('compact')
                                  .css('font-size', 'inherit');
                  }
                  }
              ],
              "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i *1 :
                    typeof i === 'number' ?
                        i : 0;
            };

          // Total over all pages
          total = api
              .column( 1 )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
              }, 0 );

              console.log(total);

              // Update footer
              api.columns('.sum', { page: 'current'}).every( function () {
                var sum = this
                  .data()
                  .reduce( function (a, b) {
                      return intVal(a) + intVal(b);
                  }, 0 );

                this.footer().innerHTML = parseFloat(sum).toFixed(2);
              } );
        }
          });

          $('#busqueda').on('click', function(e) {
              oTable.draw();
              e.preventDefault();
          });
});

  </script>

@endsection
