<div class="row">
  <div class="col-md-4 m-b-xs">
    <div class="col-sm-12">
      <div class="form-group">
        <label class="control-label">Tipo</label>
        <select class="form-control" name="tipo_reporte" id="tipo_reporte">
          <option value="1">Promotores</option>
          <option value="2">Supervisores</option>
        </select>
      </div>
    </div>
  </div>
  {{-- <div class="col-md-3 m-b-xs">
    <div class="col-sm-12">
      <div class="form-group">
        <label class="control-label">Plan</label>
        <select class="form-control" name="tipo_reporte" id="tipo_plan">
          <option value="0">Todos</option>
          <option value="1">Diario</option>
          <option value="2">Semanal</option>
          <option value="2">Quincena</option>
        </select>
      </div>
    </div>
  </div> --}}
<div class="col-md-8 m-b-xs">
  <div class="col-md-6">
    <div class="form-group">
      <label class="control-label">Desde:</label>
      {{ Form::date('fecha_inicio', \Carbon\Carbon::now()->startOfMonth(),['class' => 'form-control', 'id' => 'inicioFecha'])}}
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label class="control-label">Hasta:</label>
      {{ Form::date('fecha_fin', \Carbon\Carbon::now(),['class' => 'form-control', 'id' => 'finFecha'])}}
    </div>
  </div>
</div>
<div class="col-sm-12">
  <div class="form-group">
    <label class="control-label"></label>
    <button type="submit" id="busqueda" class="btn btn-primary pull-right" name="button">Buscar</button>
  </div>
</div>
</div>
<div class="row">
  <div class="table-responsive col-xs-12">
    <table class="table table-striped table-bordered table-hover" id="tabla-filtrada">
      <thead>
        <tr>
          <th>Colaborador</th>
          <th class="sum">Capital R.</th>
          <th class="sum">Mora R.</th>
          <th class="sum">Rutas</th>
          <th class="sum"># Clientes</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th>Total: </th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </tfoot>
    </table>
  </div>
</div>


<script type="text/javascript">
$(document).ready(function(){
    var  oTable = $('#tabla-filtrada').DataTable({
      "language": {
          "url": "{{asset('fonts/dataTablesEsp.json')}}",
      },
      pageLength: 10,
      "processing": true,
      "serverSide": true,
      'columns': [
        {data: 'nombre'},
        {data: 'total_capital'},
        {data: 'total_mora'},
        {data: 'rutas'},
        {data: 'total_clientes'},
      ],
      ajax: {
           url: '{{route('reportes.promotor_dos')}}',
           data: function (d) {
               d.inicio = $('input[name=fecha_inicio]').val();
               d.fin = $('input[name=fecha_fin]').val();
               // d.tipo_plan = $('#tipo_plan option:selected').val();
               d.tipo_plan = 0;
               d.tipo_reporte = $('#tipo_reporte option:selected').val();
           }
       },
    });

    $('#busqueda').on('click', function(e) {
        oTable.draw();
        e.preventDefault();
    });
  });

</script>
