<!DOCTYPE html>

<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title></title>
    <style type="text/css">
    @media print {
        body {
        /* transform: scale(.5); */
        /* margin-top: 1cm; */
        /* : 2.5cm; */
        margin-left: 2cm;
        margin-right: 2cm;
        /* zoom: 50%; */
            }

        }
    .ble {
        border-left: 1px black solid;
    }

    .bri {
        border-right: 1px black solid;
    }

    .bbo {
        border-bottom: 1px black solid;
    }

    .bto {
        border-top: 1px black solid;
    }

    .bco {
        border: 1px black solid;
    }

    table td.equis
    {
       background-image: linear-gradient(to bottom right,  transparent calc(50% - 1px), black, transparent calc(50% + 1px));
    }
    </style>
  </head>
  @php
  use Carbon\Carbon;
  setlocale(LC_TIME, 'es_AR.utf8');
  Carbon::setUtf8(false);
@endphp
  <body>
    <div class="row">
      <div class="container">
          <h5>{{Carbon::now()->toTimeString()}}</h5>
          <h4 class="pull-right">RUTA {{$credito->ruta->first()->hoja_ruta->nombre}}</h4>
          <p><br></p>
          <h2 align="center">DATOS DE SOLICITUD</h2>
          <table>
            <tbody>
              <tr>
                <td colspan="3"style="width: 105mm; height: 11mm;"><img style="height: 15mm; width: 100mm" src="{{asset('images//system//logo_credi.png')}}"></img></td>
                <td valign="top" class="bco" style="width: 71mm; height: 11mm;">&nbsp; Fecha: <strong style="font-size: 18px"> &nbsp; &nbsp; &nbsp; {{Carbon::parse($credito->created_at)->format('d/m/Y')}} </strong></td>
              </tr>
              <tr>
                <td class="bri ble" style="width: 35mm; height: 11mm" align="center">CODIGO CLIENTE: <br> <strong>{{$credito->cliente->id}} </strong></td>
                <td colspan="2" style="width: 70mm; height: 14mm">
                  <table >
                    <tr>
                      <td style="width:5mm;" ></td>
                      <td class="bco" valign="top" style="width: 30mm; height:14mm;" align="center">CODIGO PTMO<br> <strong>{{$credito->id}}</strong> </td>
                      <td style="width:3mm;" ></td>
                      <td class="bco" valign="top" style="width: 30mm; height:14mm;" align="center">Hora:<br> </td>
                    </tr>
                  </table>
                </td>
                <td class="ble bri" style="width: 70mm; height: 11mm">Fecha de aprobacion:</td>
              </tr>
              <tr class="bco">
                <td valign="top" colspan="5" style="height: 15mm;"><t style="top">&nbsp; NOMBRE Y APELLIDO: <br>
                &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong style="text-transform: uppercase;"> {{$credito->cliente->persona->nombre . " " . $credito->cliente->persona->apellido}} <strong></t></td>
              </tr>
              <tr>
              </tbody>
            </table>
            <table> <!-- style="border-color: black;" border="1" -->
              <tbody>
                <tr style="border-left: 1px black solid; border-bottom: 1px black solid; border-right: 1px black solid;">
                <td valign="top" style="height: 15mm; width: 10mm">&nbsp; CUI: </td>
                <td>
                  <table style="border-color: black;" border="1">
                    <tr>
                      <td style="height: 11mm; width: 95mm;" align="center"><b style="font-size: 20px">{{$credito->cliente->persona->dpi}}</b></td>
                    </tr>
                  </table>
                </td>
                <td style="width: 20mm"></td>
                <td valign="top" style="width: 15mm;" border=0> Sexo: </td>
                <td>
                  <table>
                    <tr>
                      <td class="equis" align="center" style="width: 11mm; height: 10mm; border: 1px black solid">M</td>
                      <td style="width: 5mm"></td>
                      <td align="center" style="width: 11mm; border: 1px black solid">F</td>
                      <td style="width: 8.3mm"></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <table>
            <tr style="border-left: 1px black solid; border-bottom: 1px black solid; border-right: 1px black solid; ">
              <td style="height: 11mm; width: 35mm; border-right: 1px black solid" valign="top">&nbsp; NACIONALIDAD:
                  <br> &nbsp; &nbsp; &nbsp;<strong>{{$credito->cliente->nacionalidad}}</strong></td>
              <td style="height: 11mm; width: 39mm;" valign="top">&nbsp; &nbsp; FECHA DE NACIMIENTO:</td>
              <td style="width: 31mm">
                <table>
                  <tr>
                    @php
                      $fecha = Carbon::parse($credito->cliente->persona->fecha_nacimiento);
                    @endphp
                    <td style="width: 10mm; height: 8mm" align="center" class="bco">{{$fecha->day}}</td>
                    <td style="width: 10mm;" align="center" class="bco">{{$fecha->month}}</td>
                    <td style="width: 10mm;" align="center" class="bco">{{$fecha->year}}</td>
                  </tr>
                </table>
              </td>
              <td class="ble" style="width: 70.9mm;"> &nbsp;  &nbsp; LUGAR DE NACIMIENTO <br> &nbsp; &nbsp; &nbsp; &nbsp; <b style="text-transform: uppercase;">{{$credito->cliente->persona->location->states->name . "," . $credito->cliente->persona->location->cities->name}}<b> </td>
            </tr>
          </table>
          <table>
            <tr>
              <tr class="ble bri bbo">
                <td style="width: 85mm; height: 11mm">&nbsp; VECINDAD: <br><strong style="text-transform: uppercase;">&nbsp; &nbsp; {{$credito->cliente->persona->location->states->name . "," . $credito->cliente->persona->location->cities->name}} </strong></td>
                <td style="width: 45.5mm" >TELEFONO1: <br> <strong>&nbsp; &nbsp; {{$credito->cliente->persona->telefono}} </strong></td>
                <td style="width: 45.6mm" >TELEFONO2: <br> <strong>&nbsp; &nbsp; {{$credito->cliente->persona->celular1}}</strong> </td>
              </tr>
            </tr>
          </table>
          <table>
            <tr class="ble bri bbo">
              <td style="width: 176.4mm; height: 21mm; vertical-align: middle;" align="center"><b><u>DATOS DE RECIBO</u></b></td>
            </tr>
          </table>
          <table>
            <tr style="width: 176.4mm; height: 11mm" class="ble bri bbo">
              <td style="width: 88.2mm;" valign="top"> &nbsp; &nbsp; Nombre: <br> &nbsp; &nbsp; &nbsp; &nbsp;
               <b>CAROLINA ELIZABETH JACINTO</b></td>
              <td style="width: 88.1mm;" valign="top">Direccion: <br>&nbsp; &nbsp; &nbsp; <b>BARRIO GUADALUPE</b> </td>
            </tr>
          </table>
          <table>
            <tr style="width: 176.4mm; height: 11mm" class="ble bri bbo">
              <td style="width: 176.4mm" valign="top">&nbsp; &nbsp; REFERENCIA DE DOMICILIO: <br> &nbsp; &nbsp;
              &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <b>{{$credito->cliente->persona->domicilio}}</b></td>
            </tr>
            <tr style="width: 176.4mm; height: 7mm" class="ble bri bbo">
              <td style="width: 176.4mm" valign="top">&nbsp; &nbsp; ACTIVIDAD ECONOMICA: <br> &nbsp; &nbsp;
              &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <b>{{$credito->cliente->actividad}}</b></td>
            </tr>
          </table>
          <table>
            <tr style="height: 22mm;" class="bri ble">
              <td style="width: 43mm" valign="top">&nbsp; MONTO ACTUAL
                <table>
                  <tr>
                    <td style="width: 2mm"></td>
                    <td class="bco" style="height: 11mm; width: 37mm; font-size: 18px;" align="center"><b>Q.  {{$credito->capital_activo}}</b></td>
                  </tr>
                </table>
              </td>
              <td style="width: 34mm" valign="top">MONTO SOLICITADO:</td>
              <td valign="top">
                <table>
                  <tr style="height: 9mm">
                    <td class="bco" style="height: 9mm; width: 60mm"><b style="font-size: 20px">&nbsp; &nbsp; Q.<b></td>
                  </tr>
                  <tr style="height:0.5mm">
                  </tr>
                  <tr style="height: 8mm">
                    <td>
                      <table>
                        <tr>
                          <td class="bco" style="width: 13mm; height: 8mm" align="center">20%</td>
                          <td style="width: 3mm"></td>
                          <td class="bco" style="width: 13mm; height: 8mm" align="center">25%</td>
                          <td style="width: 3mm"></td>
                          <td class="bco" style="width: 13mm; height: 8mm" align="center">30%</td>
                          <td style="width: 3mm"></td>
                          <td class="bco" style="width: 13mm; height: 8mm" align="center">45%</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
              <td style="width: 36mm" valign="top">CUOTA PROVISIONAL:
                <table>
                  <tr class="bco" style="height: 11mm">
                    <td style="width: 32mm"><b style="font-size: 18px">&nbsp; Q.<b/></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr style="height: 18mm" class="bri ble bbo">
              <td style="width: 43mm" valign="top">&nbsp; CANCELACION TOTAL
                <table>
                  <tr>
                    <td style="width: 2mm"></td>
                    <td class="bco" style="height: 11mm; width: 37mm; font-size: 18px;" align="center"><b>Q. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</b></td>
                  </tr>
                </table>
              </td>
              <td style="width: 34mm" valign="top">MONTO ASIGNADO:</td>
              <td>
                <table>
                  <tr style="height: 9mm">
                    <td class="bco" style="height: 9mm; width: 62.5mm"><b style="font-size: 20px">&nbsp; &nbsp; Q.<b></td>
                  </tr>
                </table>
              </td>
              <td style="width: 36mm" valign="top">CUOTA ASIGNADA:
                <table>
                  <tr class="bco" style="height: 11mm">
                    <td style="width: 32mm"><b style="font-size: 18px">&nbsp; Q.<b/></td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <table>
            <tr class="ble bri bbo">
              <td colspan="2" align="center" style="height: 9mm; width: 176.4mm"><b style="font-size: 18px;">REFERENCIAS PERSONALES</b></td>
            </tr>
            <tr class="ble bri bbo">
              <td valign="top" style="height: 9mm; width: 130mm">&nbsp; NOMBRE: <br> <b> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {{$credito->cliente->referencias->first()->nombre ." ". $credito->cliente->referencias->first()->apellido}}</b></td>
              <td valign="top">TEL:<br> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {{$credito->cliente->referencias->first()->telefono}}</td>
            </tr>
            <tr class="ble bri bbo">
              <td valign="top" style="height: 9mm; width: 130mm">&nbsp; NOMBRE: <br> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<b>{{$credito->cliente->referencias->last()->nombre . " " . $credito->cliente->referencias->last()->apellido}}</b></td>
              <td valign="top">TEL: <br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {{$credito->cliente->referencias->last()->telefono}}<br> </td>
            </tr>
          </table>
          <table>
            <tr style="height: 15mm">
              <td style="width: 176.4mm" align="center" valign="bottom"><b>F:__________________________________________________<b></td>
            </tr>
            <tr>
              <td align="center"><b>{{$credito->cliente->persona->nombre ." ".$credito->cliente->persona->apellido}}</b></td>
            </tr>
            <tr>
              <td align="center"><b>Firma del Solicitante</b></td>
            </tr>
          </table>
      </div>
    </div>
  </body>
</html>

<script type="text/javascript">
    window.setInterval('imprimir()',1000);
    
    function imprimir(){
        window.print();
        window.close();        
    }
    
</script>


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
