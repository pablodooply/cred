
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style type="text/css">
    @media print {
        body {
        /* transform: scale(.5); */
        margin-top: 1cm;
        /* : 2.5cm; */
        margin-left: 1cm;
        margin-right: 22.7cm;
        zoom: 50%;
    }

    .estilo{
      height: 8mm;
    }

    .col1{
      width: 7mm;
    }

    .col2{
      width: 16mm;
    }

    .texto{
      font-size: 18px;
    }

    td {
      border:  1.5px solid;
    }
}
        /* table {page-break-inside: avoid;
        } */
    </style>
  </head>
  <body style="">
    <div style="border: black 1px solid; padding: 10px;">
      <p style="text-align: right;margin: 5px;"><b class="texto">Cod. De Ptmo:</b>&nbsp; &nbsp; Cre-{{$credito->id}} &nbsp; &nbsp; &nbsp; <b class="texto">Frecuencia:</b> &nbsp; &nbsp; {{$credito->plan->nombre}}</p>
      <p style="text-align: justify;margin: 5px;"><b class="texto"></b>&nbsp; &nbsp; </p>
      <p style="text-align: justify;margin: 5px;"><b class="texto">Nombre:</b>&nbsp; &nbsp; {{$credito->cliente->persona->nombre}} {{$credito->cliente->persona->apellido}}</p>
      <!--<p style="text-align: justify;margin: 5px;"><b class="texto">Dirección:</b>&nbsp; &nbsp; {{$credito->cliente->direccion_recibo}}</p>-->
      <!--<p style="text-align: justify;margin: 5px;"><b class="texto">No. Prmo:</b>&nbsp; &nbsp; {{App\Prestamo::where('cliente_id', $credito->cliente->id)->count()}}</p>-->
      <!--<p style="text-align: justify;margin: 5px;"><b class="texto">Ubicación:</b>&nbsp; &nbsp; {{$credito->cliente->persona->domicilio}}</p>-->
      <p style="text-align: justify;margin: 5px;"><b class="texto"></b>&nbsp; &nbsp; </p>
      <p style="text-align: justify;margin: 5px;"><b class="texto"></b>&nbsp; &nbsp; </p>
      @php
        $ruta = App\Ruta::where('prestamo_id', $credito->id)->first();
      @endphp
      <!--<p style="text-align: justify;margin: 5px;"><b class="texto">Ruta:</b>&nbsp; &nbsp; {{$ruta->hoja_ruta->nombre}}</p>-->
      <!--<p style="text-align: justify;margin: 5px;"><b class="texto">Teléfono:</b>&nbsp; &nbsp; {{isset($credito->cliente->persona->celular1) ? $credito->cliente->persona->celular1 : "No hay"}}</p>-->
      <!--<p style="text-align: justify;margin: 5px;"><b class="texto">Cod. Cliente:</b>&nbsp; &nbsp; Cli-{{$credito->cliente->id}}</p>-->
      <p style="text-align: justify;margin: 5px;"><b class="texto"></b>&nbsp; &nbsp; </p>
      <p style="text-align: justify;margin: 5px;"><b class="texto"></b>&nbsp; &nbsp; </p>
      <p style="text-align: justify;margin: 5px;"><b class="texto"></b>&nbsp; &nbsp; </p>
      <p style="text-align: center;margin: 5px;"><b class="texto">Fecha Inicial:</b>&nbsp; &nbsp; {{\Carbon\Carbon::parse($credito->fecha_inicio)->format('d/m/Y')}} &nbsp; &nbsp; &nbsp; <b class="texto">Fecha Final:</b> &nbsp; &nbsp; {{\Carbon\Carbon::parse($credito->fecha_fin)->format('d/m/Y')}}</p>
      <p style="text-align: center;margin: 5px;"><b class="texto">Monto:</b>&nbsp; &nbsp; &nbsp;{{$credito->monto}}  &nbsp; &nbsp; &nbsp;<b class="texto">Cuota:</b> &nbsp; &nbsp; {{$credito->plan->cuota}} &nbsp; &nbsp; <b class="texto">Recargo:</b> &nbsp; &nbsp; {{$credito->plan->mora}}</p>
      <div class="table-responsive">
        <table border="1.5" style="border-collapse: collapse; width: 100%;">
          <tbody>
            <tr>
              <td class="estilo col1" style="text-align: center;">No.</td>
              <td class="estilo col2" style="text-align: center;">Fecha</td>
              <td class="estilo col2" style="text-align: center;">Abono</td>
              <td class="estilo col1" style="text-align: center;">CA</td>
              <td class="estilo col1" style="text-align: center;">No.</td>
              <td class="estilo col2" style="text-align: center;">Fecha</td>
              <td class="estilo col2" style="text-align: center;">Abono</td>
              <td class="estilo col1" style="text-align: center;">CA</td>
            </tr>
            @for ($i=0; $i < $cant1; $i++)
              <tr>
                <td class="estilo col1" style="text-align: center;">{{$tabla[$i]->no_dia}}</td>
                <td class="estilo col2" style="text-align: center;">{{\Carbon\Carbon::parse($tabla[$i]->fecha)->format('d/m/Y')}}</td>
                <td class="estilo col2"></td>
                <td class="estilo col1"></td>
                @if(($cant2 + $i) < ($cant1 + $cant2))
                  <td class="estilo col1" style="text-align: center;">{{$tabla[$cant2 + $i]->no_dia}}</td>
                  <td class="estilo col2" style="text-align: center;">{{\Carbon\Carbon::parse($tabla[$cant2 + $i]->fecha)->format('d/m/Y')}}</td>
                  <td class="estilo col2"></td>
                  <td class="estilo col1"></td>
                @else
                  <td class="estilo col1"></td>
                  <td class="estilo col2"></td>
                  <td class="estilo col2"></td>
                  <td class="estilo col1"></td>
                @endif
              </tr>
            @endfor
          </tbody>
        </table>
      </div>
    </div>
    <p style="text-align: left;"></p>
  </body>
</html>
<script>
       window.print();
       window.close();
</script>
