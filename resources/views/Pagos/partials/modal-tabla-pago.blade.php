<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <h4 class="modal-title">Efectuar pagos pendientes</h4>
</div>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12">
      <a href="/creditos/{{$id}}" type="button" class="btn btn-block btn-outline btn-primary"><strong>Ir al crédito <span class="text-info">cre-{{$id}}</span></strong></a>
      <br>
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTables-total" id="datatable-pagos-pendientes">
          <thead>
            <tr>
              <th>No. Día</th>
              <th>Fecha</th>
              <th>Cuota</th>
              <th>Mora</th>
              <th>Total Pago</th>
              <th>Estado</th>
              <th>Monto Pagado</th>
              <th>Fecha Ingreso</th>
              <th>Acción</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $('#datatable-pagos-pendientes').DataTable({
    processing: true,
    serverSide: true,
    method: 'GET',
    ajax: '/tabla/pagos/pendientes/{{$id}}',
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    pageLength: 5,
    bLengthChange: false,
    buttons: [{
        extend: 'copy'
      },
      {
        extend: 'csv'
      },
      {
        extend: 'excel',
        title: 'Pagos'
      },
      {
        extend: 'pdf',
        title: 'Pagos'
      },

      {
        extend: 'print',
        customize: function(win) {
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ],
    columns: [{
        name: 'no_dia',
        data: 'no_dia',
      },
      {
        name: 'fecha',
        data: 'fecha',
      },
      {
        name: 'cuota',
        data: 'cuota',
      },
      {
        name: 'mora',
        data: 'mora',
      },
      {
        name: 'total_pago',
        data: 'total_pago',
      },
      {
        name: 'estado',
        data: 'estado',
      },
      {
        name: 'monto_pagado',
        data: 'monto_pagado',
      },
      {
        name: 'Fecha_ingreso',
        data: 'Fecha_ingreso',
      },
      {
        name: 'accion',
        data: 'accion',
      },

    ],
  });

});
</script>
