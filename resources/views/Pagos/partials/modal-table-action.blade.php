@if ($ficha->id==$ficha_pago->first()->id)
  <a class="btn btn-primary btn-outline btn-sm pagar-btn" type="button" value="{{$ficha->id}}" data-toggle="modal" data-target=".bd-modal-pago-pendiente">
    <i class="fa">
      <strong>Q</strong>
    </i>
  </a>
@else
  <a class="btn btn-primary btn-outline btn-sm " type="button" value="" disabled>
    <i class="fa">
      <strong>Q</strong>
    </i>
  </a>
@endif
<script type="text/javascript">
$(".pagar-btn").click(function(){
  var valor = $(this).attr('value');
  // console.log(valor);
  obtenerDatosTablaPendiente(valor);
});

function obtenerDatosTablaPendiente(valor){
  var url = '{{ route("pagos.info.pendiente", ":id") }}';
  url = url.replace(':id', valor);
  $.ajax({
      type: "GET",
      url: url,
      success: function( response ) {
        $('#nom_clienteee').val(response['cliente']);
        $('#prestamooo').val(response['prestamo_id']);
        $('#no_prestamooo').val(response['no_prestamo']);
        $('#no_cuotaaa').val(response['no_cuota']);
        $('#moraaa').val(response['mora']);
        $('#totalll').val(response['cuota']);
        // console.log(response);
      }
  });
}
</script>
