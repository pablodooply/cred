@extends('layouts.app')

@section('title', 'Pagos')

@section('content')

@section('nombre','Pagos')
@section('ruta')
  <li class="active">
      <strong>Historial de pagos</strong>
  </li>
@endsection
    <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-sm-12">
                      <div class="ibox float-e-margins">
                        <div class="ibox-title">
                          <h5>Historial de pagos</h5>
                        </div>
                        <div class="ibox-content">
                          <input type="text" placeholder="Cliente: Nombre, Prestamo: Cre-123, Plan: Diario" class="input form-control" id="busqueda-pagos">
                          <ul class="nav">
                            {{-- <p><span class="pull-left text-muted">{{count($pagos)}} Pagos</span></p> --}}
                          </ul>
                          <div class="table-responsive">
                            <table class='table table-striped table-hover' id="tabla-pagos">
                              <thead>
                                <tr>
                                  <th>DPI</th>
                                  <th>Codigo</th>
                                  <th>Cliente</th>
                                  <th>Prestamo</th>
                                  <th>Promotor</th>
                                  <th>Ruta</th>
                                  <th>Plan</th>
                                  <th class="sum">Monto</th>
                                  <th>Fecha</th>
                                </tr>
                              </thead>

                              <tfoot>
                                <tr>
                                  <th>DPI</th>
                                  <th>Codigo</th>
                                  <th>Cliente</th>
                                  <th>Prestamo</th>
                                  <th>Promotor</th>
                                  <th>Ruta</th>
                                  <th>Plan</th>
                                  <th class="sum">Monto</th>
                                  <th>Fecha</th>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
@endsection

@section('scripts')

<script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function(){
  $('#tabla-pagos').DataTable({
    order: [[ 8, "desc" ]],
    language: {
        "url": "{{asset('fonts/dataTablesEsp.json')}}",
    },
    paging: true,
    info: false,
    dom : 'tip',
    processing: true,
    serverSide: true,
    ajax: {
      "url": '{{route('api.pagos')}}'
    },
    pageLength: 10,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: [
      {extend: 'copy'},
      {extend: 'csv'},
      {extend: 'excel', title: 'Creditos'},
      {extend: 'pdf', title: 'Creditos'},
      {extend: 'print',
         customize: function (win)
         {
            $(win.document.body).addClass('white-bg');
            $(win.document.body).css('font-size', '10px');

            $(win.document.body).find('table')
                    .addClass('compact')
                    .css('font-size', 'inherit');
        }
      }
    ],
    columns: [
        {data: 'DPI', name: 'DPI'},
        {data: 'Codigo', name: 'Codigo'},
        {data: 'Cliente', name: 'Cliente'},
        {data: 'Prestamo', name: 'Prestamo'},
        {data: 'Promotor', name: 'Promotor'},
        {data: 'Ruta', name: 'Ruta'},
        {data: 'Plan', name: 'Plan'},
        {data: 'Monto', name: 'Monto'},
        {data: 'Fecha', name: 'Fecha'},
    ]
  });
});
</script>

@endsection
