@extends('layouts.app')

@section('title', 'Minor page')

@section('link')
  <link href="{{asset('css/steps/jquery.steps.css')}}" rel="stylesheet">

@endsection

@section('content')
  <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox">
                <div class="ibox-title">
                  <h5>Registro</h5>
                </div>
                <div class="ibox-content">
                  {!! Form::open(['route' => 'usuarios.store', 'method' => 'POST', "id" => "form-usuario"]) !!}
                  <div>
                    <h3>Datos personales</h3>
                    <section>
                      <div class="row">
                          <!--*****************Nombre******************-->
                          <div class="form-group col-sm-4">
                              {{ Form::label('name', 'Nombre') }}
                              {{ Form::text('nombre', null, ['class' => 'form-control required']) }}
                          </div>
                          <!--*****************Apellido******************-->
                          <div class="form-group col-sm-4">
                              {{ Form::label('name', 'Apellido') }}
                              {{ Form::text('apellido', null, ['class' => 'form-control required']) }}
                          </div>
                          <!--*****************DPI******************-->
                          <div class="form-group col-sm-4">
                            {{ Form::label('name', 'DPI') }}
                            {{ Form::text('dpi', null, ['class' => 'form-control required']) }}
                          </div>

                          <div class="form-group col-sm-12">
                              {{ Form::label('name', 'Dirección') }}
                              {{ Form::text('direccion', null, ['class' => 'form-control'])}}
                          </div>

                          <div class="form-group col-sm-4">
                              {{ Form::label('name', 'Genero') }}
                              {{-- {{ Form::text('genero', null, ['class' => 'form-control']) }} --}}
                              <select class="form-control" name="genero">
                                <option value="1">Hombre</option>
                                <option value="0">Mujer</option>
                              </select>
                          </div>

                          <div class="form-group col-sm-4">
                              {{ Form::label('name', 'Telefono 1') }}
                              {{ Form::text('telefono', null, ['class' => 'form-control', 'id' => 'telefono1' ]) }}
                          </div>
                          <!--*****************Celular1******************-->
                          <div class="form-group col-sm-4">
                              {{ Form::label('name', 'Telefono Móvil 1') }}
                              {{ Form::text('celular1', null, ['class' => 'form-control', 'id' => 'celular1' ]) }}
                          </div>

                          <div class="form-group col-sm-4">
                              {{ Form::label('name', 'Telefono Móvil 2') }}
                              {{ Form::text('celular2', null, ['class' => 'form-control', 'id' => 'celular2' ]) }}
                          </div>
                          <!--*****************Celular3******************-->
                          <div class="form-group col-sm-8">
                              {{ Form::label('foto', 'Foto de Perfil') }}
                              {{ Form::file('foto', ['class' => 'form-control']) }}
                          </div>
                      </div>
                    </section>
                    <h3>Datos usuario</h3>
                    <section>
                      <div class="form-group col-sm-12">
                          {{ Form::label('name', 'Email') }}
                          {{ Form::text('email', null, ['type' => 'email', 'class' => 'form-control required', 'id' => 'email']) }}
                      </div>
                      <div class="form-group col-sm-6">
                          {{ Form::label('name', 'Contraseña') }}
                          {{ Form::text('password', null, ['type' => 'password', 'class' => 'form-control required', 'id' => 'password' ]) }}
                      </div>
                      <div class="form-group col-sm-6">
                          {{ Form::label('name', 'Verificar Contraseña') }}
                          {{ Form::text('password_confirmation', null, ['data-parsley-equalto' => '#pwd', 'type' => 'password','class' => 'form-control' ]) }}
                      </div>
                    </section>
                    <h3>Datos empresa</h3>
                    <section>
                      <div class="form-group col-sm-4">
                          {{ Form::label('name', 'Agencia') }}
                          <input class="form-control" type="text" name="agencia" value="" disabled>
                      </div>
                      <div class="form-group col-sm-4">
                          {{ Form::label('name', 'Rol') }}
                          {{-- {!! Form::select('rol_id', $rol, 1, ['class' => 'form-control']) !!} --}}
                          <select class="form-control" name="rol_id">
                            <option value="">Administrador</option>
                            <option value="">Rutero</option>
                            <option value="">Secretaria</option>
                          </select>
                      </div>
                      <div class="form-group col-sm-4">
                        {{ Form::label('name', 'Sueldo') }}
                        {{ Form::text('sueldo', null, ['type' => 'number', 'class' => 'form-control']) }}
                      </div>
                    </section>
                  </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="{{asset ('js/steps/jquery.steps.min.js')}}"></script>
<script src="{{asset ('js/validate/jquery.validate.min.js')}}"></script>



<script type="text/javascript">
  var form = $("#form-usuario");
  form.validate({
      errorPlacement: function errorPlacement(error, element) { element.before(error); },
      rules: {
          confirm: {
              equalTo: "#password"
          }
      }
  });

  form.children("div").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    onStepChanging: function (event, currentIndex, newIndex)
    {
      // Allways allow previous action even if the current form is not valid!
      if (currentIndex > newIndex)
      {
          return true;
      }
      // Forbid next action on "Warning" step if the user is to young
      if (newIndex === 3 && Number($("#age-2").val()) < 18)
      {
          return false;
      }
      // Needed in some cases if the user went back (clean up)
      if (currentIndex < newIndex)
      {
          // To remove error styles
          form.find(".body:eq(" + newIndex + ") label.error").remove();
          form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
      }
      form.validate().settings.ignore = ":disabled,:hidden";
      return form.valid();
    },
    onFinishing: function (event, currentIndex)
    {
        form.validate().settings.ignore = ":disabled";
        return form.valid();
    },
    onFinished: function (event, currentIndex)
    {
        alert("Submitted!");
    }
});

</script>

@endsection
