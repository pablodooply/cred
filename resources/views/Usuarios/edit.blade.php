@extends('layouts.app')

@section('title', 'Editar Empleado')

@section('content')

      <div class="row wrapper border-bottom white-bg page-heading">
          <div class="col-lg-10">
              <h2>Editar empleado</h2>
              <ol class="breadcrumb">
                  <li>
                      <a href="{{route('home')}}">Inicio</a>
                  </li>
                  <li class="active">
                      <strong>Editar</strong>
                  </li>
              </ol>
          </div>
          <div class="col-lg-2">

          </div>
      </div>
      <div class="wrapper wrapper-content animated fadeInRight">

                  <div class="row">
                      <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                          @if ($errors->any())
                              <div class="alert alert-danger">
                                  <ul>
                                      @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                              </div>
                          @endif
                          <div class="ibox-title">
                            <h5>Registro</h5>
                          </div>
                          <div class="ibox-content">
                            <h5 class="text-warning">Datos Personales</h5>
                            <hr>
                            <div class="form-row col-sm-12">
                              {!! Form::open(['id'=> 'form','route' => ['usuarios.update', $usuario->id], 'method' => 'PUT', 'files' => true]) !!}

                                <!--*****************Nombre******************-->
                                <div class="form-group col-sm-4">
                                    {{ Form::label('name', 'Nombre') }}
                                    {{ Form::text('nombre', $persona->nombre, ['class' => 'form-control', 'id' => 'nombre' ]) }}
                                </div>
                                <!--*****************Apellido******************-->
                                <div class="form-group col-sm-4">
                                    {{ Form::label('name', 'Apellido') }}
                                    {{ Form::text('apellido', $persona->apellido, ['class' => 'form-control', 'id' => 'apellido' ]) }}
                                </div>
                                <!--*****************DPI******************-->
                                <div class="form-group col-sm-4">
                                  {{ Form::label('name', 'DPI') }}
                                  {{ Form::text('dpi', $persona->dpi, ['class' => 'form-control', 'id' => 'dpi' ]) }}
                                </div>
                            </div>

                            <div class="col-md-12">
                              <div class="form-group col-sm-12">
                                {{ Form::label('name', 'Dirección') }}
                                {{ Form::text('direccion', $persona->domicilio, ['class' => 'form-control'])}}
                              </div>
                            </div>

                            <div class="col-md-12">
                              <div class="form-group col-sm-4">
                                {{ Form::label('name', 'Fecha de Nacimiento') }}
                                {{ Form::date('fecha_nacimiento', $persona->fecha_nacimiento,['class' => 'form-control', ])}}
                              </div>

                              <div class="form-group col-sm-4">
                                {{ Form::label('name', 'Departamento') }}
                                <select name="state" id="departamento" class="form-control" >
                                  @foreach ($states as $state )
                                    <option value="{{$state->id}}">{{$state->name}}</option>
                                  @endforeach
                                </select>
                              </div>
                              <!--*****************ciudad******************-->
                              <div class="form-group col-sm-4">
                                {{ Form::label('name', 'Ciudad') }}
                                <select name="city" id="city" class="form-control" >
                                  <option value="1">Quetzaltenango</option>
                                </select>
                              </div>
                            </div>

                            <div class="form-row col-sm-12">
                                <!--*****************Codigo Postal******************-->
                                <div class="form-group col-sm-4">
                                    {{ Form::label('name', 'Genero') }}
                                    {{-- {{ Form::text('codigo_postal', null, ['class' => 'form-control', 'id' => 'codigo_postal' ]) }} --}}
                                    <select class="form-control" name="genero" disabled>
                                      @if($persona->genero == 1)
                                        <option value="1">Hombre</option>
                                      @else
                                        <option value="0">Mujer</option>
                                      @endif
                                    </select>
                                </div>
                                <!--*****************Telefono 1******************-->
                                <div class="form-group col-sm-4">
                                    {{ Form::label('name', 'Telefono 1') }}
                                    {{ Form::text('telefono1', $persona->telefono, ['class' => 'form-control', 'id' => 'telefono1' ]) }}
                                </div>
                                <!--*****************Celular1******************-->
                                <div class="form-group col-sm-4">
                                    {{ Form::label('name', 'Telefono Móvil 1') }}
                                    {{ Form::text('celular1', $persona->celular1, ['class' => 'form-control', 'id' => 'celular1' ]) }}
                                </div>

                            </div>
                            <div class="form-row col-sm-12">
                                <!--*****************Celular2******************-->
                                <div class="form-group col-sm-4">
                                    {{ Form::label('name', 'Telefono Móvil 2') }}
                                    {{ Form::text('celular2', $persona->celular2, ['class' => 'form-control', 'id' => 'celular2' ]) }}
                                </div>
                                <!--*****************Celular3******************-->
                                <div class="form-group col-sm-8">
                                    {{ Form::label('foto', 'Foto de Perfil') }}
                                    {{ Form::file('foto',['class' => 'form-control']) }}
                                </div>
                            </div>

                            <h5 class="text-info">Datos de usuario</h5>
                            <hr>
                            <div class="form-row col-sm-12">

                              <div class="form-group col-sm-6">
                                  {{ Form::label('name', 'Correo') }}
                                  {{ Form::text('email', $usuario->email, ['type' => 'email', 'class' => 'form-control', 'id' => 'email']) }}
                              </div>
                              <div class="form-group col-sm-6">
                                  {{ Form::label('name', 'Contraseña') }}
                                  {{ Form::text('password', null, ['type' => 'password', 'class' => 'form-control required', 'id' => 'password' ]) }}
                              </div>
                            </div>
                            <h5 class="text-info">Datos de la empresa</h5>
                            <hr>
                            <div class="form-row col-sm-12">
                                <!--*****************Agencia******************-->
                                <div class="form-group col-sm-4">
                                    {{ Form::label('name', 'Agencia') }}
                                    {{-- {{ Form::text('celular2', $agencia, ['class' => 'form-control', 'id' => 'celular2']) }} --}}
                                    <input class="form-control" type="text" name="agencia" value={{$agencia->nombre}} disabled>
                                </div>
                                <div class="form-group col-sm-4">
                                    {{ Form::label('name', 'Rol') }}
                                    <select class="form-control" name="rol_id">
                                      <option value="{{$usuario->roles->first()->id}}">{{$usuario->roles->first()->nombre}}</option>
                                      @foreach ($roles as $ro)
                                        @if($ro->id != $usuario->roles->first()->id)
                                          <option value='{{$ro->id}}'>{{$ro->nombre}}</option>
                                        @endif
                                      @endforeach
                                    </select>
                                </div>
                                {{--*****************Sueldo****************** --}}
                                <div class="form-group col-sm-4">
                                  {{ Form::label('name', 'Sueldo') }}
                                  {{ Form::text('sueldo', $usuario->sueldo_base, ['type' => 'number', 'class' => 'form-control']) }}
                                </div>
                            </div>
                            {{--*****************Comision Cliente****************** --}}

                            <div class="form-row col-sm-12">
                              <div class="form-group col-sm-3">
                                {{ Form::label('name', 'Comision Por Cliente') }}
                                {{ Form::text('comision_cliente', $usuario->comision_cliente_nuevo, ['type' => 'number', 'class' => 'form-control']) }}
                              </div>
                              {{--*****************Comision Por Capital****************** --}}
                              <div class="form-group col-sm-3">
                                {{ Form::label('name', 'Comision Por Capital') }}
                                {{ Form::text('comision_capital', $usuario->comision_capital_activo, ['type' => 'number', 'class' => 'form-control']) }}
                              </div>

                              {{--*****************Combustible****************** --}}
                              <div class="form-group col-sm-3">
                                {{ Form::label('name', 'Combustible') }}
                                {{ Form::text('combustible', $usuario->combustible, ['type' => 'number', 'class' => 'form-control']) }}
                              </div>
                              <div class="form-group col-sm-3">
                                {{ Form::label('name', 'Descuento por Mora') }}
                                {{ Form::text('Descuento', $usuario->descuento_mora, ['type' => 'number', 'class' => 'form-control']) }}
                              </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group" id="guardar">
                                  {{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
                                  {{ Form::button('Cancelar',['class' => 'btn btn-sm btn-danger', 'id' => 'btn_cancel'])}}
                              </div>
                            </div>
                          </div>
                        {!! Form::close() !!}

                        </div>


                      </div>
                  </div>
              </div>
  @endsection

  @section('scripts')

<script type="text/javascript">
$("#btn_cancel").click(function(){
  window.history.back();
});

  $("#departamento").change(function(){
    obtenerDatos();
  });

  function obtenerDatos(){
    var valor = $("#departamento :selected").attr("value"); // The text content of the selected option
    var url = '{{ route("api.municipios", ":id") }}';
    url = url.replace(':id', valor);
    console.log(url);
    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          $("#city").empty();
          $.each(response, function(id,name){
              $("#city").append('<option value="'+name+'">'+ id  +'</option>');
            });
        }
    });
  }

  obtenerDatos();

</script>

  @show
