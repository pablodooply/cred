@extends('layouts.app')

@section('title', 'Registro')

@section('link')
  <link href="{{asset('css/steps/jquery.steps.css')}}" rel="stylesheet">

  <link href="{{asset('css/tool/complemento.css')}}" rel="stylesheet"  media="screen">

@endsection

@section('content')

@section('nombre','Registro de colaboradores')
@section('ruta')
  <li class="active">
      <strong>Registro de colaboradores</strong>
  </li>
@endsection

    <div class="wrapper wrapper-content animated fadeInRight">

                <div class="row">
                    <div class="col-lg-12">
                      <div class="ibox float-e-margins">
                        <div class="ibox-title">
                          <h5>Registro</h5>
                        </div>
                        <div class="ibox-content">
                          {!! Form::open(['route' => 'usuarios.store', 'method' => 'POST', "id" => "form-usuario", 'files' => true]) !!}
                          <div>
                            <h3>Datos personales</h3>
                            <section>
                              <div class="row">
                                  <!--*****************Nombre******************-->
                                  <div class="form-group col-sm-4">
                                      {{ Form::label('name', 'Nombre') }}
                                      {{ Form::text('nombre', null, ['class' => 'form-control required']) }}
                                  </div>
                                  <!--*****************Apellido******************-->
                                  <div class="form-group col-sm-4">
                                      {{ Form::label('name', 'Apellido') }}
                                      {{ Form::text('apellido', null, ['class' => 'form-control required']) }}
                                  </div>
                                  <!--*****************DPI******************-->
                                  <div class="form-group col-sm-4">
                                    {{ Form::label('name', 'DPI') }}
                                    {{ Form::text('dpi', null, ['class' => 'form-control required']) }}
                                  </div>

                                  <div class="form-group col-sm-12">
                                      {{ Form::label('name', 'Dirección') }}
                                      {{ Form::text('direccion', null, ['class' => 'form-control'])}}
                                  </div>

                                  <div class="form-group col-sm-4">
                                      {{ Form::label('name', 'Fecha de Nacimiento') }}
                                      {{ Form::date('fecha_nacimiento', \Carbon\Carbon::now(),['class' => 'form-control', ])}}
                                  </div>

                                  <div class="form-group col-sm-4">
                                    {{ Form::label('name', 'Departamento') }}
                                    <select name="state" id="departamento" class="form-control" >
                                      @foreach ($states as $state )
                                        <option value="{{$state->id}}">{{$state->name}}</option>
                                      @endforeach
                                    </select>
                                  </div>
                                  <!--*****************ciudad******************-->
                                  <div class="form-group col-sm-4">
                                    {{ Form::label('name', 'Ciudad') }}
                                    <select name="city" id="city" class="form-control" >
                                      <option value="1">Quetzaltenango</option>
                                    </select>
                                  </div>


                                  <div class="form-group col-sm-4">
                                      {{ Form::label('name', 'Genero') }}
                                      {{-- {{ Form::text('genero', null, ['class' => 'form-control']) }} --}}
                                      <select class="form-control" name="genero">
                                        <option value="1">Hombre</option>
                                        <option value="0">Mujer</option>
                                      </select>
                                  </div>

                                  <div class="form-group col-sm-4">
                                      {{ Form::label('name', 'Teléfono 1') }}
                                      {{ Form::text('telefono', null, ['class' => 'form-control', 'id' => 'telefono1' ]) }}
                                  </div>
                                  <!--*****************Celular1******************-->
                                  <div class="form-group col-sm-4">
                                      {{ Form::label('name', 'Teléfono Móvil 1') }}
                                      {{ Form::text('celular1', null, ['class' => 'form-control', 'id' => 'celular1' ]) }}
                                  </div>

                                  <div class="form-group col-sm-4">
                                      {{ Form::label('name', 'Teléfono Móvil 2') }}
                                      {{ Form::text('celular2', null, ['class' => 'form-control', 'id' => 'celular2' ]) }}
                                  </div>
                                  <!--*****************Celular3******************-->
                                  <div class="form-group col-sm-8">
                                      {{ Form::label('foto', 'Foto de Perfil') }}
                                      {!! Form::file('foto', ['class' => 'form-control', 'id' => 'foto']) !!}
                                  </div>

                                  <div class="profile-image col-sm-4 col-sm-offset-8">
                                    <img id="imagen" src="{{asset('images/default/default_perfil.jpg')}}" class="pull-center img-circle circle-border m-b-md" alt="profile">
                                  </div>
                              </div>
                            </section>
                            <h3>Datos usuario</h3>
                            <section>
                              <div class="form-group col-sm-12">
                                  {{ Form::label('name', 'Correo') }}
                                  {{ Form::text('email', null, ['type' => 'email', 'class' => 'form-control', 'id' => 'email']) }}
                              </div>
                              <div class="form-group col-sm-6">
                                  {{ Form::label('name', 'Contraseña') }}
                                  {{ Form::text('password', null, ['type' => 'password', 'class' => 'form-control required', 'id' => 'password' ]) }}
                              </div>
                              <div class="form-group col-sm-6">
                                  {{ Form::label('name', 'Verificar Contraseña') }}
                                  {{ Form::text('password_confirmation', null, ['data-parsley-equalto' => '#pwd','id' => "confirm", 'type' => 'password','class' => 'form-control' ]) }}
                              </div>
                              <div class="form-group col-sm-4">
                                  {{ Form::label('name', 'Agencia') }}
                                  <input class="form-control" type="text" name="agencia" value="{{$agencia ? $agencia->nombre : "No existe"}}" disabled>
                              </div>
                              <div class="form-group col-sm-4">
                                  {{ Form::label('name', 'Rol') }}
                                  {{-- {!! Form::select('rol_id', $rol, 1, ['class' => 'form-control']) !!} --}}
                                  <select class="form-control" name="rol_id">
                                    @foreach ($rol as $ro )
                                      <option value="{{$ro->id}}">{{$ro->nombre}}</option>
                                    @endforeach
                                  </select>
                              </div>
                              <div class="form-group col-sm-4">
                                {{ Form::label('name', 'Sueldo') }}
                                {{ Form::text('sueldo', null, ['type' => 'number', 'class' => 'form-control']) }}
                              </div>
                            </section>
                          </div>
                          {!! Form::close() !!}
                        </div>

                      </div>


                    </div>
                </div>
            </div>
@endsection

@section('scripts')

  <script src="{{asset ('js/steps/jquery.steps.js')}}"></script>
  <script src="{{asset ('js/validate/jquery.validate.min.js')}}"></script>



<script type="text/javascript">
    var form = $("#form-usuario");
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            nombre: "required",
            apellido: "required",

            dpi: {
                required: true,
            },

            email: {
                required: true,
                email: true,
            },

            direccion: {
                required: true,
            },

            password: {
                required: true,
                minlength: 6,
            },

            password_confirmation: {
                equalTo: "#password"
            },

            sueldo:{
                required: true,
            }

        },
        messages: {
          nombre: "El nombre es requerido",

          apellido: "El apellido es requerido",

          dpi: {
            required: "El DPI es requerido",
          },

          direccion: {
              required: "La direccion es requerida",
          },

          email: {
            required: "El correo es obligatorio",
            email: "Por favor, introduce una dirección de correo electrónico válida",
          },

          password: {
              required: "La contraseña es requerida",
              minlength: "La contraseña debe tener minimo 6 caracteres"
          },

          password_confirmation: "Por favor, introduzca el mismo valor de nuevo."
        },

          sueldo: {
            required: "El sueldo es necesario"
          }

    });

    form.children("div").steps({
      headerTag: "h3",
      bodyTag: "section",
      transitionEffect: "slideLeft",
      labels:{
        previous: "Anterior",
        next: "Siguiente",
        finish: "Registrar",
      },
      onStepChanging: function (event, currentIndex, newIndex)
      {
        // Allways allow previous action even if the current form is not valid!
        if (currentIndex > newIndex)
        {
            return true;
        }
        // Forbid next action on "Warning" step if the user is to young
        if (newIndex === 3 && Number($("#age-2").val()) < 18)
        {
            return false;
        }
        // Needed in some cases if the user went back (clean up)
        if (currentIndex < newIndex)
        {
            // To remove error styles
            form.find(".body:eq(" + newIndex + ") label.error").remove();
            form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
        }
        form.validate().settings.ignore = ":disabled,:hidden";
        return form.valid();
      },
      onFinishing: function (event, currentIndex)
      {
          form.validate().settings.ignore = ":disabled";
          return form.valid();
      },
      onFinished: function (event, currentIndex)
      {
        form.submit();

      }
  });

  function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#imagen').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
    }
  }

  $("#foto").change(function() {
    readURL(this);
  });

  $("#departamento").change(function(){
    obtenerDatos();
  });

  function obtenerDatos(){
    var valor = $("#departamento :selected").attr("value"); // The text content of the selected option
    var url = '{{ route("api.municipios", ":id") }}';
    url = url.replace(':id', valor);
    console.log(url);
    $.ajax({
        type: "GET",
        url: url,
        success: function( response ) {
          $("#city").empty();
          $.each(response, function(id,name){
              $("#city").append('<option value="'+name+'">'+ id  +'</option>');
            });
        }
    });
  }

  obtenerDatos();
</script>

@show
