@extends('layouts.app')

@section('title', 'Prospectos')

@section('link')
  <link href="{{asset('css/rangedatepicker/daterangepicker.css')}}" rel="stylesheet"  media="screen">
@endsection

@section('content')

@section('nombre','Prospectos')
@section('ruta')
  <li class="active">
      <strong>Prospectos</strong>
  </li>
@endsection
@include('Prospectos.modalCreate')
  <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
              <div class="ibox">
                <div class="ibox-title">
                  <h5>Listado de prospectos</h5> <a data-toggle="modal" data-target="#modal-create" class="btn btn-primary btn-sm pull-right"
                                                    type="button" name="button"><span class="fa fa-plus"> </span> Agregar prospecto</a>
                </div>
                <div class="ibox-content">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="col-md-4">
                        <input id="date-range" class="form-control" type="text" name="" value="">
                      </div>
                      <div class="col-md-8">
                        <input id="busqueda-prospecto" placeholder="Ruta: Alfa" class="form-control" type="text" name="" value="">
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="table-responsive">
                        {!! Form::open(['id'=> 'form-prospecto-del','method'  => 'get']) !!}

                      <table id="tabla-prospectos" class="table table-striped">
                        <thead>
                          <tr>
                            <th>Fecha proxima</th>

                            <th>Nombre</th>
                            <th style="width:20em;">Domicilio</th>

                            <th>Telefono</th>
                            <th>Ruta</th>
                            @if(Auth::user()->hasRole('Administrador'))
                            <th>Acciones</th>
                            @endif
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($prospectos->sortByDesc('fecha_proxima') as $prospecto)
                            <tr>
                              <td>{{$prospecto->fecha_proxima}}</td>

                              <td>{{$prospecto->nombre .' '.$prospecto->apellido}}</td>
                              <td>{{$prospecto->domicilio}}</td>

                              <td>{{$prospecto->telefono}}</td>
                              <td>{{$prospecto->hoja_ruta->nombre}}</td>
                              @if(Auth::user()->hasRole('Administrador'))
                              <td><a value="{{$prospecto->id}}" id="eliminar" class="btn btn-danger btn-sm "><span class="fa fa-times"></span></a></td>
                              @endif
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                      {!! Form::close() !!}
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{asset('js/sweetalert/sweetalert.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/dataTables/datatables.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/moment/moment.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/rangedatepicker/daterangepicker.js')}}"></script>



    <script type="text/javascript">
    var global;
      $('#tabla-prospectos').on('click','#eliminar', function(){
        var valor = $(this).attr('value');
        var form = $('#form-prospecto-del');
        var url = "{{route('prospecto.delete', ':id')}}";
        url = url.replace(':id', valor);
        form.attr('action', url);

        var valor = $(this).attr('value');
        console.log(valor);
        swal({
          title: "Esta seguro de eliminar al cliente de la lista de prospectos??",
          text: "El cliente sera eliminado",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            form.submit();
            swal("Cliente eliminado", {
              icon: "success",
            });
          } else {
            swal("El cliente sigue en la lista");
          }
        });
      });


      var oTable = $('#tabla-prospectos').DataTable({
        "language": {
            "url": "{{asset('fonts/dataTablesEsp.json')}}",
        },
        "paging":   true,
        "info":     false,
        'dom' : 'tip',
        "order": [[ 0, "desc" ]]
      },
    );

    $("#date-range").daterangepicker({
      "locale": {
        "format": "DD/MM/YYYY",
        "fromLabel": "Desde",
        "toLabel": "Hasta",
        "applyLabel": "Aplicar",
        "cancelLabel": "Cancelar",
        "daysOfWeek": [
          "Dom",
          "Lun",
          "Mar",
          "Mie",
          "Jue",
          "Vie",
          "Sab"
        ],
        "monthNames": [
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
        ],
      }
    });

    $('#busqueda-prospecto').keyup(function(){
          oTable.search($(this).val()).draw() ;
    });

    $("#date-range").change(function(){
      console.log('cambio fecha');
      var tabla = $("#tabla-prospectos").DataTable();
      var rango_fechas = $("#date-range").val();
      var valores = rango_fechas.split(' - ');
      var inicio = valores[0];
      var fin = valores[1];
      var url = '{{route('prospecto.info')}}';
      $.ajax({
        type: "GET",
        url: url,
        data: {
          'inicio' : inicio,
          'fin'    : fin,
        },
        success: function(response){
          console.log(response);
          global = response;
          tabla.clear().draw();
          for (var i = 0; i < response.length; i++) {
            var datos_ac = response[i];
            tabla.row.add([
                datos_ac.fecha_proxima,
                datos_ac.nombre + " " + datos_ac.apellido,
                datos_ac.domicilio,
                datos_ac.telefono,
                datos_ac.hoja,
                "<a value=" + datos_ac.id + " id='eliminar' class='btn btn-danger btn-circle btn-sm'><span class='fa fa-times'></span></a>",
            ]).draw();
          }
        },
      });
    })
    </script>
@endsection
