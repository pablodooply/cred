<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listabn extends Model
{
    //
    protected $table      = 'listabn';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $fillable = [
      'nombre', 'descripcion'
    ];

    protected $guarded = [
      'id'
     ];
}
