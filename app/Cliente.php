<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Cliente extends Model
{
    //
    protected $table      = 'cliente';
    protected $primaryKey = 'id';
    public    $timestamps = true;

    protected $fillable = [
      'fecha_ingreso','empresa_trabajo', 'direccion_trabajo', 'direccion_recibo', 'nombre_recibo', 'telefono_empresa',
      'tiempo_trabajando','estado_civil' ,'salario', 'observaciones', 'persona_id', 'listabn_id', 'clasificacion_id',
      'gps_id', 'tipo_casa', 'foto_dpi','foto_recibo', 'actividad', 'direccion_cobrar',
      'nacionalidad', 'foto_firma', 'no_hijos'
    ];

    protected $guarded = [
      'id'
     ];

     public function persona()
     {
         return $this->belongsTo('App\Persona');
     }

     public function referencias()
     {
         return $this->hasMany('App\Referencia_personal');
     }

     public function prestamos()
     {
         return $this->hasMany('App\Prestamo');
     }

     public function ficha_pago()
      {
          return $this->hasManyThrough('App\Ficha_pago', 'App\Prestamo');
      }

      public function gps()
      {
          return $this->belongsTo('App\Gps');
      }

      public function obtenerEdad()
      {
          return Carbon::parse($this->persona->fecha_nacimiento)->age;
      }

      public function obtenerClasificacion()
      {
          $clasi = "";
          $ClienteClas = \App\Cliente::find($this->id);
          $ClasCliente = \App\Prestamo::where('cliente_id',$ClienteClas->id)->orderBy('id','DESC')->first();
          if (isset($classCliente)) {
            switch ($ClasCliente->clasificacion_id) {
              case 0:
                $clasi = "<p class='text-right'><span class='badge badge-success'>A</span></p>";
              break;
              case 1:
                $clasi = "<p class='text-right'><span class='badge badge-primary'>A</span></p>";
              break;
              case 2:
                $clasi = "<p class='text-right'><span class='badge badge-warning'>B</span></p>";
              break;
              case 3:
                $clasi = "<p class='text-right'><span class='badge badge-danger'>C</span></p>";
              break;
              default:
                $clasi = "<p class='text-right'><span class='badge badge-success'>A</span></p>";
              break;
            }
          }
          else {
            $clasi = "<p class='text-right'><span class='badge badge-success'>A</span></p>";
          }

          return $clasi;
      }

      public function obtenerClasificacion2()
      {
          $clasi = "";
          $ClienteClas = \App\Cliente::find($this->id);
          $ClasCliente = \App\Prestamo::where('cliente_id',$ClienteClas->id)->orderBy('id','DESC')->first();
          switch ($ClasCliente->clasificacion_id) {
            case 0:
              $clasi = "<td><span class='badge badge-success'>A</span></td>";
            break;
            case 1:
              $clasi = "<td class='text-right'><span class='badge badge-primary'>A</span></td>";
            break;
            case 2:
              $clasi = "<td class='text-right'><span class='badge badge-warning'>B</span></td>";
            break;
            case 3:
              $clasi = "<td class='text-right'><span class='badge badge-danger'>C</span></td>";
            break;
            default:
              $clasi = "<p class='text-right'><span class='badge badge-success'>A</span></p>";
            break;
          }
          return $clasi;
      }

}
