<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boleta_pago extends Model
{
    //
    protected $table = 'boleta_pago';
    protected $primaryKey = 'id';
    public    $timestamps = true;

    protected $fillable = [
      'fecha', 'sueldo_base', 'comision_clientes', 'comision_capital', 'descuento_mora', 'combustible', 'total', 'users_id',
    ];

    protected $guarded = [
      'id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
