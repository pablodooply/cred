<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    protected $table      = 'location';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $fillable = [
      'countries_id', 'states_id', 'cities_id'

    ];

    protected $guarded = [
      'id'
     ];

     public function states()
     {
       return $this->belongsTo('App\States');
     }

     public function cities()
     {
       return $this->belongsTo('App\Cities');
     }
}
