<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruta extends Model
{
    //
    protected $table      = 'ruta';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $fillable = [
      'nombre', 'prestamo_id', 'hoja_ruta_id', 'estado', 'hora', 'traslado', 'destacado'
    ];

    protected $guarded = [
      'id'
     ];


     public function hoja_ruta()
     {
         return $this->belongsTo('App\Hoja_ruta');
     }

     public function prestamo()
     {
         return $this->belongsTo('App\Prestamo');
     }




}
