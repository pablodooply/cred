<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prospecto extends Model
{
    //
    protected $table      = 'prospecto';
    protected $primaryKey = 'id';
    public    $timestamps = true;

    protected $fillable = [
      'nombre', 'apellido' , 'domicilio', 'fecha_proxima', 'telefono', 'observacion', 'hoja_ruta_id'
    ];


    protected $guarded = [
      'id'
     ];

     public function hoja_ruta()
     {
       return $this->belongsTo('App\Hoja_ruta');
     }

}
