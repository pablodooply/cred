<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\FichasQuincena',
        'App\Console\Commands\FichasDiario',
        'App\Console\Commands\FichasSemanal',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $filePath= public_path() . 'fichassalida.txt';
          if(!is_file($filePath)){
           // Some simple example content.
           echo is_file($filePath);
            file_put_contents($filePath, 'Inicio de Archivo');     // Save our content to the file.
          }
          $schedule->command('fichas:quincena')
          // ->everyMinute()
          // ->daily()
          //Corre el servicio a media noche
          ->appendOutputTo($filePath);
          $schedule->command('fichas:diario')
          // ->everyMinute()
          // ->daily()
          //Corre el servicio a media noche
          ->appendOutputTo($filePath);
          $schedule->command('fichas:semanal')
          // ->everyMinute()
          // ->daily()
          //Corre el servicio a media noche
          ->appendOutputTo($filePath);
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
