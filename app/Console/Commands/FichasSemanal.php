<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Cliente;
use App\User;
use App\Ruta;
use App\Prestamo;
use App\Pago;
use App\Ficha_pago;
use App\Agencia;
use App\Boleta_pago;
use App\Comentario;
use App\Plan;
use File;
use App\Http\Controllers\PagoController;

class FichasSemanal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fichas:semanal';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Correcion de pagos en fichas semanal con estado no pagado';

    /**
     * Create a new command instance.
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $controller = app()->make('App\Http\Controllers\PagoController');
      app()->call([$controller, 'fichasSemanales'], []);
    }
}
