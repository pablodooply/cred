<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    //
    protected $table      = 'plan';
    protected $primaryKey = 'id';
    public    $timestamps = true;

    protected $fillable = [
      'nombre', 'total', 'interes', 'mora', 'periodo_id', 'cuota','capital'
    ];

    protected $guarded = [
      'id'
     ];

     public function prestamos()
     {
       return $this->hasMany('App\Prestamo');
     }

     public function periodo()
     {
         return $this->belongsTo('App\Periodo');
     }


}
