<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gps extends Model
{
    //
    protected $table      = 'gps';
    protected $primaryKey = 'id';
    public    $timestamps = true;

    protected $fillable = [
      'coords_lat', 'coords_lon'
    ];

    protected $guarded = [
      'id'
     ];

     public function timeline_gps()
     {
       return $this->hasMany('App\Timeline_gps');
     }
}
