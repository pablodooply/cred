<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\View\Factory as ViewFactory;
class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
     public function boot(ViewFactory $view)
 {

   $view->composer('*', 'App\Http\GlobalComposer');


 // you can register here  before render the view

 }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
