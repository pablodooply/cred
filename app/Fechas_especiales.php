<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fechas_especiales extends Model
{
    //
    protected $table      = 'fechas_especiales';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $fillable = [
      'fecha', 'descripcion'
    ];

    protected $guarded = [
      'id'
     ];
}
