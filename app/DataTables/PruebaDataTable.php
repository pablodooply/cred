<?php

namespace App\DataTables;
use Yajra\DataTables\Services\DataTable;
use Html;


use App\Prestamo;

class PruebaDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable()
    {
      return datatables($this->query())
      ->addColumn('Estado', function(Prestamo $prestamo){
        return $prestamo->obtenerEstado();
      });
    }


    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
     public function query()
     {
         $prestamos = Prestamo::query()->with(['cliente.persona'])->select('prestamo.*');
         return $this->applyScopes($prestamos);
     }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
     public function html()
     {
         return $this->builder()
             ->columns($this->getColumns())
             ->ajax('')
             ->parameters([
                 'dom' => 'Bfrtip',
                 'buttons' => ['csv', 'excel', 'pdf', 'print', 'reset', 'reload'],
             ]);
     }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $valor = 100;
        return [
          'Codigo'  => ['name' => 'prestamo.id', 'data' => 'id'],
          'Monto'   => ['monto' => 'prestamo.monto', 'data' => 'monto'],
          'Cliente' => ['cliente' => 'cliente.persona.nombre', 'data'  => 'cliente.persona.nombre'],
          'Estado',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Users_' . date('YmdHis');
    }
}
