<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado_p extends Model
{
    //
    protected $table      = 'estado_p';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $fillable = [
      'nombre', 'descripcion'
    ];

    protected $guarded = [
      'id'
     ];

     public function prestamos()
     {
         return $this->hasMany('App\Prestamo');
     }

}
