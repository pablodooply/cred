<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    //
    protected $table      = 'comentario';
    protected $primaryKey = 'id';
    public    $timestamps = true;

    protected $fillable = [
      'descripcion', 'commentable_id', 'updated_at', 'commentable_type',
    ];

    protected $guarded = [
      'id'
     ];

     public function commentable()
     {
         return $this->morphTo();
     }
}
