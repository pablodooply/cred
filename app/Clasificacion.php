<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clasificacion extends Model
{
    //
    protected $table = 'clasificacion';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $fillable = [
      'nombre', 'descripcion'
    ];

    protected $guarded = [
      'id'
     ];

     public function prestamos()
     {
         return $this->hasMany('App\Prestamo');
     }

     public function users()
     {
         return $this->hasMany('App\Users');
     }
}
