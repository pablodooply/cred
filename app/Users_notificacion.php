<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_notificacion extends Model
{
    //
    protected $table = 'users_notificacion';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $guarded = [
      'id'
     ];
}
