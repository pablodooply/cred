<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    //
   protected $table      = 'persona';
   protected $primaryKey = 'id';
   public    $timestamps = false;

   protected $fillable = [
     'nombre', 'apellido', 'dpi', 'nit', 'genero', 'profesion', 'fecha_nacimiento', 'celular1',
     'celular2', 'telefono', 'domicilio', 'foto_perfil', 'location_id', 'tipo', 'cumple'
   ];

   protected $guarded = [
     'id'
    ];


    public function user()
    {
        return $this->hasMany('App\User');
    }

    public function location()
    {
      return $this->belongsTo('App\Location');
    }

    public function cliente()
    {
        return $this->hasMany('App\Cliente');
    }

}
