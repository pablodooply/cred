<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timeline_gps extends Model
{
    //
    protected $table      = 'timeline_gps';
    protected $primaryKey = 'id';
    public    $timestamps = true;

    protected $fillable = [
      'user_id', 'gps_id'
    ];


    protected $guarded = [
      'id'
     ];

     public function user()
     {
         return $this->belongsTo('App\User');
     }

     public function gps()
     {
         return $this->belongsTo('App\Gps');
     }

     public function ultima_posicion()
     {
         return $this->gps->orderby('id','DESC')->take(1)->get();;
     }
}
