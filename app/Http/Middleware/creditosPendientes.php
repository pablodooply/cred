<?php

namespace App\Http\Middleware;

use Closure;
use App\Prestamo;

class creditosPendientes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      // dd($request->route()->parameters());
      $prestamo=Prestamo::where('id',$request->route()->parameters())->first();

      // dd($prestamo->id);
      if($prestamo->estado_p_id==11||$prestamo->estado_p_id==0||$prestamo->estado_p_id==1)
      {
        abort(511);
      }
      else {
        return $next($request);
      }

    }
}
