<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agencia;

class ConfiguracionController extends Controller
{
    //
    public function index(){
      return view('Configuracion.index');
    }

    public function create(Request $request){
        $validatedData = $request->validate([
          'acronimo'      => 'required',
          'nombre'        => 'required',
          'capital'       => 'required',
          'direccion'     => 'nullable',
          'telefono'      => 'nullable',
        ]);

        $agencia = Agencia::create([
          'acronimo'        => $request->input('acronimo'),
          'nombre'          => $request->input('nombre'),
          'capital'         => $request->input('capital'),
          'direccion'       => $request->input('direccion'),
          'telefono'        => $request->input('telefono')
        ]);

        $agencia = Agencia::find(1);
        // $pagos = Pago::with('prestamo.plan');
        return redirect()->route('home');

    }
}
