<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


use App\Notificacion;
use App\Prestamo;
use App\Periodo;
use App\Plan;


class NotificacionController extends Controller
{


    /**
    * Busca todos los prestamos pendientes, para su enlistado en la parte de
    * notificacion, solo el administrador y supervisor tiene acceso a ella.
    */
    public function pendiente(Request $request){
      $usuario = Auth::user();

      if($usuario->hasRole('Administrador')){
        $prestamos = Prestamo::whereIn('estado_p_id',[1])->orderBy('created_at','DES')->get();
        $prestamos_examinados = Prestamo::whereIn('estado_p_id',[2])->orderBy('created_at','DES')->get();

      } else if($usuario->hasRole('Supervisor')){
        $prestamos1 = Prestamo::whereIn('estado_p_id',[1])->orderBy('created_at','DES')->get();
        if(count($prestamos1)>0)
        {$prestamos = Prestamo::whereIn('estado_p_id',[1])
        ->whereHas('ruta.hoja_ruta' , function($query) use ($prestamos1) {
          $query->where('supervisor_id', Auth::user()->id);
         })
        ->orderBy('created_at','DES')->get();}
      }
      else {
        $prestamos = Prestamo::whereIn('estado_p_id',[1])
        ->whereHas('ruta.hoja_ruta' , function($query) use ($prestamos1) {
          $query->where('id',  0);
        });
      }

      if($request->ajax()){
        return view('Notificacion.index', compact('prestamos'));}
      return view('Notificacion.pendientes', compact('prestamos','prestamos_examinados'));
    }

    public function examinados(Request $request){
      $usuario = Auth::user();

      if($usuario->hasRole('Administrador')){
        $prestamos = Prestamo::whereIn('estado_p_id',[1])->orderBy('created_at','DES')->get();
        $prestamos_examinados = Prestamo::whereIn('estado_p_id',[2])->orderBy('created_at','DES')->get();
      }
      return view('Notificacion.examinados', compact('prestamos','prestamos_examinados'));

    }

    /**
    * muesta la informacion del prestamo para su aprobacion,
    * @param int $id del prestamo que va a mostrar la informacion.
    * @return view que muestra la informacion del credito.
    */
    public function show($id){
      $prestamo = Prestamo::find($id);
      $prestamos_ant = Prestamo::where("cliente_id", '=', $prestamo->cliente_id)->where('id', "!=", $id)->where('estado_p_id', '!=', 11)->get();
      $periodos = Periodo::where('nombre',$prestamo->plan->nombre)->get();
      // $planes = Plan::where('periodo_id',$prestamo->plan->periodo->id)->get();
      $planes = Plan::where('periodo_id',$prestamo->plan->periodo->id)->pluck("capital", "id")->toArray();

      $saldototal = 0;
      foreach($prestamos_ant as $credito)
      {
        $saldo = $credito->monto - $credito->pagado;
        $saldototal = $saldototal + $saldo;
      }


      $saldototal = 0;
      $id=[];
      foreach($prestamos_ant as $credito)

      {

        // $saldo = $credito->monto - $credito->pagado;
        //
        // $saldototal = $saldototal + $saldo;
        $listadoPagos=[];
        $pagosSaldo = null;
        $pagosMora = null;
        $totalPagosFicha = null;
        $pagosSaldo = \App\Pago::where('prestamo_id',$credito->id)->sum('monto');
        $pagosMora = \App\Pago::where('prestamo_id',$credito->id)->sum('mora');
        $totalPagosFicha=\App\Ficha_pago::where('prestamo_id',$credito->id)->whereNotNull('pago_id')->get();
        $sumaTotalPagos=0;
        $saldototalPre = 0;
        $moraPendiente = 0;
        $saldototal2 = 0;
        foreach ($totalPagosFicha as $pagosList) {
          $listadoPagos[]=\App\Pago::selectRaw('sum(monto) as total')->where('id',$pagosList->pago_id)->first()->total;
          // $sumaPagos=$sumaPagos+$listadoPagos->mont o;
        }

        if(isset($listadoPagos))
        {
          foreach ($listadoPagos as $sumaPagos) {
            $sumaTotalPagos=$sumaTotalPagos+$sumaPagos;
          }
        }

        else {
          $sumaTotalPagos=0;
        }

        // $totalRealPago=\App\Ficha_pago::where('')
         $saldototalPre=$sumaTotalPagos-$pagosMora;
         $moraPendiente=$credito->mora - $credito->mora_recuperada;
         $saldototal2=($credito->monto - $saldototalPre)+$moraPendiente;
         // dd($saldoFinal);

           if($saldototal2<0)
           {
             $saldototal=0+ $saldototal;
           }
           else {
             $saldototal=$saldototal2+ $saldototal;
           }
           $id[]=$credito->id;

      }
      // dd($saldototal);

      return view('Notificacion.show',compact('prestamo', 'prestamos_ant', 'periodos', 'planes', 'saldototal'));
    }

    public function create(){
      return view('Notificacion.create');
    }

    /**
    * Indica el numero de prestamos pendientes en la barra superior.
    *
    *
    */
    public function num(){
      $usuario = Auth::user();
      if($usuario->hasRole('Administrador')){
        $prestamos = Prestamo::whereIn('estado_p_id',[1]);
        // $prestamos = Notificacion::whereIn('descripcion',['Nuevo'])->where('objeto_type', '=', 'Prestamo')->where('estado', '=', 1);
      } else if($usuario->hasRole('Supervisor')){
        // $prestamos = Notificacion::where('descripcion','=','Nuevo')
        // ->where('objeto_type', '=', 'Prestamo')->where('estado', '=', 1);
        $prestamos1 = Prestamo::whereIn('estado_p_id',[1])->orderBy('created_at','DES')->get();
        if(count($prestamos1)>0)
        {$prestamos = Prestamo::whereIn('estado_p_id',[1])
        ->whereHas('ruta.hoja_ruta' , function($query) use ($prestamos1) {
          $query->where('supervisor_id', Auth::user()->id);
         })
        ->orderBy('created_at','DES')->get();}
        else {
          $prestamos = Prestamo::whereIn('estado_p_id',[1])
          ->whereHas('ruta.hoja_ruta' , function($query) use ($prestamos1) {
            $query->where('id',  0);
          });
        }
      }
      return $prestamos->count();
    }

}
