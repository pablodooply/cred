<?php

namespace App\Http\Controllers;
use Illuminate\Support\Collection as Collection;

use Illuminate\Http\Request;

use App\Cliente;
use App\Prestamo;
use App\User;
use App\Rol;
use App\Hoja_ruta;
use Carbon\Carbon;
use App\Agencia;
use App\Pago;
use App\Boleta_pago;
use App\Ruta;
use Auth;

use DB;

class ReportesController extends Controller
{
      public function index($tipo){
      switch ($tipo) {
        case 1:
          return view('Reportes.index');
          break;
        case 2:
          return view('Reportes.clientes');
          break;
        case 3:
          return view('Reportes.promotores');
          break;
        case 4:
          return view('Reportes.moras');
          break;
        case 5:
          return view('Reportes.clasificacion');
          break;
        case 6:
            $hoja_ruta = Hoja_ruta::where('activa','=',1)->get();
          return view('Reportes.ruta',compact('hoja_ruta'));
          break;
        case 7:
          return view('Reportes.tabla.reporte_promotor_1');
          break;
        case 8:
          return view('Reportes.tabla.reporte_promotor_2');
          break;
      }
    return view('Reportes.index');
    }

    public function apiBusquedaClientes(Request $request){
      // dd($request);
      $inicio = $request->get('inicio') ? $request->get('inicio') : Carbon::now()->startOfMonth()->subMonth();
      $fin = $request->get('fin') ? $request->get('fin') : Carbon::now();
      $tipo = $request->get('tipo');
      $prestamo2 = $request->get('prestamo');
      $clientes = Cliente::with(array('prestamos' => function($query) use ($inicio, $fin, $prestamo2)
      {
          $query->where('created_at','<=', $fin)->whereIn('estado_p_id',[$prestamo2]);
                // ->where('fecha_finalizo','>' ,$inicio);
      }))->where('fecha_ingreso', '<=', $fin)->where('listabn_id','=',$tipo)
         ->get();

      $datos = array();

      $total_cliente = 0;
      $total_dinero = 0;
      $total_recolectado=0;
      $total_pendiente=0;
      foreach ($clientes as $cliente) {
        if($cliente->prestamos->isEmpty()){

        } else {
        $recolectado = 0;
        $moras = 0;
            foreach ($cliente->prestamos as $prestamo) {
              $recolectado = $recolectado + $prestamo->pagos->where('fecha', '<=', $fin)->sum('monto');
            }

            $datosCliente = array(
              'dpi'               => $cliente->persona->dpi,
              'nombre'            => $cliente->persona->nombre,
              'capital_total'     => $cliente->prestamos->sum('monto'),
              'capital_activo'    => $cliente->prestamos->sum('capital_activo'),
              'interes'           => $cliente->prestamos->sum('plan.interes'),
              'mora'              => $cliente->prestamos->sum('mora'),
              'total_recolectado' => $recolectado ?  $recolectado : 0,
              'pendiente'         => $cliente->prestamos->sum('monto') + $cliente->prestamos->sum('mora') - $recolectado,
              'estado'            => $cliente->listabn_id == 1 ? 'Activo' : 'Bloqueado',
              'promotor'          => $cliente->prestamos->first()->user->persona->nombre,
              'codigo_prestamo'   => $cliente->prestamos->last()->id,
              'plan'              => $cliente->prestamos->first()->plan->nombre,
            );

            $total_dinero = $total_dinero + $cliente->prestamos->sum('monto');
            $total_recolectado = $total_recolectado + $recolectado ?  $recolectado : 0;
            $total_pendiente = $cliente->prestamos->sum('monto') - $recolectado;
            $total_cliente = $total_cliente + 1;
        $datos[] = $datosCliente;
        }
      }


      $datosApi = datatables()->collection($datos)->toJson();
      return $datosApi;

    }


    public function apiBusquedaPromotores(Request $request){
      $tipo = $request->get('tipo');
      $inicio = $request->get('inicio') ? $request->get('inicio') : Carbon::now()->startOfMonth()->subMonth();
      $fin = $request->get('fin') ? $request->get('fin') : Carbon::now();

      if($tipo == 1){
        $hoja_rutas = Hoja_ruta::where('activa',1)
                                  ->with(['user.persona','prestamos' => function($scope) use ($inicio, $fin){
          $scope->wherebetween('fecha_desembolso', [$inicio, $fin])->where('estado_p_id', '!=', '1');
        }])
        ->where('supervisor_id',Auth()->user()->id)
        ->get();

        $datos = array();

        $hoja_rutas = $hoja_rutas->groupBy('user_id');
        foreach ($hoja_rutas as $hoja_ruta) {
          $diario = 0;
          $semanal = 0;
          $quincena = 0;
          $total = 0;
          $nombre = $hoja_ruta->first()->user->persona->nombre . " " . $hoja_ruta->first()->user->persona->apellido;
          foreach($hoja_ruta as $hoja){
            $diario = $diario + $hoja->prestamos->where('plan.nombre','Diario')->sum('monto');
            $semanal = $semanal + $hoja->prestamos->where('plan.nombre','Semanal')->sum('monto');
            $quincena = $quincena + $hoja->prestamos->where('plan.nombre','Quincena')->sum('monto');
            $total = $total +$hoja->prestamos->sum('monto');
          }
          $temp = array(
            'promotor'  => $nombre,
            'diario'    => $diario,
            'semanal'   => $semanal,
            'quincena'  => $quincena,
            'total'     => $total,
          );

          $datos[] = $temp;
        }

        $datosApi = datatables()->collection($datos)->toJson();
        return $datosApi;
      } else{
          $roles = Rol::whereIn('nombre',['Promotor', 'Supervisor'])->with(['users.prestamos' => function($query) use($inicio,$fin){
            return $query->whereBetween('fecha_desembolso',[$inicio, $fin]);
          }])->get();
          $hoja_ruta1= Hoja_ruta::where('supervisor_id','=', Auth::user()->id)->pluck('user_id');
          $numeros = ['uno', 'dos', 'tres', 'cuatro'];
          dd($hoja_ruta1);
          $promotores = collect();
          foreach ($roles as $rol) {
            $promotores = $promotores->where('estado','=',1)->merge($rol->users)
                                      ->whereNull('updated_at');
          }
          array_flatten($promotores);
          dd($promotores);
          $datos = [];
          foreach ($promotores as $promotor) {
            $temp = array(
              'promotor' => $promotor->persona->nombre . ' ' . $promotor->persona->apellido,
              'diario'  =>  $promotor->prestamos->where('plan.nombre','Diario')->count(),
              'semanal' =>  $promotor->prestamos->where('plan.nombre','Semanal')->count(),
              'quincena' => $promotor->prestamos->where('plan.nombre','Quincena')->count(),
              'total' =>  $promotor->prestamos->count(),
            );

          $datos[] = $temp;
          }

          $datosApi = datatables()->collection($datos)->toJson();
          return $datosApi;
      }
    }

    public function apiBusquedaPlanes(Request $request){
      $inicio = $request->get('inicio') ? $request->get('inicio') : Carbon::now()->startOfMonth()->subMonth();
      $fin = $request->get('fin') ? $request->get('fin') : Carbon::now();
      $plan = $request->get('plan');
      $tipo = $request->get('tipo');

      $datos = array();
      if($plan == 0){
        $datos[] = $this->planDiario($tipo, $inicio, $fin);
        $datos[] = $this->planSemanal($tipo, $inicio, $fin);
        $datos[] = $this->planQuincena($tipo, $inicio, $fin);
      }

      $datosApi = dataTables()->collection($datos)->toJson();
      return $datosApi;
  }

    public function planDiario($tipo, $inicio, $fin){
      if($tipo <= 2){
        $rango = array(3,4,5,6);
      } else if($tipo == 3){
        $rango = array(10);
      } else{
        $rango = array(9);
      }


      $prestamos_diario = Prestamo::whereIn('estado_p_id',$rango)->where(function($query) use ($inicio, $fin){
        return $query->wherebetween('fecha_desembolso', [$inicio, $fin]);
      })->get()->where('plan.nombre', 'Diario');

      switch ($tipo) {
        case 0:
          $capital_colocado          = $prestamos_diario->sum('capital_activo');
          $interes_colocado          = $prestamos_diario->sum('plan.interes');
          $total_colocado            = $capital_colocado + $interes_colocado;
          $mora                      = 0;
          $array_colocado = array(
            'plan'             => 'Diario',
            'capital'          => $capital_colocado,
            'interes'          => $interes_colocado,
            'mora'             => $mora,
            'total'            => $total_colocado,
          );

          return $array_colocado;
          break;

        case 1:
          $capital_cobrado           = $prestamos_diario->sum(function($query){return $query->pagos->sum('capital');});
          $interes_cobrado           = $prestamos_diario->sum(function($query){return $query->pagos->sum('interes');});
          $mora                      = $prestamos_diario->sum(function($query){return $query->pagos->sum('mora');});
          $total_cobrado             = $capital_cobrado + $interes_cobrado + $mora;
          $array_cobrado = array(
            'plan'              => "Diario",
            'capital'           => $capital_cobrado,
            'interes'           => $interes_cobrado,
            'mora'              => $mora,
            'total'             => $total_cobrado,
          );
          return $array_cobrado;
          break;

        case 2:
          $capital_colocado          = $prestamos_diario->sum('capital_activo');
          $interes_colocado          = $prestamos_diario->sum('plan.interes');

          $capital_pendiente         = $capital_colocado - $prestamos_diario->sum('capital_recuperado');
          $interes_pendiente         = $interes_colocado - $prestamos_diario->sum('interes');
          $mora                      = $prestamos_diario->sum('mora') - $prestamos_diario->sum('mora_recuperada');
          $total_pendiente           = $capital_pendiente + $interes_pendiente + $mora;

          $array_pendiente = array(
            'plan'            => "Diario",
            'capital'         => $capital_pendiente,
            'interes'         => $interes_pendiente,
            'mora'            => $mora,
            'total'           => $total_pendiente,
          );
          return $array_pendiente;
          break;

          case 3:
            $capital_moroso          = $prestamos_diario->sum('capital_activo') - $prestamos_diario->sum('capital_recuperado');
            $interes_moroso          = $prestamos_diario->sum('interes') - $prestamos_diario->sum('plan.interes');
            $mora                    = $prestamos_diario->sum('mora') - $prestamos_diario->sum('mora_recuperada');
            $total_moroso            = $capital_moroso + $interes_moroso + $mora;

            $array_moroso = array(
              'plan'            => "Diario",
              'capital'         => $capital_moroso,
              'interes'         => $interes_moroso,
              'mora'            => $mora,
              'total'           => $total_moroso,
            );
            return $array_moroso;
            break;

            case 4:
              $capital_vencido          = $prestamos_diario->sum('capital_activo');
              $interes_vencido          = $prestamos_diario->sum('interes');
              $mora                     = $prestamos_diario->sum('mora');
              $total_vencido            = $capital_vencido + $interes_vencido + $mora;

              $array_vencido = array(
                'plan'            => "Diario",
                'capital'         => $capital_vencido,
                'interes'         => $interes_vencido,
                'mora'            => $mora,
                'total'           => $total_vencido,
              );
              return $array_vencido;
              break;
      }
    }

    public function planSemanal($tipo, $inicio, $fin){
      if($tipo <= 2){
        $rango = array(3,4,5,6);
      } else if($tipo == 3){
        $rango = array(10);
      } else{
        $rango = array(9);
      }

      $prestamos_diario = Prestamo::whereIn('estado_p_id',$rango)->where(function($query) use ($inicio, $fin){
        return $query->wherebetween('fecha_desembolso', [$inicio, $fin]);
      })->get()->where('plan.nombre', 'Semanal');


      switch ($tipo) {
        case 0:
          $capital_colocado          = $prestamos_diario->sum('capital_activo');
          $interes_colocado          = $prestamos_diario->sum('plan.interes');
          $mora                      = 0;
          $total_colocado            = $capital_colocado + $interes_colocado;
          $array_colocado = array(
            'plan'             => 'Semanal',
            'capital'          => $capital_colocado,
            'interes'          => $interes_colocado,
            'mora'             => $mora,
            'total'            => $total_colocado,
          );
          return $array_colocado;
          break;

        case 1:
          $capital_cobrado           = $prestamos_diario->sum(function($query){return $query->pagos->sum('capital');});
          $interes_cobrado           = $prestamos_diario->sum(function($query){return $query->pagos->sum('interes');});
          $mora           = $prestamos_diario->sum(function($query){return $query->pagos->sum('mora');});
          $total_cobrado             = $capital_cobrado + $interes_cobrado + $mora;
          $array_cobrado = array(
            'plan'              => "Semanal",
            'capital'           => $capital_cobrado,
            'interes'           => $interes_cobrado,
            'mora'              => $mora,
            'total'             => $total_cobrado,
          );
          return $array_cobrado;
          break;

        case 2:
          $capital_colocado          = $prestamos_diario->sum('capital_activo');
          $interes_colocado          = $prestamos_diario->sum('plan.interes');

          $capital_pendiente         = $capital_colocado - $prestamos_diario->sum('capital_recuperado');
          $interes_pendiente         = $interes_colocado - $prestamos_diario->sum('interes');
          $mora                      = $prestamos_diario->sum('mora') -  $prestamos_diario->sum('mora_recuperada');;
          $total_pendiente           = $capital_pendiente + $interes_pendiente;
          $array_pendiente = array(
            'plan'            => "Semanal",
            'capital'         => $capital_pendiente,
            'interes'         => $interes_pendiente,
            'mora'            => $mora,
            'total'           => $total_pendiente,
          );
          return $array_pendiente;
          break;

          case 3:
            $capital_moroso          = $prestamos_diario->sum('capital_activo') - $prestamos_diario->sum('capital_recuperado');
            $interes_moroso          = $prestamos_diario->sum('interes') - $prestamos_diario->sum('plan.interes');
            $mora                    = $prestamos_diario->sum('mora') - $prestamos_diario->sum('mora_recuperada');
            $total_moroso            = $capital_moroso + $interes_moroso + $mora;

            $array_moroso = array(
              'plan'            => "Semanal",
              'capital'         => $capital_moroso,
              'interes'         => $interes_moroso,
              'mora'            => $mora,
              'total'           => $total_moroso,
            );
            return $array_moroso;
            break;

            case 4:
              $capital_vencido          = $prestamos_diario->sum('capital_activo');
              $interes_vencido          = $prestamos_diario->sum('interes');
              $mora                     = $prestamos_diario->sum('mora');
              $total_vencido            = $capital_vencido + $interes_vencido + $mora;

              $array_vencido = array(
                'plan'            => "Semanal",
                'capital'         => $capital_vencido,
                'interes'         => $interes_vencido,
                'mora'            => $mora,
                'total'           => $total_vencido,
              );
              return $array_vencido;
              break;
      }

    }

    public function planQuincena($tipo, $inicio, $fin){
      if($tipo <= 2){
        $rango = array(3,4,5,6);
      } else if($tipo == 3){
        $rango = array(10);
      } else{
        $rango = array(9);
      }

      $prestamos_diario = Prestamo::whereIn('estado_p_id',$rango)->where(function($query) use ($inicio, $fin){
        return $query->wherebetween('fecha_desembolso', [$inicio, $fin]);
                      })->get()->where('plan.nombre', 'Quincena');

      switch ($tipo) {
        case 0:
          $capital_colocado          = $prestamos_diario->sum('capital_activo');
          $interes_colocado          = $prestamos_diario->sum('plan.interes');
          $mora                      = 0;
          $total_colocado            = $capital_colocado + $interes_colocado;
          $array_colocado = array(
            'plan'             => 'Quincena',
            'capital'          => $capital_colocado,
            'interes'          => $interes_colocado,
            'mora'             => $mora,
            'total'            => $total_colocado,
          );

          return $array_colocado;
          break;

        case 1:
          $capital_cobrado           = $prestamos_diario->sum(function($query){return $query->pagos->sum('capital');});
          $interes_cobrado           = $prestamos_diario->sum(function($query){return $query->pagos->sum('interes');});
          $mora                      = $prestamos_diario->sum(function($query){return $query->pagos->sum('mora');});
          $total_cobrado             = $capital_cobrado + $interes_cobrado + $mora;
          $array_cobrado = array(
            'plan'              => "Quincena",
            'capital'           => $capital_cobrado,
            'interes'           => $interes_cobrado,
            'mora'              => $mora,
            'total'             => $total_cobrado,
          );
          return $array_cobrado;
          break;

        case 2:
          $capital_colocado          = $prestamos_diario->sum('capital_activo');
          $interes_colocado          = $prestamos_diario->sum('plan.interes');

          $capital_pendiente         = $capital_colocado - $prestamos_diario->sum('capital_recuperado');
          $interes_pendiente         = $interes_colocado - $prestamos_diario->sum('interes');
          $mora                      = $interes_colocado - $prestamos_diario->sum('mora') - $interes_colocado - $prestamos_diario->sum('mora_recuperada');
          $total_pendiente           = $capital_pendiente + $interes_pendiente;
          $array_pendiente = array(
            'plan'            => "Quincena",
            'capital'         => $capital_pendiente,
            'interes'         => $interes_pendiente,
            'mora'            => $mora,
            'total'           => $total_pendiente,
          );
          return $array_pendiente;
          break;

          case 3:
            $capital_moroso          = $prestamos_diario->sum('capital_activo') - $prestamos_diario->sum('capital_recuperado');
            $interes_moroso          = $prestamos_diario->sum('interes') - $prestamos_diario->sum('plan.interes');
            $mora                    = $prestamos_diario->sum('mora') - $prestamos_diario->sum('mora_recuperada');
            $total_moroso            = $capital_moroso + $interes_moroso + $mora;

            $array_moroso = array(
              'plan'            => "Quincena",
              'capital'         => $capital_moroso,
              'interes'         => $interes_moroso,
              'mora'            => $mora,
              'total'           => $total_moroso,
            );
            return $array_moroso;
            break;

            case 4:
              $capital_vencido          = $prestamos_diario->sum('capital_activo');
              $interes_vencido          = $prestamos_diario->sum('interes');
              $mora                     = $prestamos_diario->sum('mora');
              $total_vencido            = $capital_vencido + $interes_vencido + $mora;

              $array_vencido = array(
                'plan'            => "Quincena",
                'capital'         => $capital_vencido,
                'interes'         => $interes_vencido,
                'mora'            => $mora,
                'total'           => $total_vencido,
              );
              return $array_vencido;
              break;
      }

    }

    public function apiReporteMoras(Request $request){
      $inicio = $request->get('inicio') ? $request->get('inicio') : Carbon::now()->startOfMonth()->subMonth();
      $fin = $request->get('fin') ? $request->get('fin') : Carbon::now();
      $tipo = $request->get('tipo');

      $hoja_rutas = Hoja_ruta::where('activa',1)->with(['user.persona','prestamos' => function($scope) use ($inicio, $fin){
        $scope->wherebetween('fecha_desembolso', [$inicio, $fin]);
      }])->get();
      $datos = array();
      $hoja_rutas = $hoja_rutas->groupBy('user_id');

      foreach ($hoja_rutas as $hoja_ruta) {
        $total_mora = 0;
        $mora_cobrada = 0;
        $mora_pendiente = 0;
        $nombre = $hoja_ruta->first()->user->persona->nombre . " " . $hoja_ruta->first()->user->persona->apellido;
        foreach($hoja_ruta as $hoja){
          $total_mora = $total_mora + $hoja->prestamos->where('plan.periodo_id',$tipo)->sum('mora');
          $mora_cobrada =  $mora_cobrada + $hoja->prestamos->where('plan.periodo_id',$tipo)->sum('mora_recuperada');
          $mora_pendiente = $mora_pendiente + ($total_mora - $mora_cobrada);
        }
        $temp = array(
          'nombre'  => $nombre,
          'total_mora'    => $total_mora,
          'mora_cobrada'   => $mora_cobrada,
          'mora_pendiente'  => $mora_pendiente,
        );

        $datos[] = $temp;
      }

      $datosApi = datatables()->collection($datos)->toJson();
      return $datosApi;
    }


    public function apiReportesGeneral(Request $request)
    {
      // dd($request->get('fin'));
      //Fechas a filtrar
      $inicio = $request->get('inicio') ? $request->get('inicio') : Carbon::now()->startOfMonth()->subMonth();
      $fin = $request->get('fin') ? $request->get('fin') : Carbon::now();
      $dt = Carbon::now()->format('Y-m-d');

      //Se crear array que almacenara datos
      $datos = array();

      $ciclos = 1;

      if($inicio == $dt)
        $ciclos = 1;
      else
        $ciclos = 2;

      //Si ciclos es significa que solo recupero datos de inicio a fin

      //capital actual
      $capital_actual = Agencia::find(1)->capital;

      //Pagos realizados en ese rango de fechas
      $pagos = Pago::where(function($query) use ($inicio, $fin){
        return $query->whereBetween('fecha', [$inicio, $fin]);
      })->get();

      //Pagos a empleados en ese rango de fechas
      $boletas = Boleta_pago::where(function($query) use ($inicio, $fin){
        return $query->whereBetween('fecha', [$inicio, $fin]);
      })->get();

      //Prestamos otorgados en ese rango de fechas
      $prestamos = Prestamo::where(function($query) use ($inicio, $fin){
        return $query->whereBetween('fecha_desembolso',[$inicio, $fin]);
      })->get();

      $capital_anterior = $capital_actual + ($prestamos->sum('monto') + $boletas->sum('total'));

      $pagos_totales = Pago::select('interes','mora','capital');

      $interesRecuperado_actual = round($pagos_totales->sum('interes'),2);
      $moraRecuperada_actual = round($pagos_totales->sum('mora'),2);
      $capitalRecuperado_actual =  round($pagos_totales->sum('capital'),2);

      $interesRecuperado_anterior = $interesRecuperado_actual - $pagos->sum('interes');
      $moraRecuperada_anterior = $moraRecuperada_actual - $pagos->sum('mora');
      $capitalRecuperado_anterior = $capitalRecuperado_actual - $pagos->sum('capital');

      $ganancias_actuales = $interesRecuperado_actual + $moraRecuperada_actual;
      $ganancias_anteriores = $interesRecuperado_anterior + $moraRecuperada_anterior;

      $datoCapital = $capitalRecuperado_actual - $interesRecuperado_anterior;
      $datoInteres = $interesRecuperado_actual - $interesRecuperado_anterior;
      $datoMora = $moraRecuperada_actual - $moraRecuperada_anterior;

      $datosAReporte = array(
        'capitalRecolectado' => $datoCapital,
        'capital' => $capital_actual,
        'interesRecolectado' => $datoInteres,
        'moraRecolectada' => $datoMora,
        'moraTotal' => 0);

      $datos[] = $datosAReporte;

      $datosApi = datatables()->collection($datos)->toJson();

      return $datosApi;
    }

    public function infoReporteRuta(Request $request){
      $inicio = $request->get('inicio');
      $fin = $request->get('fin');
      $idHojaRuta = $request->get('ruta');

      $fichas_pago_hoy = Ruta::join('prestamo', 'prestamo.id', '=','ruta.prestamo_id')
                  ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                  ->join('pago', 'prestamo.id', '=', 'pago.prestamo_id')
                  ->join('cliente','cliente.id','=','prestamo.cliente_id')
                  ->join('persona', 'persona.id', '=','cliente.persona_id')
                  ->join('ficha_pago', 'pago.id', '=','ficha_pago.pago_id')
                  ->where('hoja_ruta_id',$idHojaRuta)
                  //->whereIn('prestamo.estado_p_id',[3,4,5,10])
                  ->whereBetween('pago.fecha', [$inicio,$fin])
                  ->select('persona.nombre','persona.apellido', 'prestamo.id', 'ficha_pago.cuota', 'pago.monto','pago.interes',
                  'pago.capital', 'pago.mora', 'pago.fecha', 'hoja_ruta.id as idHoja','pago.interes', 'pago.mora', 'pago.capital',
                  'ficha_pago.no_dia')
                  ->get();

      $datosApi = datatables()->collection($fichas_pago_hoy)->toJson();

      return $datosApi;
    }

    public function ReportePromotorUno(){
    }

    public function ReportePromotorDos(Request $request){
      //Filtros del lado del cliente
      $tipo_reporte = $request->get('tipo_reporte');
      $tipo_plan = $request->get('tipo_plan');
      $fecha_inicio = $request->get('inicio');
      $fecha_fin = $request->get('fin');

      if($tipo_reporte == 1){
        $usuarios = Rol::find(4)->users()->with(['hoja_ruta.prestamos' => function($query) use ($fecha_inicio, $fecha_fin){
          return $query->where('fecha_inicio','>=' ,$fecha_inicio);
        }, 'hoja_ruta.prestamos.pagos' => function($query) use ($fecha_inicio, $fecha_fin){return $query->whereBetween('fecha', [$fecha_inicio, $fecha_fin]);}])
        ->get();

        // dd($usuarios);
      //Promotor
      //Todos los planes
      $datos = array();
      if($tipo_plan == 0){
          foreach ($usuarios as $usuario) {
            $total_capital = 0;
            $total_mora = 0;
            $total_clientes = 0;
            $total_interes = 0;
            $rutas = "";
            foreach ($usuario->hoja_ruta as $hoja) {
              // $total_capital = $hoja->prestamos->sum('pago.capital');
              $rutas = $rutas . $hoja->nombre . ", ";
              $total_clientes = $total_clientes + $total_clientes + $hoja->prestamos->count();
              $total_capital = $total_capital + $hoja->prestamos->sum(function($query){return $query->pagos->sum('capital');});
              $total_mora = $total_mora + $hoja->prestamos->sum(function($query){return $query->pagos->sum('mora');});
              $total_interes = $total_mora + $hoja->prestamos->sum(function($query){return $query->pagos->sum('interes');});
            }
            $temp = array(
              'nombre'  => $usuario->persona->nombre . " " . $usuario->persona->apellido,
              'rutas' => $rutas,
              'total_clientes' => $total_clientes,
              'total_capital' => $total_capital,
              'total_mora' => $total_mora,
              'total_interes' => $total_interes,
            );
            $datos[] = $temp;
          }
          $datosApi = datatables()->collection($datos)->toJson();
          return $datosApi;
      // }else if($tipo_plan == 1){
      //   $plan = 'Diario';
      //   foreach ($usuarios as $usuario) {
      //       $prestamos = $usuario->hoja_ruta()->with(['prestamos' => function($query) use($plan){
      //         return $query->where('nombre', $plan);
      //       }])->get();
      //
      //     $total_capital = 0;
      //     $total_mora = 0;
      //     $total_clientes = 0;
      //     $total_interes = 0;
      //     $rutas = "";
      //     foreach ($usuario->hoja_ruta as $hoja) {
      //       // $total_capital = $hoja->prestamos->sum('pago.capital');
      //       $rutas = $rutas . " [" . $hoja->nombre . "]";
      //       $total_clientes = $total_clientes + $total_clientes + $hoja->prestamos->count();
      //       $total_capital = $total_capital + $hoja->prestamos->sum(function($query){return $query->pagos->sum('capital');});
      //       $total_mora = $total_mora + $hoja->prestamos->sum(function($query){return $query->pagos->sum('mora');});
      //       $total_interes = $total_mora + $hoja->prestamos->sum(function($query){return $query->pagos->sum('interes');});
      //     }
      //     $temp = array(
      //       'nombre'  => $usuario->persona->nombre . " " . $usuario->persona->apellido,
      //       'rutas' => $rutas,
      //       'total_clientes' => $total_clientes,
      //       'total_capital' => $total_capital,
      //       'total_mora' => $total_mora,
      //       'total_interes' => $total_interes,
      //     );
      //     $datos[] = $temp;
      //   }
      //   $datosApi = datatables()->collection($datos)->toJson();
      //   return $datosApi;
      // }else if($tipo_plan == 2){
      // }else if($tipo_plan == 3){
      }

      } else{
        $usuarios = Rol::find(2)->users()->with(['hoja_ruta.prestamos' => function($query) use ($fecha_inicio, $fecha_fin){
          return $query->where('fecha_inicio','>=' ,$fecha_inicio);
        }, 'hoja_ruta.prestamos.pagos' => function($query) use ($fecha_inicio, $fecha_fin){return $query->whereBetween('fecha', [$fecha_inicio, $fecha_fin]);},
        'hoja_ruta_supervisor.prestamos' => function($query) use ($fecha_inicio, $fecha_fin){
          return $query->where('fecha_inicio','>=' ,$fecha_inicio);
        }, 'hoja_ruta_supervisor.prestamos.pagos' => function($query) use ($fecha_inicio, $fecha_fin){return $query->whereBetween('fecha', [$fecha_inicio, $fecha_fin]);}
      ])
        ->get();

        $datos = array();

        foreach ($usuarios as $usuario) {
          $hojas = collect();
          $total_capital = 0;
          $total_mora = 0;
          $total_clientes = 0;
          $total_interes = 0;
          $rutas = "";
          $hojas_rutas = $usuario->hoja_ruta;
          $hojas_rutas = $hojas_rutas->merge($usuario->hoja_ruta_supervisor);

          // dd($hojas_rutas);
          // dd($hojas_ruta->groupBy('Hoja_ruta'));
          foreach ($hojas_rutas as $hoja) {
              // $hojas->push($hoja);
              // $total_capital = $hoja->prestamos->sum('pago.capital');
              $rutas = $rutas .' ['. $hoja->nombre . "]";
              $total_clientes = $total_clientes + $total_clientes + $hoja->prestamos->count();
              $total_capital = $total_capital + $hoja->prestamos->sum(function($query){return $query->pagos->sum('capital');});
              $total_mora = $total_mora + $hoja->prestamos->sum(function($query){return $query->pagos->sum('mora');});
              $total_interes = $total_mora + $hoja->prestamos->sum(function($query){return $query->pagos->sum('interes');});
          }
          // foreach ($usuario->hoja_ruta_supervisor as $hoja) {
          //   // code...
          //   $rutas = $rutas .' ['. $hoja->nombre . "]";
          //   $total_clientes = $total_clientes + $total_clientes + $hoja->prestamos->count();
          //   $total_capital = $total_capital + $hoja->prestamos->sum(function($query){return $query->pagos->sum('capital');});
          //   $total_mora = $total_mora + $hoja->prestamos->sum(function($query){return $query->pagos->sum('mora');});
          //   $total_interes = $total_mora + $hoja->prestamos->sum(function($query){return $query->pagos->sum('interes');});
          // }

          $temp = array(
            'nombre'  => $usuario->persona->nombre . " " . $usuario->persona->apellido,
            'rutas' => $rutas,
            'total_clientes' => $total_clientes,
            'total_capital' => $total_capital,
            'total_mora' => $total_mora,
            'total_interes' => $total_interes,
          );
          $datos[] = $temp;
        }
        $datosApi = datatables()->collection($datos)->toJson();
        return $datosApi;
      }
    }
}
