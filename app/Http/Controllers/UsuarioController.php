<?php

namespace App\Http\Controllers;
use Yajra\Datatables\Datatables as Datatable;

use Carbon\Carbon;

use App\User;
use App\Rol;
use App\Location;
use App\Persona;
use App\Agencia;
use App\Ruta;
use App\States;
use App\Rol_user;
use App\Boleta_pago;

use DB;

use App\Http\Requests\UsuarioRequest;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class UsuarioController extends Controller
{

  public function __construct()
  {
      // $this->middleware('role:Admnistrador.Supervisor')->only('index', 'show');
      // $this->middleware('role:Admnistrador')->only('edit','update');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    //   $empleados_activos = collect();
    //   $empleados_no_activos = collect();

      //Mostrar empleados
      $usuario = Auth::user();
      if($usuario->hasAnyRole(['Administrador', 'Secretaria'])){
        $empleados_activos = User::where("estado", "=", 1)->where('id', '!=', 1)->get();
        $empleados_no_activos = User::where("estado", "=", 0)->get();
      } else if($usuario->hasRole('Supervisor')){
        $empleados = array();
        foreach ($usuario->hoja_supervisor() as $hoja) {
          $empleados[] = $hoja->user;
        }
        $empleados = collect($empleados);
        $empleados_activos = $empleados->where('estado',1);
        $empleados_no_activos = $empleados->where('estado',0);
      }


      return view('Usuarios.index',compact('empleados_activos', "empleados_no_activos"));
    }

    public function tableEmpleados(Request $request){
      $tipo = $request->get('tipo');
      $usuario = Auth::user();
      if($usuario->hasAnyRole(['Administrador', 'Secretaria'])){
        $empleados_activos = User::where("estado", "=", 1)->where('id', '!=', 1)->get();
        $empleados_no_activos = User::where("estado", "=", 0)->get();
      } else if($usuario->hasRole('Supervisor')){
        $empleados = array();
        foreach ($usuario->hoja_supervisor() as $hoja) {
          $empleados[] = $hoja->user;
        }
        $empleados = collect($empleados);
        $empleados_activos = $empleados->where('estado',1);
        $empleados_no_activos = $empleados->where('estado',0);
      }

      if($tipo == 1){
        $empleados = $empleados_activos;
      } else{
        $empleados = $empleados_no_activos;
      }

      return DataTable::of($empleados)
      ->addColumn('Codigo', function($empleado){
        return '<a class="client-link" value="'.$empleado->id.'">Emp-'. $empleado->id .'</a>';
      })
      ->addColumn('Nombre', function($empleado){
        return  $empleado->persona->nombre . " " . $empleado->persona->apellido;
      })
      ->addColumn('No. Prestamos', function($empleado){
        $hojas_ruta = $empleado->hoja_ruta()->with(['ruta.prestamo' => function($query){
          return $query->whereIn('estado_p_id',[3,4,5,10]);
        }])->get();
        $total_prestamos = 0;
        foreach ($hojas_ruta as $hoja) {
          foreach ($hoja->ruta as $ruta) {
            if($ruta->prestamo != null){
              $total_prestamos++;
            }
          }
        }
        return $total_prestamos;
      })
      ->addColumn('Estado', function($empleado){
        if($empleado->estado == 1){
          $estado = '<a><span class="badge badge-primary pull-center">Activo</span></a>';
        } else{
          $estado = '<a><span class="badge badge-default pull-center">No activo</span></a>';
        }
        return $estado;
      })
      ->addColumn('Puesto', function($empleado){
        return $empleado->roles->first()->nombre;
      })
      ->addColumn('Acciones', function($empleado) use($usuario){
        $accion = '<a href="' . route('usuarios.show',$empleado->id) .'" class="btn btn-outline btn-success btn-sm"><i class="fa fa-eye"></i></a>';
        if($usuario->hasRole('Administrador')){
          $accion = $accion . '<a href="' . route('usuarios.edit',$empleado->id) . '" class="btn btn-outline btn-info btn-sm"><i class="fa fa-pencil"></i></a>';
        }
        return $accion;
      })
      ->escapeColumns([])
      ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Vista para mostrar formulario de empleado.
        $rol = Rol::all();
        $agencia = Agencia::find(1);
        $states = States::all();
        return view('Usuarios.registrar', compact('rol','agencia', 'states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate([
            'nombre' => 'required',
            'apellido' => 'required',
            'dpi' => 'required',
            'nit' => 'nullable',
            'fecha_nacimiento' => 'required',
            'direccion' => 'required',
            'telefono' => 'nullable',
            'celular1' => 'nullable',
            'celular2' => 'nullable',
            'foto_perfil' => 'nullable',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
            'rol' => 'requerid',
            'sueldo' => 'nullable',
        ]);


            // try code

            // Creacion de la locacion del empleado
            $locat = Location::firstOrCreate([
              'countries_id' => 1,
              'states_id' => $request->input('state'),
              'cities_id' => $request->input('city'),
            ]);


            if($request->file('foto') == null){
              $nombre_imagen = null;
            } else{
            $imagen = $request->file('foto');
            $nombre_imagen  = $request->input('nombre') . time() . '.' . $imagen->getClientOriginalExtension();
            $destinoPath = public_path('//images//empleados//perfil');
            $imagen->move($destinoPath, $nombre_imagen);
            $path = $destinoPath . "\\" . $nombre_imagen;
            }

            // Creacion del perfil del empleado
            $personaNew = Persona::create([
              'nombre' => $request->input('nombre'),
              'apellido' => $request->input('apellido'),
              'dpi' => $request->input('dpi'),
              'genero' => $request->input('genero'),
              'fecha_nacimiento' => $request->input('fecha_nacimiento'),
              'telefono' => $request->input('telefono'),
              'celular1' => $request->input('celular1'),
              'celular2' => $request->input('celular2'),
              'domicilio' => $request->input('direccion'),
              'foto_perfil' =>  $nombre_imagen,
              'location_id' => $locat->id,
            ]);


            // Creacion de usuario del empleado
            $userNew = User::create([
              'name' => $request->input('nombre'),
              'email' => $request->input('email'),
              'password' => bcrypt($request->input('password')),
              'persona_id' => $personaNew->id,
              'agencia_id' => Agencia::find(1)->id,
              'sueldo_base' => $request->input('sueldo')
            ]);

            $rol = $request->input('rol_id');

            $rol_user = Rol_user::create([
              'user_id' => $userNew->id,
              'rol_id' => $rol,
            ]);

            // Creacion de bitacora
              return redirect()->route('usuarios.show', ['id' => $userNew->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(Request $request ,$id)
       {
         $usuario = User::findorfail($id);
         $inicio = Carbon::parse($request->get('fecha1'))->format('Y-m-d');
         $fin = Carbon::parse($request->get('fecha2'))->format('Y-m-d');
         $total_pagado = DB::table('boleta_pago')
                         ->where('users_id', '=', $id)
                         ->wherebetween('fecha',[$inicio,$fin])
                         ->select('boleta_pago.total')
                         ->sum('total');
         $total_pagado = round($total_pagado,2);
        if($usuario->hasRole('Promotor'))
         {
           if($usuario->hoja_ruta == null){
             return view('Usuarios.show',compact('usuario'));
           } else{
             $comision_1 = 0.01*$usuario->hoja_ruta->sum('capital_activo');
             $mora_pagada = DB::table('pago')
                             ->join('prestamo', 'pago.prestamo_id', 'prestamo.id')
                             ->join('ruta', 'ruta.prestamo_id', 'prestamo.id')
                             ->join('hoja_ruta', 'ruta.hoja_ruta_id', 'hoja_ruta.id')
                             ->where('hoja_ruta.user_id',$id)
                             ->wherebetween('pago.fecha', [$inicio, $fin])
                             ->sum('pago.mora');
             // $mora_pagada2 = $usuario->hoja_ruta->sum(function($query){return $query->prestamos->sum(function($query){return $query->pagos->sum('mora')});});
             $mora_acumulada = 0;
             // $mora_acumulada = $usuario->prestamos()->Fichas()->get();
             $mora_acumulada = $usuario->hoja_ruta->sum(function($query) use ($inicio, $fin)
               {
                 return $query->prestamos->sum(function($query) use ($inicio, $fin)
                 {
                   return $query->ficha_pago()->where(function($query) use ($inicio, $fin){
                     $fichas = $query->wherebetween('fecha', [$inicio, $fin]);
                   })->sum('ajuste');
                 });
               });
             $mora = $usuario->hoja_ruta->sum('mora');
             $mora_recuperada =$usuario->hoja_ruta->sum('mora_recuperada');
             $mora_actual =  $mora - $mora_recuperada; //1500
             $mora_anterior = ($mora-$mora_acumulada) - ($mora_recuperada - $mora_pagada);
             $comision_2 = 0.05*($mora_actual-$mora_anterior);
             $comision_2 = $comision_2*-1;
             return view('Usuarios.show',compact('usuario','total_pagado','mora_actual','mora_anterior','comision_1', 'comision_2'));
           }
         }
         if($usuario->hasRole('Supervisor'))
         {
           if($usuario->hoja_ruta_supervisor == null){
             return view('Usuarios.show',compact('usuario'));
           } else{
             $comision_1 = 0.01*$usuario->hoja_ruta_supervisor->sum('capital_activo');
             $mora_pagada = DB::table('pago')
                             ->join('prestamo', 'pago.prestamo_id', 'prestamo.id')
                             ->join('ruta', 'ruta.prestamo_id', 'prestamo.id')
                             ->join('hoja_ruta', 'ruta.hoja_ruta_id', 'hoja_ruta.id')
                             ->where('hoja_ruta.supervisor_id',$id)
                             ->wherebetween('pago.fecha', [$inicio, $fin])
                             ->sum('pago.mora');
             // $mora_pagada2 = $usuario->hoja_ruta->sum(function($query){return $query->prestamos->sum(function($query){return $query->pagos->sum('mora')});});
             $mora_acumulada = 0;
             // $mora_acumulada = $usuario->prestamos()->Fichas()->get();
             $mora_acumulada = $usuario->hoja_ruta_supervisor->sum(function($query) use ($inicio, $fin)
               {
                 return $query->prestamos->sum(function($query) use ($inicio, $fin)
                 {
                   return $query->ficha_pago()->where(function($query) use ($inicio, $fin){
                     $fichas = $query->wherebetween('fecha', [$inicio, $fin]);
                   })->sum('ajuste');
                 });
               });
             $mora = $usuario->hoja_ruta_supervisor->sum('mora');
             $mora_recuperada =$usuario->hoja_ruta_supervisor->sum('mora_recuperada');
             $mora_actual =  $mora - $mora_recuperada; //1500
             $mora_anterior = ($mora-$mora_acumulada) - ($mora_recuperada - $mora_pagada);
             $comision_2 = 0.05*($mora_actual-$mora_anterior);
             $comision_2 = $comision_2*-1;
             // dd($usuario->hoja_ruta_supervisor);
             return view('Usuarios.show',compact('usuario','total_pagado','mora_actual','mora_anterior','comision_1', 'comision_2'));
           }
         }
       }
    public function crediMyn(Request $request ,$id)
      {
        //
        //Detalle de un cliente, perfil y hoja de pago
        if ($request->isMethod('post')) {
        $usuario = User::findorfail($id);

        $inicio = Carbon::parse($request->get('fecha1'))->format('Y-m-d');
        $fin = Carbon::parse($request->get('fecha2'))->format('Y-m-d');

        $total_pagado = DB::table('boleta_pago')
                        ->where('users_id', '=', $id)
                        ->wherebetween('fecha',[$inicio,$fin])
                        ->select('boleta_pago.total')
                        ->sum('total');

        $total_pagado = round($total_pagado,2);
        if($usuario->hasRole('Promotor'))
        {
          if($usuario->hoja_ruta == null){
            // return view('Usuarios.show',compact('usuario'));
          } else{
            $comision_1 = 0.01*$usuario->hoja_ruta->sum('capital_activo');

            $mora_pagada = DB::table('pago')
                            ->join('prestamo', 'pago.prestamo_id', 'prestamo.id')
                            ->join('ruta', 'ruta.prestamo_id', 'prestamo.id')
                            ->join('hoja_ruta', 'ruta.hoja_ruta_id', 'hoja_ruta.id')
                            ->where('hoja_ruta.user_id',$id)
                            ->wherebetween('pago.fecha', [$inicio, $fin])
                            ->sum('pago.mora');



            // $mora_pagada2 = $usuario->hoja_ruta->sum(function($query){return $query->prestamos->sum(function($query){return $query->pagos->sum('mora')});});
            $mora_acumulada = 0;

            // $mora_acumulada = $usuario->prestamos()->Fichas()->get();
            $mora_acumulada = $usuario->hoja_ruta->sum(function($query) use ($inicio, $fin)
              {
                return $query->prestamos->sum(function($query) use ($inicio, $fin)
                {
                  return $query->ficha_pago()->where(function($query) use ($inicio, $fin){
                    $fichas = $query->wherebetween('fecha', [$inicio, $fin]);
                  })->sum('ajuste');
                });
              });

            $mora = $usuario->hoja_ruta->sum('mora');
            $mora_recuperada =$usuario->hoja_ruta->sum('mora_recuperada');
            $mora_actual =  $mora - $mora_recuperada; //1500
            $mora_anterior = ($mora-$mora_acumulada) - ($mora_recuperada - $mora_pagada);

            $comision_2 = 0.05*($mora_actual-$mora_anterior);

            $comision_2 = $comision_2*-1;
            // return compact('usuario','total_pagado','mora_actual','mora_anterior','comision_1', 'comision_2');
            return response()->json(['usuario' => $usuario, 'total_pagado' => $total_pagado, 'mora_actual' => $mora_actual, 'mora_anterior' => $mora_anterior, 'comision_1' => $comision_1, 'comision_2' => $comision_2]);

          }
        }

        if($usuario->hasRole('Supervisor'))
        {
          if($usuario->hoja_ruta_supervisor == null){
            // return view('Usuarios.show',compact('usuario'));
          } else{
            $comision_1 = 0.01*$usuario->hoja_ruta_supervisor->sum('capital_activo');
            $mora_pagada = DB::table('pago')
                            ->join('prestamo', 'pago.prestamo_id', 'prestamo.id')
                            ->join('ruta', 'ruta.prestamo_id', 'prestamo.id')
                            ->join('hoja_ruta', 'ruta.hoja_ruta_id', 'hoja_ruta.id')
                            ->where('hoja_ruta.supervisor_id',$id)
                            ->wherebetween('pago.fecha', [$inicio, $fin])
                            ->sum('pago.mora');
            // $mora_pagada2 = $usuario->hoja_ruta->sum(function($query){return $query->prestamos->sum(function($query){return $query->pagos->sum('mora')});});
            $mora_acumulada = 0;
            // $mora_acumulada = $usuario->prestamos()->Fichas()->get();
            $mora_acumulada = $usuario->hoja_ruta_supervisor->sum(function($query) use ($inicio, $fin)
              {
                return $query->prestamos->sum(function($query) use ($inicio, $fin)
                {
                  return $query->ficha_pago()->where(function($query) use ($inicio, $fin){
                    $fichas = $query->wherebetween('fecha', [$inicio, $fin]);
                  })->sum('ajuste');
                });
              });
            $mora = $usuario->hoja_ruta_supervisor->sum('mora');
            $mora_recuperada =$usuario->hoja_ruta_supervisor->sum('mora_recuperada');
            $mora_actual =  $mora - $mora_recuperada; //1500
            $mora_anterior = ($mora-$mora_acumulada) - ($mora_recuperada - $mora_pagada);
            $comision_2 = 0.05*($mora_actual-$mora_anterior);
            $comision_2 = $comision_2*-1;
            // dd($usuario->hoja_ruta_supervisor);
            return response()->json(['usuario' => $usuario, 'total_pagado' => $total_pagado, 'mora_actual' => $mora_actual, 'mora_anterior' => $mora_anterior, 'comision_1' => $comision_1, 'comision_2' => $comision_2]);
            // return compact('usuario','total_pagado','mora_actual','mora_anterior','comision_1', 'comision_2');
          }
        }
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $usuario = User::find($id);
        $persona = Persona::find($usuario->persona_id);
        $roles = Rol::all();
        $rol = Rol::find($usuario->rol_id);
        $agencia = Agencia::find($usuario->agencia_id);
        $states = States::all();
        return view('Usuarios.edit',compact('usuario','persona','roles','agencia','states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validatedData = $request->validate([
            'nombre' => 'required',
            'apellido' => 'required',
            'dpi' => 'required',
        ]);

            //try code
            $user = User::find($id);
            $persona = Persona::find($user->persona_id);
            $user->update([
              'agencia_id' => Agencia::find(1)->id,
              'sueldo_base' => $request->input('sueldo'),
              'comosion_cliente_nuevo' => $request->input('comision_cliente'),
              'comision_capital_activo' => $request->input('comision_capital'),
              'combustible' => $request->input('combustible'),
              'email' => $request->input('email'),
            ]);

            $user->roles->first()->pivot->update([
              'rol_id' => Rol::find($request->input('rol_id'))->id,
            ]);

            if($request->input('password')!=null){
              $user->update([
                'password' => bcrypt($request->input('password')),
              ]);
            }



            if($request->file('foto') == null){
              $nombre_imagen = null;
            } else{
            $imagen = $request->file('foto');
            $nombre_imagen  = $request->input('nombre') . time() . '.' . $imagen->getClientOriginalExtension();
            $destinoPath = public_path('//images//empleados//perfil');
            $imagen->move($destinoPath, $nombre_imagen);
            $path = $destinoPath . "\\" . $nombre_imagen;
            }

            $persona->update([
              'nombre' => $request->input('nombre'),
              'apellido' => $request->input('apellido'),
              'dpi' => $request->input('dpi'),
              'nit' => $request->input('nit'),
              'genero' => $request->input('genero'),
              'fecha_nacimiento' => $request->input('fecha_nacimiento'),
              'telefono' => $request->input('telefono'),
              'celular1' => $request->input('celular1'),
              'celular2' => $request->input('celular2'),
              'domicilio' => $request->input('direccion'),
              'foto_perfil' => $nombre_imagen,
              'domicilio' => $request->input('direccion'),
              'tipo'      => "Empleado",
            ]);

            // Location::find($persona->location_id)->update([
            //   'countries_id' => $request->input('country'),
            //   'states_id' => $request->input('state'),
            //   'cities_id' => $request->input('city'),
            // ]);
            return redirect()->route('usuarios.show', ['id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $usuario = User::find($id);


        $usuario->update([
          'estado'  =>  0,
        ]);

        $usuario->save();

        return redirect()->route('usuarios.index');
    }


    public function posicionesCapital(){
      $roles = Rol::whereIn('nombre',['Promotor', 'Supervisor'])->get();
      $promotores = collect();
      foreach ($roles as $rol) {
        $promotores = $promotores->where('estado',1)->merge($rol->users);
      }

        $posiciones = array();
        //***********Capital Activo*********************
        foreach ($promotores as $promotor) {
          $suma_total = 0;
          $suma_total = $promotor->hoja_ruta->sum('capital_activo');
          $temp = array(
            'codigo'  => $promotor->id,
            'nombre'  => $promotor->persona->nombre . ' ' .$promotor->persona->apellido,
            'imagen'  => $promotor->persona->foto_perfil,
            'total'   => $suma_total,
          );

          $posiciones[] = $temp;
        }

        $posiciones = collect($posiciones)->sortBy('total')->reverse();

        return $posiciones->first();
    }

    public function posicionesMora(){
      $roles = Rol::whereIn('nombre',['Promotor', 'Supervisor'])->get();
      $promotores = collect();
      foreach ($roles as $rol) {
        $promotores = $promotores->where('estado',1)->merge($rol->users);
      }

        $posiciones = array();
        //***********Capital Activo*********************
        foreach ($promotores as $promotor) {
          $suma_total = 0;
          foreach ($promotor->hoja_ruta as $hoja_ruta) {
            $suma_total = $suma_total + $hoja_ruta->prestamos->sum('mora_recuperada');
          }
          $temp = array(
            'codigo'  => $promotor->id,
            'nombre'  => $promotor->persona->nombre ." " . $promotor->persona->apellido,
            'imagen'  => $promotor->persona->foto_perfil,
            'total'   => $suma_total,
          );

          $posiciones[] = $temp;
        }

        $posiciones = collect($posiciones)->sortBy('total')->reverse();

        return $posiciones->first();
    }

    public function posiciones(Request $request){
      $tipo = $request->get('tipo') ? $request->get('tipo') : 1;
      $inicio = $request->get('inicio') ? $request->get('inicio') : Carbon::now()->startOfMonth()->format('Y-m-d');
      $fin = $request->get('fin') ? $request->get('fin') : Carbon::now()->format('Y-m-d');
      // dd($tipo);
      $roles = Rol::whereIn('nombre',['Promotor', 'Supervisor'])->get();
      $promotores = collect();
      foreach ($roles as $rol) {
        $promotores = $promotores->where('estado',1)->merge($rol->users);
      }

      $posiciones = array();
      if($tipo == 1){
        //Devuelve posiciones de capital activo
        foreach ($promotores as $promotor) {
          $suma_total = 0;
          $suma_total = $promotor->hoja_ruta->sum('capital_activo');
          $temp = array(
            'codigo'  => $promotor->id,
            'nombre'  => $promotor->persona->nombre . ' ' .$promotor->persona->apellido,
            'total'   => $suma_total,
          );

          $posiciones[] = $temp;
        }
        $posiciones = collect($posiciones)->sortBy('total')->reverse();

      }else {
        foreach ($promotores as $promotor) {
          $moraReal = 0;
          $mora_r = $promotor->mora_recuperada($inicio, $fin);
          $mora_a = $promotor->mora_aumentada($inicio, $fin);
          $moraReal = $mora_r - $mora_a;
          $temp = array(
            'codigo'  => $promotor->id,
            'nombre'  => $promotor->persona->nombre . ' ' .$promotor->persona->apellido,
            'total'   => $mora_r,
          );
          $posiciones[] = $temp;
        }
      }
      $posiciones = collect($posiciones)->sortBy('total')->reverse();
    //   dd($posiciones);
      if($request->ajax()){
        return $posiciones;
      } else{
      return view('Usuarios.posiciones', compact('posiciones'));
      }
    }


    public function calcularPago(){

    }
}
