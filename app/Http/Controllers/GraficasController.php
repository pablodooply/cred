<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Agencia;
use App\Prestamo;
use App\Pago;
use App\Ficha_pago;
use App\Boleta_pago;
use App\Rol;
use App\Hoja_ruta;


class GraficasController extends Controller
  {
    //
    public function index(){
      return view('Graficas.index');
    }

    public function datosGrafico2(Request $request)
    {
      //Se crear array que almacenara datos
      $datos = array();

      //Fechas a filtrar
      $inicio = Carbon::createFromFormat('d/m/Y', $request->get('inicio'))->format('Y-m-d');
      $fin = Carbon::createFromFormat('d/m/Y', $request->get('fin'))->format('Y-m-d');
      $dt = Carbon::now()->format('Y-m-d');

      $array_fechas = array(
        $inicio,
        $fin,
      );

      $datos[] = $array_fechas;

      $ciclos = 1;

      if($inicio == $dt)
        $ciclos = 1;
      else
        $ciclos = 2;

      //Si ciclos es significa que solo recupero datos de inicio a fin

      //capital actual
      $capital_actual = Agencia::find(1)->capital;

      //Pagos realizados en ese rango de fechas
      $pagos = Pago::where(function($query) use ($inicio, $fin){
        return $query->whereBetween('fecha', [$inicio, $fin]);
      })->get();

      //Pagos a empleados en ese rango de fechas
      $boletas = Boleta_pago::where(function($query) use ($inicio, $fin){
        return $query->whereBetween('fecha', [$inicio, $fin]);
      })->get();

      //Prestamos otorgados en ese rango de fechas
      $prestamos = Prestamo::where(function($query) use ($inicio, $fin){
        return $query->whereBetween('fecha_desembolso',[$inicio, $fin]);
      })->get();

      $capital_anterior = $capital_actual + ($prestamos->sum('monto') + $boletas->sum('total'));

      $pagos_totales = Pago::select('interes','mora','capital');

      $interesRecuperado_actual = round($pagos_totales->sum('interes'),2);
      $moraRecuperada_actual = round($pagos_totales->sum('mora'),2);
      $capitalRecuperado_actual =  round($pagos_totales->sum('capital'),2);

      $interesRecuperado_anterior = $interesRecuperado_actual - $pagos->sum('interes');
      $moraRecuperada_anterior = $moraRecuperada_actual - $pagos->sum('mora');
      $capitalRecuperado_anterior = $capitalRecuperado_actual - $pagos->sum('capital');

      $ganancias_actuales = $interesRecuperado_actual + $moraRecuperada_actual;
      $ganancias_anteriores = $interesRecuperado_anterior + $moraRecuperada_anterior;

      $array_capital = array(
        $capital_anterior,
        $capital_actual,
      );
      $datos[] = $array_capital; //1

      $array_interes = array(
        $interesRecuperado_anterior,
        $interesRecuperado_actual,
      );
      $datos[] = $array_interes; //2

      $array_mora = array(
        $moraRecuperada_anterior,
        $moraRecuperada_actual,
      );
      $datos[] = $array_mora; //3

      $array_capitalRecuperado = array(
        $capitalRecuperado_anterior,
        $capitalRecuperado_actual,
      );
      $datos[] = $array_capitalRecuperado;

      $array_ganancias = array(
        $ganancias_anteriores,
        $ganancias_actuales,
      );
      $datos[] = $array_ganancias; //4

      return $datos;
    }

    public function datosGrafico(Request $request)
    {
      //Se crear array que almacenara datos
      $datos = array();

      $inicio = Carbon::createFromFormat('d/m/y', $request->get('inicio'))->format('Y-m-d');
      $fin = Carbon::createFromFormat('d/m/y', $request->get('fin'))->format('Y-m-d');


      //Se obtiene capital actual
      $capital_actual = Agencia::find(1)->capital;

      //La fecha de hoy
      $dt = Carbon::now()->format("Y-m-d");

      //Se obtiene todos los pagos realizados
      $pagos = Pago::all();

      //Se crear los diferentes arrays para la grafica
      $capital = array();
      $interes = array();
      $mora = array();
      $capital_recuperado = array();
      $ganacias = array();

      //Se actualizar los arrays con los valores de hoy
      $capital[] = $capital_actual;
      $interes[] = round($pagos->sum('interes'),2);
      $mora[] = round($pagos->sum('mora'),2);
      $capital_recuperado[] = $pagos->sum('capital');
      $ganancias[] = round($pagos->sum('interes') + $pagos->sum('mora'),2);

      //Se establece contador a 0 para guardar los arrays
      $cont = 0;

      //se crear un bucle que comenzara desde el mes actual hasta inicios del año
      for ($i=Carbon::now()->month; $i > 0 ; $i--) {
        $inicio_mes = Carbon::now()->subMonth($cont)->startOfMonth()->format("Y-m-d");

        //Se guardara cada interacion de la herramienta que devolvera la informacion de cada mes.
        $datos[] = $this->tool($capital_actual, $inicio_mes, $dt);
        $capital_actual = $datos[$cont]['capital'];
        $capital[] = $capital_actual;
        $interes[] = $datos[$cont]['intereses'];
        $mora[] = $datos[$cont]['mora'];
        $capital_recuperado[] = $datos[$cont]['capital_recuperado'];
        $ganancias[] = $datos[$cont]['ganancias'];
        $cont++;
        $dt = Carbon::now()->subMonth($cont)->endOfMonth()->format("Y-m-d");
      }

        $datosGrafica = array(array_reverse($capital), array_reverse($interes), array_reverse($mora), array_reverse($capital_recuperado), array_reverse($ganancias));

      return $datosGrafica;
    }

    public function tool($capital_actual, $inicio_mes, $fin_mes){
      //Capital desde inicio del mes
      $prestamos = Prestamo::where(function($query) use($fin_mes, $inicio_mes){
        $query->whereBetween('fecha_inicio', [$inicio_mes, $fin_mes]);
      })->get();

      //Pagos
      $pagos = Pago::where(function($query) use($inicio_mes, $fin_mes){
        $query->whereBetween('fecha', [$inicio_mes, $fin_mes]);
      })->get();

      $capital_mes_anterior = $capital_actual + $prestamos->sum('monto');

      $datosCapital = array(
        'mes'       => $inicio_mes,
        'capital'   => $capital_mes_anterior,
        'pagos'     => round($pagos->sum('monto'),2),
        'intereses' => round($pagos->sum('interes'),2),
        'mora'      => round($pagos->sum('mora'),2),
        'capital_recuperado'   => $pagos->sum('capital'),
        'ganancias' => $pagos->sum('interes') + $pagos->sum('mora'),
        );

      return $datosCapital;

    }


    public function promotor(Request $request){
      $inicio =  $request->get('inicio') ? Carbon::createFromFormat('d/m/Y', $request->get('inicio'))->format('Y-m-d') : null;
      $fin =  $request->get('fin') ? Carbon::createFromFormat('d/m/Y', $request->get('fin'))->format('Y-m-d') : null;

      $capital_activo = array();
      $capital_total = array();
      $moras = array();
      $nombres = array();
      $moras_pen = array();
      // dd($tipo);
      $roles = Rol::whereIn('nombre',['Promotor', 'Supervisor'])->get();
      $promotores = collect();
      foreach ($roles as $rol) {
        $promotores = $promotores->where('estado',1)->merge($rol->users);
      }

      foreach ($promotores as $promotor) {
        $nombre = $promotor->persona->nombre;
        $moraReal = 0;
        $mora_r = $promotor->mora_recuperada($inicio, $fin);
        $mora_p = $promotor->mora_pendiente($inicio, $fin);
        $capital_a = $promotor->tcapital($inicio, $fin);
        $capital_t = $promotor->capital_total($inicio, $fin);

        $nombres[] = $nombre;
        $capital_activo[] = $capital_a;
        $capital_total[] = $capital_t;
        $moras[] = $mora_r;
        $moras_pen[] = $mora_p;

      }

      $datos = array($nombres, $capital_total, $capital_activo, $moras, $moras_pen);
      if($request->ajax()){
        return $datos;
      }else {
        return view("Graficas.promotores");
      }
    }

    public function graficaRuta(Request $request){
      // dd($request->get('tipo'));
      if($request->ajax()){
        $tipo = $request->get('tipo');
        if($tipo == 0){
          $hojas_ruta = Hoja_ruta::where('activa', '=', 1)->get();

          return $this->datosGraficas($hojas_ruta, $request, 1);
        }else{
          $id_ruta = $tipo;
          $hoja = Hoja_ruta::find($id_ruta);

          return $this->datosGraficas($hoja, $request, 2);
        }
      }else{
        $hojas_ruta = Hoja_ruta::where('activa', '=', 1)->get();
        return view('Graficas.graficaRuta', compact('hojas_ruta'));
      }

    }

    public function datosGraficas($hojas_ruta, $request, $tipo){
      $inicio =  $request->get('inicio') ? Carbon::createFromFormat('d/m/Y', $request->get('inicio'))->format('Y-m-d') : Carbon::now()->startOfMonth()->format('Y-m-d');
      $fin =  $request->get('fin') ? Carbon::createFromFormat('d/m/Y', $request->get('fin'))->format('Y-m-d') : Carbon::now()->endOfMonth()->format('Y-m-d');
      $nombres = array();
      $capital_activo = array();
      $capital_total = array();
      $moras = array();
      if($tipo == 1){
        foreach ($hojas_ruta as $ruta) {
          $nombres[] = $ruta->nombre;
          $pagos = $ruta->pagos($inicio, $fin);
          $capital_total[] = $pagos->sum('capital') + $pagos->sum('interes');
          $capital_activo[] = $pagos->sum('capital');
          $moras[] = $pagos->sum('mora');
        }
      } else{
          $nombres[] = $hojas_ruta->nombre;
          $pagos = $hojas_ruta->pagos($inicio, $fin);
          $capital_total[] = $pagos->sum('capital') + $pagos->sum('interes');
          $capital_activo[] = $pagos->sum('capital');
          $moras[] = $pagos->sum('mora');
      }

      $datos = array($nombres, $capital_total, $capital_activo, $moras);

      return $datos;
  }
}
