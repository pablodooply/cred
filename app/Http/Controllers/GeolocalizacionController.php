<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\User;
use App\Timeline_gps;
use App\Gps;
use App\Cliente;
use App\Prestamo;
use App\Ruta;
use App\Rol;
use DB;
use App\Ficha_pago;

class GeolocalizacionController extends Controller
{
    //
    public function index(){
      $usuario = Auth::user();
      $acceso = array('Administrador','Secretaria');
      $roles = Rol::whereIn('nombre',['Promotor', 'Supervisor'])->get();
      if($usuario->hasAnyRole($acceso)){

        $empleados = collect();
        foreach ($roles as $rol) {
          $empleados = $empleados->merge($rol->users);
        }
      // $empleados = User::with('timeline_gps.gps')->where('estado', '=', 1)->whereIn('rol_id',[2,4])->get();
        $empleado = $empleados->first();
        $rutas = collect();

        if($empleado != null){
          if($empleado->hasRole('Supervisor')){
            $hojas_rutas = $empleado->hoja_supervisor()->unique();
            foreach ($hojas_rutas as $hoja_ruta) {
              $rutas = $rutas->merge(Ruta::where('hoja_ruta_id', '=', $hoja_ruta->id)->with('prestamo.cliente')->get());
            }
          } else{
            foreach ($empleado->hoja_ruta as $hoja_ruta) {
              $rutas = $rutas->merge(Ruta::where('hoja_ruta_id', '=', $hoja_ruta->id)->with('prestamo.cliente')->get());
            }
          }
        }
      } else if($usuario->hasRole('Supervisor')){
        $empleados = array();
        $rutas = collect();
        $hojas = $usuario->hoja_supervisor()->unique();
        foreach ($hojas as $hoja) {
          $empleados[] = $hoja->user;
        }
        $empleados = collect($empleados)->unique();
        $empleado = $empleados->first();
        if($empleado != null){
          foreach ($empleado->hoja_ruta as $hoja_ruta) {
            $rutas = $rutas->merge(Ruta::where('hoja_ruta_id', '=', $hoja_ruta->id)->with('prestamo.cliente')->get());
          }
        }
      }
      return view('Geolocalizacion.index', compact('empleado', 'rutas', 'empleados'));
    }

    public function posicion_empleado(){
      if (isset($_GET['latitude'])){
        $lat = (isset($_GET['latitude']) ? $_GET['latitude'] : null);
        $lon = (isset($_GET['longitude']) ? $_GET['longitude'] : null);
        $lon = $lon + 360;
        $alt = (isset($_GET['altitude']) ? $_GET['altitude'] : null);
        $codigo = (isset($_GET["secret"]) ? $_GET["secret"] : null);

        $lat = str_replace(',', '.', $lat);
        $lon = str_replace(',', '.', $lon);
        $codigo = preg_replace("/\W+/", "_", $codigo);
        $time = time();
        // dd($lat, $lon, $alt, $codigo);


      //check of data validity
        $empleado = User::find($codigo);


        if($empleado!=null){
        $gps = Gps::create([
          'coords_lat' => $lat,
          'coords_lon' => $lon,
      ]);
      $time_line = Timeline_gps::create([
          'user_id' => $empleado->id,
          'gps_id'   => $gps->id,
      ]);
}
}
    }

    public function posicion_cliente(){
      if (isset($_GET['latitude']) && isset($_GET['latitude'])){
        $lat = (isset($_GET['latitude']) ? $_GET['latitude'] : null);
        $lon = (isset($_GET['longitude']) ? $_GET['longitude'] : null);
        $lon = $lon + 360;
        $alt = (isset($_GET['altitude']) ? $_GET['altitude'] : null);
        $codigo = (isset($_GET["secret"]) ? $_GET["secret"] : null);
        $id_prestamo = (isset($_GET["prestamoid"]) ? $_GET["prestamoid"] : null);

        $lat = str_replace(',', '.', $lat);
        $lon = str_replace(',', '.', $lon);
        $codigo = preg_replace("/\W+/", "_", $codigo);
        $time = time();
        // dd($lat, $lon, $alt, $codigo);
      }
      // dd($gps);
      //check of data validity

        $cliente = Cliente::find($codigo);
        $prestamo = Prestamo::find($id_prestamo);

        $gps = Gps::create([
          'coords_lat' => $lat,
          'coords_lon' => $lon,
       ]);

       $cliente->update([
         'gps_id' => $gps->id,
       ]);

       $prestamo->update([
         'estado_p_id' => 5,
       ]);


     return redirect()->back();
    }

    public function getPosition(Request $request, $id)
      {

        $datos;
        $empleado = User::find($id);
//         if($empleado->timeline_gps->isEmpty()){
// dd($empleado->timeline_gps);
// }
// else {
//   dd('nanai');
// }
        if(!$empleado->timeline_gps->isEmpty()){
        $datos = array(
          'nombre'    =>  $empleado->persona->nombre,
          'latitud'   =>  $empleado->timeline_gps->last()->gps->coords_lat,
          'longitud'  =>  $empleado->timeline_gps->last()->gps->coords_lon,
        );
      }else{
        $datos="";
      }

        return $datos;
    }

    public function getPositionsClients($id){
      $empleado = User::find($id);
      $rutas2 = array();
      $hojas = null;
      // $rutas = collect();

      if($empleado->hasRole('Promotor')){
        $hojas = $empleado->hoja_ruta;
      } else if($empleado->hasRole('Supervisor')){
        $hojas = $empleado->hoja_supervisor();
        $hojas = $hojas->unique();
      }

      foreach ($hojas as $hoja_ruta) {
          // $rutas = $rutas->merge(Ruta::where('hoja_ruta_id', '=', $hoja_ruta->id)->with('prestamo.cliente')->get());
          $rutas = Ruta::with('prestamo')->where('hoja_ruta_id', '=', $hoja_ruta->id)->get();
          $rutas = $rutas->filter(function ($ruta){
            if($ruta->prestamo->estado_p_id != 9){
              return $ruta;
            }
          });          foreach($rutas as $ruta){
            if($ruta->prestamo->cliente->gps == null){

            }else{
              $rutaNew = array([
                'lon' => $ruta->prestamo->cliente->gps->coords_lon,
                'lat' => $ruta->prestamo->cliente->gps->coords_lat,
                'nombre'  => $ruta->prestamo->cliente->persona->nombre,
                'id'  => $ruta->prestamo->id,
                'estado' => $ruta->prestamo->estado_p_id,
                'id_cliente'  => $ruta->prestamo->cliente->id,
                ]);
                $rutas2[] = $rutaNew;
            }
          }
      }

      return $rutas2;
    }

    public function georuta(){

      $usuario = Auth::user();
      if($usuario->hasAnyRole(['Administrador', 'Secretaria'])){
        $roles = Rol::whereIn('nombre',['Promotor', 'Supervisor'])->get();
        $empleados = collect();
        foreach ($roles as $rol) {
          $empleados = $empleados->merge($rol->users);
        }
      } else if($usuario->hasRole('Supervisor')){
        $empleados = array();
        $rutas = collect();
        $hojas = $usuario->hoja_supervisor()->unique();
        foreach ($hojas as $hoja) {
          $empleados[] = $hoja->user;
        }
      }
      return view('Geolocalizacion.georuta', compact('empleados'));
    }

    public function apiRecorrido(Request $request, $id){
      $fecha = $request->get('fecha');
      $timeline = Timeline_gps::whereDate('created_at','=',$fecha)->where('user_id','=',$id)->orderBy('created_at','ASC')->get();

      $datos = array();

      foreach ($timeline as $line) {
        // code...
        $temp = array(
          'fecha'  => Carbon::parse($line->created_at)->toTimeString(),
          'lat'  => $line->gps->coords_lat,
          'lon'  => $line->gps->coords_lon,
        );
        $datos[] = $temp;
      }

      return $datos;
    }


    public function infoRutas(Request $request){
      $idPromo = $request->get('id');
      $promotor = User::find($idPromo);
      $rutas = $promotor->hoja_ruta;
      return $rutas->toArray();
    }

    public function gpsPromotor(){
      $empleado = Auth::user();
      $rutas = collect();

      if($empleado == null){

      }else{
        foreach ($empleado->hoja_ruta as $hoja_ruta) {
          $rutas = $rutas->merge(Ruta::where('hoja_ruta_id', '=', $hoja_ruta->id)->with('prestamo.cliente')->get());
      }
    }

      $rutas = $rutas->filter(function ($ruta){
        if($ruta->prestamo->estado_p_id != 9){
          return $ruta;
        }
      });

      return view('Geolocalizacion.geoUser', compact('empleado', 'rutas'));
    }


}
