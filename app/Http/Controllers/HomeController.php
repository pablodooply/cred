<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

use App\Agencia;
use App\Pago;
use App\Rol;
use App\User;
use App\Ruta;
use App\Prestamo;

class HomeController extends Controller
{
    public function index()
    {
      // if(Auth::user()->hasRole('Administrador')){
        $agencia = Agencia::find(1);
        $pagos = Pago::all();
        $roles = Rol::whereIn('nombre',['Promotor', 'Supervisor'])->get();
        $promotores = collect();
        $capital_recuperado = 0;
        $promotor_mas_capital = null;
        $posiciones = array();
        $key = 0;

        if(Auth::user()->hasRole('Promotor')){
          set_time_limit(0);
          $fichas_pago_todos = Ruta::join('prestamo', 'prestamo.id', '=','ruta.prestamo_id')
                      ->join('hoja_ruta', 'hoja_ruta.id', '=', 'ruta.hoja_ruta_id')
                      ->join('ficha_pago', 'prestamo.id', '=', 'ficha_pago.prestamo_id')
                      // ->join('cliente','cliente.id','=','prestamo.cliente_id')
                      // ->join('persona', 'persona.id', '=','cliente.persona_id')
                      ->where('hoja_ruta.user_id', '=', Auth::user()->id)
                      // ->where('ficha_pago.estado_p', '=',0)
                      ->whereIn('prestamo.estado_p_id',[2,3,5,10])
                      ->groupBy('ficha_pago.prestamo_id')
                      // ->orderBy('ficha_pago.estado_p', 'ASC')
                      // ->orderBy('ruta.hora', 'ASC')
                      ->get();
// dd($fichas_pago_todos);
          $fichas_pago_hoy = Ruta::join('prestamo', 'prestamo.id', '=','ruta.prestamo_id')
                      ->join('hoja_ruta', 'hoja_ruta.id', '=', 'ruta.hoja_ruta_id')
                      ->join('ficha_pago', 'prestamo.id', '=', 'ficha_pago.prestamo_id')
                      ->join('cliente','cliente.id','=','prestamo.cliente_id')
                      ->join('persona', 'persona.id', '=','cliente.persona_id')
                      ->where('hoja_ruta.user_id', '=', Auth::user()->id)
                      ->where('ficha_pago.fecha', '=', Carbon::now()->format("Y-m-d"))
                      ->where('ficha_pago.tipo', '=',1)
                      ->where('prestamo.estado_p_id', '=', 5)
                      ->orderBy('ficha_pago.estado_p', 'ASC')
                      ->orderBy('ruta.hora', 'ASC')
                      ->get();

          $ficha_pagos_pendientes = Ruta::join('prestamo', 'prestamo.id', '=','ruta.prestamo_id')
                      ->join('hoja_ruta', 'hoja_ruta.id', '=', 'ruta.hoja_ruta_id')
                      ->join('ficha_pago', 'prestamo.id', '=', 'ficha_pago.prestamo_id')
                      ->join('cliente','cliente.id','=','prestamo.cliente_id')
                      ->join('persona', 'persona.id', '=','cliente.persona_id')
                      ->where('hoja_ruta.user_id', '=', Auth::user()->id)
                      ->where('ficha_pago.fecha', '<', Carbon::now()->format("Y-m-d"))
                      ->where('ficha_pago.tipo', '=',1)
                      ->where('ficha_pago.estado_p', '=', 0)
                      ->orderBy('ficha_pago.estado_p', 'ASC')
                      ->orderBy('ruta.hora', 'ASC')
                      ->groupBy('prestamo.id')
                      ->get();

          $prestamos_vencidos = Prestamo::join('ruta', 'prestamo.id','=','ruta.prestamo_id')
                                ->join('hoja_ruta','hoja_ruta.id','=','ruta.hoja_ruta_id')
                                ->where('hoja_ruta.user_id','=',Auth::user()->id)
                                ->where('prestamo.estado_p_id','=',10)
                                ->get();
// dd($prestamos_vencidos);
          $prestamos_hoy = $this->indexPromotor();
          $hojas_rutas =  Auth::user()->hoja_ruta;


          return view('home/indexPromotor', compact('fichas_pago_todos', 'hojas_rutas','prestamos_hoy', 'fichas_pago_hoy', 'ficha_pagos_pendientes', 'prestamos_vencidos'));
        }else {

          return view('home/index', compact('agencia', 'pagos', 'promotor_mas_capital'));
        }
    }

    public function indexPromotor(){
      $hojas_rutas =  Auth::user()->hoja_ruta;
      $prestamos_hoy = collect();
      foreach($hojas_rutas as $hoja_ruta){
        foreach ($hoja_ruta->prestamos as $prestamo) {
          // code
          $ficha_hoy = $prestamo->ficha_pago->where('fecha', Carbon::now()->format('Y-m-d'))->where('tipo','=',1);
          if($ficha_hoy == null){}
            else{
              $prestamos_hoy = $prestamos_hoy->merge($ficha_hoy);
            }
        }
      }
      return $prestamos_hoy;
    }

    public function prueba(Request $request)
    {
      $valor1 = $request->get('valor1');
      $valor2 = $request->get('valor2');


    }
}
